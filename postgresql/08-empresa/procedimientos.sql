CREATE OR REPLACE PROCEDURE empresa.compras_clientes(IN dias INT, INOUT num int)
	LANGUAGE plpgsql
AS $procedure$
	BEGIN
SELECT count(*) into num 
FROM empresa.clientes 
WHERE Id IN (SELECT cliente FROM empresa.facturas 
WHERE fecha>= (CURRENT_DATE - dias));
	END;
$procedure$
;

CREATE OR REPLACE PROCEDURE empresa.total_factura(IN fact INT, INOUT totalf float8)
	LANGUAGE plpgsql
AS $procedure$
	BEGIN
		select sum(importe) into totalf from empresa.lineas_factura
		where factura = fact;
	END;
$procedure$
;
