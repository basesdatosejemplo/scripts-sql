--
-- Ventas por vendedor
--

CREATE OR replace VIEW empresa.ventas_vendedor AS
select empresa.vendedores.id AS id, empresa.vendedores.nombre AS nombre, empresa.vendedores.fecha_ingreso AS fecha_ingreso,
    empresa.vendedores.salario AS salario
from empresa.vendedores
where
    not exists(
    select 1
    from empresa.grupos
    where not exists(
        select 1
        from empresa.articulos
        where empresa.articulos.grupo = empresa.grupos.id
            and exists(
            select 1
            from empresa.lineas_factura inner join empresa.facturas
            	 on empresa.lineas_factura.factura = empresa.facturas.id
            where empresa.lineas_factura.articulo = empresa.articulos.id
                and empresa.facturas.vendedor = empresa.vendedores.id)));
