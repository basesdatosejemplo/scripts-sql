--
-- PostgreSQL database dump
--

-- Dumped from database version 13devel
-- Dumped by pg_dump version 13devel

-- Started on 2020-04-14 21:12:39 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 18460)
-- Name: bdjardineria; Type: SCHEMA; Schema: -; Owner: batoi
--
DROP SCHEMA if exists bdjardineria CASCADE;

CREATE SCHEMA bdjardineria;


ALTER SCHEMA bdjardineria OWNER TO batoi;

SET search_path TO bdjardineria;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 18481)
-- Name: categoria; Type: TABLE; Schema: bdjardineria; Owner: batoi
--

CREATE TABLE bdjardineria.categoria (
    categoria_nombre character varying(50) NOT NULL,
    descripcion text,
    descripcion_html text,
    imagen bytea
);


ALTER TABLE bdjardineria.categoria OWNER TO batoi;

--
-- TOC entry 204 (class 1259 OID 18487)
-- Name: cliente; Type: TABLE; Schema: bdjardineria; Owner: batoi
--

CREATE TABLE bdjardineria.cliente (
    cliente_cod bigint NOT NULL,
    nombre character varying(50) NOT NULL,
    nombre_contacto character varying(30),
    apellido_contacto character varying(30),
    telefono character varying(15) NOT NULL,
    fax character varying(15),
    linea_direccion1 character varying(50) NOT NULL,
    linea_direccion2 character varying(50),
    ciudad character varying(50) NOT NULL,
    region character varying(50),
    pais character varying(50) NOT NULL,
    codigo_postal character varying(10),
    empleado_cod bigint,
    limite_credito numeric(15,2)
);


ALTER TABLE bdjardineria.cliente OWNER TO batoi;

--
-- TOC entry 205 (class 1259 OID 18490)
-- Name: detallepedido; Type: TABLE; Schema: bdjardineria; Owner: batoi
--

CREATE TABLE bdjardineria.detallepedido (
    pedido_cod bigint NOT NULL,
    producto_cod character varying(15) NOT NULL,
    cantidad bigint NOT NULL,
    precio_unidad numeric(15,2) NOT NULL,
    linea_num smallint NOT NULL
);


ALTER TABLE bdjardineria.detallepedido OWNER TO batoi;

--
-- TOC entry 206 (class 1259 OID 18493)
-- Name: empleado; Type: TABLE; Schema: bdjardineria; Owner: batoi
--

CREATE TABLE bdjardineria.empleado (
    empleado_cod bigint NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido1 character varying(50) NOT NULL,
    apellido2 character varying(50),
    extension character varying(10) NOT NULL,
    email character varying(100) NOT NULL,
    oficina_cod character varying(10) NOT NULL,
    jefe_cod bigint,
    puesto character varying(50)
);


ALTER TABLE bdjardineria.empleado OWNER TO batoi;

--
-- TOC entry 207 (class 1259 OID 18496)
-- Name: oficina; Type: TABLE; Schema: bdjardineria; Owner: batoi
--

CREATE TABLE bdjardineria.oficina (
    oficina_cod character varying(10) NOT NULL,
    ciudad character varying(30) NOT NULL,
    pais character varying(50) NOT NULL,
    region character varying(50),
    codigo_postal character varying(10) NOT NULL,
    telefono character varying(20) NOT NULL,
    linea_direccion1 character varying(50) NOT NULL,
    linea_direccion2 character varying(50)
);


ALTER TABLE bdjardineria.oficina OWNER TO batoi;

--
-- TOC entry 208 (class 1259 OID 18499)
-- Name: pago; Type: TABLE; Schema: bdjardineria; Owner: batoi
--
CREATE TYPE bdjardineria.pago_forma_pago AS ENUM (
	'PayPal',
	'Tarjeta',
	'Cheque',
	'Transferencia',
	'Metálico');
CREATE TABLE bdjardineria.pago (
    pedido_cod bigint NOT NULL,
    pago_num smallint NOT NULL,
    forma_pago bdjardineria.pago_forma_pago DEFAULT 'PayPal'::bdjardineria.pago_forma_pago NOT NULL,
    transaccion_id character varying(50) NOT NULL,
    fecha_pago date NOT NULL,
    importe numeric(15,2) NOT NULL
);


ALTER TABLE bdjardineria.pago OWNER TO batoi;

--
-- TOC entry 209 (class 1259 OID 18503)
-- Name: pais; Type: TABLE; Schema: bdjardineria; Owner: batoi
--

CREATE TABLE bdjardineria.pais (
    nombre character varying(50) NOT NULL
);


ALTER TABLE bdjardineria.pais OWNER TO batoi;

--
-- TOC entry 210 (class 1259 OID 18506)
-- Name: pedido; Type: TABLE; Schema: bdjardineria; Owner: batoi
--
CREATE TYPE bdjardineria.pedido_estado AS ENUM (
	'Pendiente',
	'Entregado',
	'Rechazado');
CREATE TABLE bdjardineria.pedido (
    pedido_cod bigint NOT NULL,
    fecha_pedido date NOT NULL,
    fecha_esperada date NOT NULL,
    fecha_entrega date,
    estado bdjardineria.pedido_estado NOT NULL,
    comentarios_empresa text,
    cliente_cod bigint NOT NULL
);


ALTER TABLE bdjardineria.pedido OWNER TO batoi;

--
-- TOC entry 211 (class 1259 OID 18512)
-- Name: producto; Type: TABLE; Schema: bdjardineria; Owner: batoi
--

CREATE TABLE bdjardineria.producto (
    producto_cod character varying(15) NOT NULL,
    nombre character varying(70) NOT NULL,
    categoria_nombre character varying(50) NOT NULL,
    dimensiones character varying(25),
    proveedor character varying(50),
    descripcion text,
    cantidad_stock smallint NOT NULL,
    precio_venta numeric(15,2) NOT NULL,
    precio_proveedor numeric(15,2)
);


ALTER TABLE bdjardineria.producto OWNER TO batoi;

--
-- TOC entry 2275 (class 0 OID 18481)
-- Dependencies: 203
-- Data for Name: categoria; Type: TABLE DATA; Schema: bdjardineria; Owner: batoi
--

INSERT INTO bdjardineria.categoria VALUES ('Aromáticas', 'Plantas aromáticas', NULL, NULL);
INSERT INTO bdjardineria.categoria VALUES ('Frutales', 'Árboles pequeños de producción frutal', NULL, NULL);
INSERT INTO bdjardineria.categoria VALUES ('Herbaceas', 'Plantas para jardin decorativas', NULL, NULL);
INSERT INTO bdjardineria.categoria VALUES ('Herramientas', 'Herramientas para todo tipo de acción', NULL, NULL);
INSERT INTO bdjardineria.categoria VALUES ('Ornamentales', 'Plantas vistosas para la decoración del jardín', NULL, NULL);


--
-- TOC entry 2276 (class 0 OID 18487)
-- Dependencies: 204
-- Data for Name: cliente; Type: TABLE DATA; Schema: bdjardineria; Owner: batoi
--

INSERT INTO bdjardineria.cliente VALUES (1, 'DGPRODUCTIONS GARDEN', 'Daniel G', 'GoldFish', '5556901745', '5556901746', 'False Street 52 2 A', NULL, 'San Francisco', NULL, 'EEUU', '24006', 19, 3000.00);
INSERT INTO bdjardineria.cliente VALUES (3, 'Gardening Associates', 'Anne', 'Wright', '5557410345', '5557410346', 'Wall-e Avenue', NULL, 'Miami', 'Miami', 'EEUU', '24010', 19, 6000.00);
INSERT INTO bdjardineria.cliente VALUES (4, 'Gerudo Valley', 'Link', 'Flaute', '5552323129', '5552323128', 'Oaks Avenue nº22', NULL, 'New York', NULL, 'EEUU', '85495', 22, 12000.00);
INSERT INTO bdjardineria.cliente VALUES (5, 'Tendo Garden', 'Akane', 'Tendo', '55591233210', '55591233211', 'Null Street nº69', NULL, 'Miami', NULL, 'EEUU', '696969', 22, 600000.00);
INSERT INTO bdjardineria.cliente VALUES (6, 'Lasas S.A.', 'Antonio', 'Lasas', '34916540145', '34914851312', 'C/Leganes 15', NULL, 'Fuenlabrada', 'Madrid', 'España', '28945', 8, 154310.00);
INSERT INTO bdjardineria.cliente VALUES (7, 'Beragua', 'Jose', 'Bermejo', '654987321', '916549872', 'C/pintor segundo', 'Getafe', 'Madrid', 'Madrid', 'España', '28942', 11, 20000.00);
INSERT INTO bdjardineria.cliente VALUES (8, 'Club Golf Puerta del hierro', 'Paco', 'Lopez', '62456810', '919535678', 'C/sinesio delgado', 'Madrid', 'Madrid', 'Madrid', 'España', '28930', 11, 40000.00);
INSERT INTO bdjardineria.cliente VALUES (9, 'Naturagua', 'Guillermo', 'Rengifo', '689234750', '916428956', 'C/majadahonda', 'Boadilla', 'Madrid', 'Madrid', 'España', '28947', 11, 32000.00);
INSERT INTO bdjardineria.cliente VALUES (10, 'DaraDistribuciones', 'David', 'Serrano', '675598001', '916421756', 'C/azores', 'Fuenlabrada', 'Madrid', 'Madrid', 'España', '28946', 11, 50000.00);
INSERT INTO bdjardineria.cliente VALUES (11, 'Madrileña de riegos', 'Jose', 'Tacaño', '655983045', '916689215', 'C/Lagañas', 'Fuenlabrada', 'Madrid', 'Madrid', 'España', '28943', 11, 20000.00);
INSERT INTO bdjardineria.cliente VALUES (13, 'Camunas Jardines S.L.', 'Pedro', 'Camunas', '34914873241', '34914871541', 'C/Virgenes 45', 'C/Princesas 2 1ºB', 'San Lorenzo del Escorial', 'Madrid', 'España', '28145', 8, 16481.00);
INSERT INTO bdjardineria.cliente VALUES (14, 'Dardena S.A.', 'Juan', 'Rodriguez', '34912453217', '34912484764', 'C/Nueva York 74', NULL, 'Madrid', 'Madrid', 'España', '28003', 8, 321000.00);
INSERT INTO bdjardineria.cliente VALUES (15, 'Jardin de Flores', 'Javier', 'Villar', '654865643', '914538776', 'C/ Oña 34', NULL, 'Madrid', 'Madrid', 'España', '28950', 30, 40000.00);
INSERT INTO bdjardineria.cliente VALUES (16, 'Flores Marivi', 'Maria', 'Rodriguez', '666555444', '912458657', 'C/Leganes24', NULL, 'Fuenlabrada', 'Madrid', 'España', '28945', 5, 1500.00);
INSERT INTO bdjardineria.cliente VALUES (17, 'Flowers, S.A', 'Beatriz', 'Fernandez', '698754159', '978453216', 'C/Luis Salquillo4', NULL, 'Montornes del valles', 'Barcelona', 'España', '24586', 5, 3500.00);
INSERT INTO bdjardineria.cliente VALUES (18, 'Naturajardin', 'Victoria', 'Cruz', '612343529', '916548735', 'Plaza Magallón 15', NULL, 'Madrid', 'Madrid', 'España', '28011', 30, 5050.00);
INSERT INTO bdjardineria.cliente VALUES (19, 'Golf S.A.', 'Luis', 'Martinez', '916458762', '912354475', 'C/Estancado', NULL, 'Santa cruz de Tenerife', 'Islas Canarias', 'España', '38297', 12, 30000.00);
INSERT INTO bdjardineria.cliente VALUES (20, 'AYMERICH GOLF MANAGEMENT, SL', 'Mario', 'Suarez', '964493072', '964493063', 'C/Letardo', NULL, 'Barcelona', 'Cataluña', 'España', '12320', 12, 20000.00);
INSERT INTO bdjardineria.cliente VALUES (21, 'Aloha', 'Cristian', 'Rodrigez', '916485852', '914489898', 'C/Roman 3', NULL, 'Canarias', 'Canarias', 'España', '35488', 12, 50000.00);
INSERT INTO bdjardineria.cliente VALUES (22, 'El Prat', 'Francisco', 'Camacho', '916882323', '916493211', 'Avenida Tibidabo', NULL, ' Barcelona', 'Cataluña', 'España', '12320', 12, 30000.00);
INSERT INTO bdjardineria.cliente VALUES (23, 'Sotogrande', 'Maria', 'Santillana', '915576622', '914825645', 'C/Paseo del Parque', NULL, 'Sotogrande', 'Cadiz', 'España', '11310', 12, 60000.00);
INSERT INTO bdjardineria.cliente VALUES (24, 'Vivero Humanes', 'Federico', 'Gomez', '654987690', '916040875', 'C/Miguel Echegaray 54', NULL, 'Humanes', 'Madrid', 'España', '28970', 30, 7430.00);
INSERT INTO bdjardineria.cliente VALUES (25, 'Fuenla City', 'Tony', 'Muñoz Mena', '675842139', '915483754', 'C/Callo 52', NULL, 'Fuenlabrada', 'Madrid', 'España', '28574', 5, 4500.00);
INSERT INTO bdjardineria.cliente VALUES (26, 'Jardines y Mansiones CACTUS SL', 'Eva María', 'Sánchez', '916877445', '914477777', 'Polígono Industrial Maspalomas, Nº52', 'Móstoles', 'Madrid', 'Madrid', 'España', '29874', 9, 76000.00);
INSERT INTO bdjardineria.cliente VALUES (27, 'Jardinerías Matías SL', 'Matías', 'San Martín', '916544147', '917897474', 'C/Francisco Arce, Nº44', 'Bustarviejo', 'Madrid', 'Madrid', 'España', '37845', 9, 100500.00);
INSERT INTO bdjardineria.cliente VALUES (28, 'Agrojardin', 'Benito', 'Lopez', '675432926', '916549264', 'C/Mar Caspio 43', NULL, 'Getafe', 'Madrid', 'España', '28904', 30, 8040.00);
INSERT INTO bdjardineria.cliente VALUES (29, 'Top Campo', 'Joseluis', 'Sanchez', '685746512', '974315924', 'C/Ibiza 32', NULL, 'Humanes', 'Madrid', 'España', '28574', 5, 5500.00);
INSERT INTO bdjardineria.cliente VALUES (30, 'Jardineria Sara', 'Sara', 'Marquez', '675124537', '912475843', 'C/Lima 1', NULL, 'Fenlabrada', 'Madrid', 'España', '27584', 5, 7500.00);
INSERT INTO bdjardineria.cliente VALUES (31, 'Campohermoso', 'Luis', 'Jimenez', '645925376', '916159116', 'C/Peru 78', NULL, 'Fuenlabrada', 'Madrid', 'España', '28945', 30, 3250.00);
INSERT INTO bdjardineria.cliente VALUES (32, 'france telecom', 'FraÃ§ois', 'Toulou', '(33)5120578961', '(33)5120578961', '6 place d Alleray 15Ã¨me', NULL, 'Paris', NULL, 'Francia', '75010', 16, 10000.00);
INSERT INTO bdjardineria.cliente VALUES (33, 'Musée du Louvre', 'Pierre', 'Delacroux', '(33)0140205050', '(33)0140205442', 'Quai du Louvre', NULL, 'Paris', NULL, 'Francia', '75058', 16, 30000.00);
INSERT INTO bdjardineria.cliente VALUES (35, 'Tutifruti S.A', 'Jacob', 'Jones', '2 9261-2433', '2 9283-1695', 'level 24, St. Martins Tower.-31 Market St.', NULL, 'Sydney', 'Nueva Gales del Sur', 'Australia', '2000', 31, 10000.00);
INSERT INTO bdjardineria.cliente VALUES (36, 'FLORES S.L.', 'Antonio', 'Romero', '654352981', '685249700', 'Avenida España', NULL, 'Madrid', 'Fuenlabrada', 'España', '29643', 18, 6000.00);
INSERT INTO bdjardineria.cliente VALUES (37, 'THE MAGIC GARDEN', 'Richard', 'Mcain', '926523468', '9364875882', 'Lihgting Park', NULL, 'London', 'London', 'Reino Unido', '65930', 18, 10000.00);
INSERT INTO bdjardineria.cliente VALUES (38, 'El Jardin Viviente S.L', 'Justin', 'Smith', '2 8005-7161', '2 8005-7162', '176 Cumberland Street The rocks', NULL, 'Sydney', 'Nueva Gales del Sur', 'Australia', '2003', 31, 8000.00);


--
-- TOC entry 2277 (class 0 OID 18490)
-- Dependencies: 205
-- Data for Name: detallepedido; Type: TABLE DATA; Schema: bdjardineria; Owner: batoi
--

INSERT INTO bdjardineria.detallepedido VALUES (1, 'FR-67', 10, 70.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (1, 'OR-127', 40, 4.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (1, 'OR-141', 25, 4.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (1, 'OR-241', 15, 19.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (1, 'OR-99', 23, 14.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (2, 'FR-4', 3, 29.00, 6);
INSERT INTO bdjardineria.detallepedido VALUES (2, 'FR-40', 7, 8.00, 7);
INSERT INTO bdjardineria.detallepedido VALUES (2, 'OR-140', 50, 4.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (2, 'OR-141', 20, 5.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (2, 'OR-159', 12, 6.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (2, 'OR-227', 67, 64.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (2, 'OR-247', 5, 462.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (3, 'FR-48', 120, 9.00, 6);
INSERT INTO bdjardineria.detallepedido VALUES (3, 'OR-122', 32, 5.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (3, 'OR-123', 11, 5.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (3, 'OR-213', 30, 266.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (3, 'OR-217', 15, 65.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (3, 'OR-218', 24, 25.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (4, 'FR-31', 12, 8.00, 7);
INSERT INTO bdjardineria.detallepedido VALUES (4, 'FR-34', 42, 8.00, 6);
INSERT INTO bdjardineria.detallepedido VALUES (4, 'FR-40', 42, 9.00, 8);
INSERT INTO bdjardineria.detallepedido VALUES (4, 'OR-152', 3, 6.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (4, 'OR-155', 4, 6.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (4, 'OR-156', 17, 9.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (4, 'OR-157', 38, 10.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (4, 'OR-222', 21, 59.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (8, 'FR-106', 3, 11.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (8, 'FR-108', 1, 32.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (8, 'FR-11', 10, 100.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (9, 'AR-001', 80, 1.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (9, 'AR-008', 450, 1.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (9, 'FR-106', 80, 8.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (9, 'FR-69', 15, 91.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (10, 'FR-82', 5, 70.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (10, 'FR-91', 30, 75.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (10, 'OR-234', 5, 64.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (11, 'AR-006', 180, 1.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (11, 'OR-247', 80, 8.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (12, 'AR-009', 290, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (13, '11679', 5, 14.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (13, '21636', 12, 14.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (13, 'FR-11', 5, 100.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (14, 'FR-100', 8, 11.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (14, 'FR-13', 13, 57.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (15, 'FR-84', 4, 13.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (15, 'OR-101', 2, 6.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (15, 'OR-156', 6, 10.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (15, 'OR-203', 9, 10.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (16, '30310', 12, 12.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (16, 'FR-36', 10, 9.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (17, '11679', 5, 14.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (17, '22225', 5, 12.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (17, 'FR-37', 5, 9.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (17, 'FR-64', 5, 22.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (17, 'OR-136', 5, 18.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (18, '22225', 4, 12.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (18, 'FR-22', 2, 4.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (18, 'OR-159', 10, 6.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (19, '30310', 9, 12.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (19, 'FR-23', 6, 8.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (19, 'FR-75', 1, 32.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (19, 'FR-84', 5, 13.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (19, 'OR-208', 20, 4.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (20, '11679', 14, 14.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (20, '30310', 8, 12.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (21, '21636', 5, 14.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (21, 'FR-18', 22, 4.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (21, 'FR-53', 3, 8.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (22, 'OR-240', 1, 6.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (23, 'AR-002', 110, 1.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (23, 'FR-107', 50, 22.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (23, 'FR-85', 4, 70.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (23, 'OR-249', 30, 5.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (24, '22225', 3, 15.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (24, 'FR-1', 4, 7.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (24, 'FR-23', 2, 7.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (24, 'OR-241', 10, 20.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (25, 'FR-77', 15, 69.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (25, 'FR-9', 4, 30.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (25, 'FR-94', 10, 30.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (26, 'FR-15', 9, 25.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (26, 'OR-188', 4, 25.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (26, 'OR-218', 14, 25.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (27, 'OR-101', 22, 6.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (27, 'OR-102', 22, 6.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (27, 'OR-186', 40, 6.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (28, 'FR-11', 8, 99.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (28, 'OR-213', 3, 266.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (28, 'OR-247', 1, 462.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (29, 'FR-82', 4, 70.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (29, 'FR-9', 4, 28.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (29, 'FR-94', 20, 31.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (29, 'OR-129', 2, 111.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (29, 'OR-160', 10, 9.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (30, 'AR-004', 10, 1.00, 6);
INSERT INTO bdjardineria.detallepedido VALUES (30, 'FR-108', 2, 32.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (30, 'FR-12', 2, 19.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (30, 'FR-72', 4, 31.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (30, 'FR-89', 10, 45.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (30, 'OR-120', 5, 5.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (31, 'AR-009', 25, 2.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (31, 'FR-102', 1, 20.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (31, 'FR-4', 6, 29.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (32, '11679', 1, 14.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (32, '21636', 4, 15.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (32, '22225', 1, 15.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (32, 'OR-128', 29, 100.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (32, 'OR-193', 5, 20.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (33, 'FR-17', 423, 2.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (33, 'FR-29', 120, 8.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (33, 'OR-214', 212, 10.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (33, 'OR-247', 150, 462.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (34, 'FR-3', 56, 7.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (34, 'FR-7', 12, 29.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (34, 'OR-172', 20, 18.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (34, 'OR-174', 24, 18.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (35, '21636', 12, 14.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (35, 'FR-47', 55, 8.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (35, 'OR-165', 3, 10.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (35, 'OR-181', 36, 10.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (35, 'OR-225', 72, 10.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (36, '30310', 4, 12.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (36, 'FR-1', 2, 7.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (36, 'OR-147', 6, 7.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (36, 'OR-203', 1, 12.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (36, 'OR-99', 15, 13.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (37, 'FR-105', 4, 70.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (37, 'FR-57', 203, 8.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (37, 'OR-176', 38, 10.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (38, '11679', 5, 14.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (38, '21636', 2, 14.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (39, '22225', 3, 12.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (39, '30310', 6, 12.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (40, 'AR-001', 4, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (40, 'AR-002', 8, 1.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (41, 'AR-003', 5, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (41, 'AR-004', 5, 1.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (42, 'AR-005', 3, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (42, 'AR-006', 1, 1.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (43, 'AR-007', 9, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (44, 'AR-008', 5, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (45, 'AR-009', 6, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (45, 'AR-010', 4, 1.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (46, 'FR-1', 4, 7.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (46, 'FR-10', 8, 7.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (47, 'FR-100', 9, 11.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (47, 'FR-101', 5, 13.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (48, 'FR-102', 1, 18.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (48, 'FR-103', 1, 25.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (48, 'OR-234', 50, 64.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (48, 'OR-236', 45, 49.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (48, 'OR-237', 50, 19.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (49, 'OR-204', 50, 10.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (49, 'OR-205', 10, 10.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (49, 'OR-206', 5, 5.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (50, 'OR-225', 12, 10.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (50, 'OR-226', 15, 38.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (50, 'OR-227', 44, 64.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (51, 'OR-209', 50, 10.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (51, 'OR-210', 80, 39.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (51, 'OR-211', 70, 59.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (53, 'FR-2', 1, 7.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (53, 'FR-85', 1, 70.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (53, 'FR-86', 2, 11.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (53, 'OR-116', 6, 7.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (54, '11679', 3, 14.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (54, 'FR-100', 45, 10.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (54, 'FR-18', 5, 4.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (54, 'FR-79', 3, 22.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (54, 'OR-116', 8, 7.00, 6);
INSERT INTO bdjardineria.detallepedido VALUES (54, 'OR-123', 3, 5.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (54, 'OR-168', 2, 10.00, 7);
INSERT INTO bdjardineria.detallepedido VALUES (55, 'OR-115', 9, 7.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (55, 'OR-213', 2, 266.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (55, 'OR-227', 6, 64.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (55, 'OR-243', 2, 64.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (55, 'OR-247', 1, 462.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (56, 'OR-129', 1, 115.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (56, 'OR-130', 10, 18.00, 6);
INSERT INTO bdjardineria.detallepedido VALUES (56, 'OR-179', 1, 6.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (56, 'OR-196', 3, 10.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (56, 'OR-207', 4, 4.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (56, 'OR-250', 3, 10.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (57, 'FR-69', 6, 91.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (57, 'FR-81', 3, 49.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (57, 'FR-84', 2, 13.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (57, 'FR-94', 6, 9.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (58, 'OR-102', 65, 18.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (58, 'OR-139', 80, 4.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (58, 'OR-172', 69, 15.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (58, 'OR-177', 150, 15.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (74, 'FR-67', 15, 70.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (74, 'OR-227', 34, 64.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (74, 'OR-247', 42, 8.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (75, 'AR-006', 60, 1.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (75, 'FR-87', 24, 22.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (75, 'OR-157', 46, 10.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (76, 'AR-009', 250, 1.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (76, 'FR-79', 40, 22.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (76, 'FR-87', 24, 22.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (76, 'FR-94', 35, 9.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (76, 'OR-196', 25, 10.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (77, '22225', 34, 12.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (77, '30310', 15, 12.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (78, 'FR-53', 25, 8.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (78, 'FR-85', 56, 70.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (78, 'OR-157', 42, 10.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (78, 'OR-208', 30, 4.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (79, 'OR-240', 50, 6.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (80, 'FR-11', 40, 100.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (80, 'FR-36', 47, 9.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (80, 'OR-136', 75, 18.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (81, 'OR-208', 30, 4.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (82, 'OR-227', 34, 64.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (83, 'OR-208', 30, 4.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (89, 'FR-108', 3, 32.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (89, 'FR-3', 15, 7.00, 6);
INSERT INTO bdjardineria.detallepedido VALUES (89, 'FR-42', 12, 8.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (89, 'FR-66', 5, 49.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (89, 'FR-87', 4, 22.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (89, 'OR-157', 8, 10.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (90, 'AR-001', 19, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (90, 'AR-002', 10, 1.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (90, 'AR-003', 12, 1.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (91, 'FR-100', 52, 11.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (91, 'FR-101', 14, 13.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (91, 'FR-102', 35, 18.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (92, 'FR-108', 12, 23.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (92, 'FR-11', 20, 100.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (92, 'FR-12', 30, 21.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (93, 'FR-54', 25, 9.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (93, 'FR-58', 51, 11.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (93, 'FR-60', 3, 32.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (94, '11679', 12, 14.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (94, 'FR-11', 33, 100.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (94, 'FR-4', 79, 29.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (95, 'FR-10', 9, 7.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (95, 'FR-75', 6, 32.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (95, 'FR-82', 5, 70.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (96, 'FR-43', 6, 8.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (96, 'FR-6', 16, 7.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (96, 'FR-71', 10, 22.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (96, 'FR-90', 4, 70.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (97, 'FR-41', 12, 8.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (97, 'FR-54', 14, 9.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (97, 'OR-156', 10, 10.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (98, 'FR-33', 14, 8.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (98, 'FR-56', 16, 8.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (98, 'FR-60', 8, 32.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (98, 'FR-8', 18, 6.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (98, 'FR-85', 6, 70.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (99, 'OR-157', 15, 10.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (99, 'OR-227', 30, 64.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (100, 'FR-87', 20, 22.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (100, 'FR-94', 40, 9.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (101, 'AR-006', 50, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (101, 'AR-009', 159, 1.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (102, '22225', 32, 12.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (102, '30310', 23, 12.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (103, 'FR-53', 12, 8.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (103, 'OR-208', 52, 4.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (104, 'FR-85', 9, 70.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (104, 'OR-157', 113, 10.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (105, 'OR-227', 21, 64.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (105, 'OR-240', 27, 6.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (106, 'AR-009', 231, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (106, 'OR-136', 47, 18.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (107, '30310', 143, 12.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (107, 'FR-11', 15, 100.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (108, 'FR-53', 53, 8.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (108, 'OR-208', 59, 4.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (109, 'FR-22', 8, 4.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (109, 'FR-36', 12, 9.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (109, 'FR-45', 14, 8.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (109, 'OR-104', 20, 10.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (109, 'OR-119', 10, 5.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (109, 'OR-125', 3, 5.00, 6);
INSERT INTO bdjardineria.detallepedido VALUES (109, 'OR-130', 2, 18.00, 7);
INSERT INTO bdjardineria.detallepedido VALUES (110, 'AR-010', 6, 1.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (110, 'FR-1', 14, 7.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (110, 'FR-16', 1, 45.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (116, '21636', 5, 14.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (116, 'AR-001', 32, 1.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (116, 'AR-005', 18, 1.00, 5);
INSERT INTO bdjardineria.detallepedido VALUES (116, 'FR-33', 13, 8.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (116, 'OR-200', 10, 4.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (117, 'FR-78', 2, 15.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (117, 'FR-80', 1, 32.00, 3);
INSERT INTO bdjardineria.detallepedido VALUES (117, 'OR-146', 17, 4.00, 2);
INSERT INTO bdjardineria.detallepedido VALUES (117, 'OR-179', 4, 6.00, 4);
INSERT INTO bdjardineria.detallepedido VALUES (128, 'AR-004', 15, 1.00, 1);
INSERT INTO bdjardineria.detallepedido VALUES (128, 'OR-150', 18, 2.00, 2);


--
-- TOC entry 2278 (class 0 OID 18493)
-- Dependencies: 206
-- Data for Name: empleado; Type: TABLE DATA; Schema: bdjardineria; Owner: batoi
--

INSERT INTO bdjardineria.empleado VALUES (1, 'Marcos', 'Magaña', 'Perez', '3897', 'marcos@jardineria.es', 'TAL-ES', NULL, 'Director General');
INSERT INTO bdjardineria.empleado VALUES (2, 'Ruben', 'López', 'Martinez', '2899', 'rlopez@jardineria.es', 'TAL-ES', 1, 'Subdirector Marketing');
INSERT INTO bdjardineria.empleado VALUES (3, 'Alberto', 'Soria', 'Carrasco', '2837', 'asoria@jardineria.es', 'TAL-ES', 2, 'Subdirector Ventas');
INSERT INTO bdjardineria.empleado VALUES (4, 'Maria', 'Solís', 'Jerez', '2847', 'msolis@jardineria.es', 'TAL-ES', 2, 'Secretaria');
INSERT INTO bdjardineria.empleado VALUES (5, 'Felipe', 'Rosas', 'Marquez', '2844', 'frosas@jardineria.es', 'TAL-ES', 3, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (6, 'Juan Carlos', 'Ortiz', 'Serrano', '2845', 'cortiz@jardineria.es', 'TAL-ES', 3, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (7, 'Carlos', 'Soria', 'Jimenez', '2444', 'csoria@jardineria.es', 'MAD-ES', 3, 'Director oficina');
INSERT INTO bdjardineria.empleado VALUES (8, 'Mariano', 'López', 'Murcia', '2442', 'mlopez@jardineria.es', 'MAD-ES', 7, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (9, 'Lucio', 'Campoamor', 'Martín', '2442', 'lcampoamor@jardineria.es', 'MAD-ES', 7, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (10, 'Hilario', 'Rodriguez', 'Huertas', '2444', 'hrodriguez@jardineria.es', 'MAD-ES', 7, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (11, 'Emmanuel', 'Magaña', 'Perez', '2518', 'manu@jardineria.es', 'BCN-ES', 3, 'Director oficina');
INSERT INTO bdjardineria.empleado VALUES (12, 'José Manuel', 'Martinez', 'De la Osa', '2519', 'jmmart@hotmail.es', 'BCN-ES', 11, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (13, 'David', 'Palma', 'Aceituno', '2519', 'dpalma@jardineria.es', 'BCN-ES', 11, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (14, 'Oscar', 'Palma', 'Aceituno', '2519', 'opalma@jardineria.es', 'BCN-ES', 11, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (15, 'Francois', 'Fignon', '', '9981', 'ffignon@gardening.com', 'PAR-FR', 3, 'Director oficina');
INSERT INTO bdjardineria.empleado VALUES (16, 'Lionel', 'Narvaez', '', '9982', 'lnarvaez@gardening.com', 'PAR-FR', 15, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (17, 'Laurent', 'Serra', '', '9982', 'lserra@gardening.com', 'PAR-FR', 15, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (18, 'Michael', 'Bolton', '', '7454', 'mbolton@gardening.com', 'SFC-USA', 3, 'Director oficina');
INSERT INTO bdjardineria.empleado VALUES (19, 'Walter Santiago', 'Sanchez', 'Lopez', '7454', 'wssanchez@gardening.com', 'SFC-USA', 18, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (20, 'Hilary', 'Washington', '', '7565', 'hwashington@gardening.com', 'BOS-USA', 3, 'Director oficina');
INSERT INTO bdjardineria.empleado VALUES (21, 'Marcus', 'Paxton', '', '7565', 'mpaxton@gardening.com', 'BOS-USA', 20, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (22, 'Lorena', 'Paxton', '', '7665', 'lpaxton@gardening.com', 'BOS-USA', 20, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (23, 'Nei', 'Nishikori', '', '8734', 'nnishikori@gardening.com', 'TOK-JP', 3, 'Director oficina');
INSERT INTO bdjardineria.empleado VALUES (24, 'Narumi', 'Riko', '', '8734', 'nriko@gardening.com', 'TOK-JP', 23, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (25, 'Takuma', 'Nomura', '', '8735', 'tnomura@gardening.com', 'TOK-JP', 23, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (26, 'Amy', 'Johnson', '', '3321', 'ajohnson@gardening.com', 'LON-UK', 3, 'Director oficina');
INSERT INTO bdjardineria.empleado VALUES (27, 'Larry', 'Westfalls', '', '3322', 'lwestfalls@gardening.com', 'LON-UK', 26, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (28, 'John', 'Walton', '', '3322', 'jwalton@gardening.com', 'LON-UK', 26, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (29, 'Kevin', 'Fallmer', '', '3210', 'kfalmer@gardening.com', 'SYD-AU', 3, 'Director oficina');
INSERT INTO bdjardineria.empleado VALUES (30, 'Julian', 'Bellinelli', '', '3211', 'jbellinelli@gardening.com', 'SYD-AU', 29, 'Representante Ventas');
INSERT INTO bdjardineria.empleado VALUES (31, 'Mariko', 'Kishi', '', '3211', 'mkishi@gardening.com', 'SYD-AU', 29, 'Representante Ventas');


--
-- TOC entry 2279 (class 0 OID 18496)
-- Dependencies: 207
-- Data for Name: oficina; Type: TABLE DATA; Schema: bdjardineria; Owner: batoi
--

INSERT INTO bdjardineria.oficina VALUES ('BCN-ES', 'Barcelona', 'España', 'Barcelona', '08019', '+34 93 3561182', 'Avenida Diagonal, 38', '3A escalera Derecha');
INSERT INTO bdjardineria.oficina VALUES ('BOS-USA', 'Boston', 'EEUU', 'MA', '02108', '+1 215 837 0825', '1550 Court Place', 'Suite 102');
INSERT INTO bdjardineria.oficina VALUES ('LON-UK', 'Londres', 'Reino Unido', 'EMEA', 'EC2N 1HN', '+44 20 78772041', '52 Old Broad Street', 'Ground Floor');
INSERT INTO bdjardineria.oficina VALUES ('MAD-ES', 'Madrid', 'España', 'Madrid', '28032', '+34 91 7514487', 'Bulevar Indalecio Prieto, 32', '');
INSERT INTO bdjardineria.oficina VALUES ('PAR-FR', 'Paris', 'Francia', 'EMEA', '75017', '+33 14 723 4404', '29 Rue Jouffroy d''abbans', '');
INSERT INTO bdjardineria.oficina VALUES ('SFC-USA', 'San Francisco', 'EEUU', 'CA', '94080', '+1 650 219 4782', '100 Market Street', 'Suite 300');
INSERT INTO bdjardineria.oficina VALUES ('SYD-AU', 'Sydney', 'Australia', 'APAC', 'NSW 2010', '+61 2 9264 2451', '5-11 Wentworth Avenue', 'Floor #2');
INSERT INTO bdjardineria.oficina VALUES ('TAL-ES', 'Talavera de la Reina', 'España', 'Castilla-LaMancha', '45632', '+34 925 867231', 'Francisco Aguirre, 32', '5º piso (exterior)');
INSERT INTO bdjardineria.oficina VALUES ('TOK-JP', 'Tokyo', 'Japón', 'Chiyoda-Ku', '102-8578', '+81 33 224 5000', '4-1 Kioicho', '');


--
-- TOC entry 2280 (class 0 OID 18499)
-- Dependencies: 208
-- Data for Name: pago; Type: TABLE DATA; Schema: bdjardineria; Owner: batoi
--

INSERT INTO bdjardineria.pago VALUES (3,1,'Cheque','ak-std-002488','2018-06-20',2170.00),(3,2,'Tarjeta','ak-std-001285','2018-06-22',8680.00),(8,1,'Tarjeta','ak-std-006777','2018-11-12',1065.00),(9,1,'Tarjeta','ak-std-004147','2018-12-23',0.00),(9,2,'Tarjeta','ak-std-007773','2018-12-22',2535.00),(10,1,'Tarjeta','ak-std-006175','2019-01-16',2920.00),(13,1,'Transferencia','ak-std-008851','2019-01-12',664.20),(13,2,'Transferencia','ak-std-002506','2019-01-15',73.80),(14,1,'Tarjeta','ak-std-002313','2019-01-03',829.00),(15,1,'Tarjeta','ak-std-001875','2019-01-11',128.40),(15,2,'Tarjeta','ak-std-000782','2019-01-12',85.60),(16,1,'Cheque','ak-std-002847','2019-01-08',234.00),(17,1,'Transferencia','ak-std-009167','2019-01-09',337.50),(17,2,'Cheque','ak-std-002177','2019-01-08',37.50),(21,1,'Tarjeta','ak-std-004837','2019-01-10',72.80),(21,2,'Cheque','ak-std-002351','2019-01-10',109.20),(22,1,'Cheque','ak-std-005452','2019-01-13',6.00),(24,1,'Tarjeta','ak-std-009971','2018-07-17',287.00),(25,1,'Tarjeta','ak-std-008468','2019-02-03',436.50),(25,2,'PayPal','ak-std-001129','2019-02-03',1018.50),(26,1,'Tarjeta','ak-std-005885','2019-02-06',675.00),(28,1,'Tarjeta','ak-std-003431','2019-02-12',2052.00),(29,1,'Tarjeta','ak-std-001303','2018-08-03',662.00),(29,2,'Tarjeta','ak-std-001111','2018-08-03',662.00),(30,1,'Cheque','ak-std-009865','2018-08-04',711.00),(31,1,'Cheque','ak-std-008231','2018-09-04',170.80),(31,2,'Cheque','ak-std-005314','2018-09-06',73.20),(32,1,'PayPal','ak-std-004316','2017-01-08',3089.00),(33,1,'Transferencia','ak-std-001292','2017-05-22',51258.20),(33,2,'Cheque','ak-std-002377','2017-05-22',21967.80),(34,1,'Tarjeta','ak-std-000532','2017-06-21',1532.00),(38,1,'Tarjeta','ak-std-004076','2019-03-08',98.00),(39,1,'Tarjeta','ak-std-004108','2019-03-08',10.80),(39,2,'Cheque','ak-std-005226','2019-03-08',97.20),(40,1,'Cheque','ak-std-009675','2019-03-10',12.00),(42,1,'Transferencia','ak-std-002035','2019-03-24',4.00),(45,1,'PayPal','ak-std-006230','2019-04-01',6.00),(45,2,'PayPal','ak-std-001685','2019-04-03',4.00),(49,1,'Tarjeta','ak-std-004363','2018-07-14',312.50),(49,2,'Cheque','ak-std-003166','2018-07-13',312.50),(51,1,'Transferencia','ak-std-002195','2018-10-02',4650.00),(51,2,'Cheque','ak-std-002517','2018-10-04',3100.00),(55,1,'Transferencia','ak-std-005851','2018-12-11',470.70),(55,2,'Tarjeta','ak-std-007098','2018-12-12',1098.30),(58,1,'Transferencia','ak-std-008700','2019-01-27',4775.00),(74,1,'Cheque','ak-std-004115','2019-01-17',3562.00),(75,1,'Transferencia','ak-std-008656','2019-01-12',943.20),(75,2,'PayPal','ak-std-009218','2019-01-14',104.80),(77,1,'Tarjeta','ak-std-004278','2019-01-04',411.60),(77,2,'PayPal','ak-std-004357','2019-01-04',176.40),(79,1,'Cheque','ak-std-002243','2019-01-14',210.00),(79,2,'Transferencia','ak-std-005821','2019-01-13',90.00),(81,1,'Transferencia','ak-std-007265','2019-01-18',60.00),(81,2,'Tarjeta','ak-std-008054','2019-01-18',60.00),(89,1,'Tarjeta','ak-std-006013','2017-10-05',355.00),(89,2,'Tarjeta','ak-std-007120','2017-10-08',355.00),(92,1,'Tarjeta','ak-std-003204','2019-04-21',2906.00),(95,1,'Tarjeta','ak-std-008676','2018-01-04',302.50),(95,2,'Tarjeta','ak-std-006287','2018-01-05',302.50),(99,1,'PayPal','ak-std-007362','2019-02-16',828.00),(99,2,'Tarjeta','ak-std-002727','2019-02-15',1242.00),(100,1,'Transferencia','ak-std-005623','2019-01-12',800.00),(101,1,'Transferencia','ak-std-003858','2019-03-08',62.70),(101,2,'Tarjeta','ak-std-009624','2019-03-10',146.30),(102,1,'PayPal','ak-std-001621','2018-12-30',660.00),(105,1,'Transferencia','ak-std-007323','2019-02-15',1204.80),(105,2,'Cheque','ak-std-001477','2019-02-15',301.20),(106,1,'Cheque','ak-std-003193','2019-05-16',1077.00),(107,1,'Cheque','ak-std-003795','2019-04-06',643.20),(107,2,'PayPal','ak-std-008543','2019-04-09',2572.80),(109,1,'Tarjeta','ak-std-004329','2016-05-26',0.00),(109,2,'Tarjeta','ak-std-001324','2016-05-27',553.00),(116,1,'PayPal','ak-std-006565','2018-06-30',264.00);



--
-- TOC entry 2281 (class 0 OID 18503)
-- Dependencies: 209
-- Data for Name: pais; Type: TABLE DATA; Schema: bdjardineria; Owner: batoi
--

INSERT INTO bdjardineria.pais VALUES ('Afganistán');
INSERT INTO bdjardineria.pais VALUES ('Albania');
INSERT INTO bdjardineria.pais VALUES ('Alemania');
INSERT INTO bdjardineria.pais VALUES ('Algeria');
INSERT INTO bdjardineria.pais VALUES ('Andorra');
INSERT INTO bdjardineria.pais VALUES ('Angola');
INSERT INTO bdjardineria.pais VALUES ('Anguila');
INSERT INTO bdjardineria.pais VALUES ('Antártida');
INSERT INTO bdjardineria.pais VALUES ('Antigua y Barbuda');
INSERT INTO bdjardineria.pais VALUES ('Arabia Saudita');
INSERT INTO bdjardineria.pais VALUES ('Argentina');
INSERT INTO bdjardineria.pais VALUES ('Armenia');
INSERT INTO bdjardineria.pais VALUES ('Aruba');
INSERT INTO bdjardineria.pais VALUES ('Australia');
INSERT INTO bdjardineria.pais VALUES ('Austria');
INSERT INTO bdjardineria.pais VALUES ('Azerbaiyán');
INSERT INTO bdjardineria.pais VALUES ('Bahamas');
INSERT INTO bdjardineria.pais VALUES ('Bahrein');
INSERT INTO bdjardineria.pais VALUES ('Bangladesh');
INSERT INTO bdjardineria.pais VALUES ('Barbados');
INSERT INTO bdjardineria.pais VALUES ('Bélgica');
INSERT INTO bdjardineria.pais VALUES ('Belice');
INSERT INTO bdjardineria.pais VALUES ('Benín');
INSERT INTO bdjardineria.pais VALUES ('Bhután');
INSERT INTO bdjardineria.pais VALUES ('Bielorrusia');
INSERT INTO bdjardineria.pais VALUES ('Birmania');
INSERT INTO bdjardineria.pais VALUES ('Bolivia');
INSERT INTO bdjardineria.pais VALUES ('Bosnia y Herzegovina');
INSERT INTO bdjardineria.pais VALUES ('Botsuana');
INSERT INTO bdjardineria.pais VALUES ('Brasil');
INSERT INTO bdjardineria.pais VALUES ('Brunéi');
INSERT INTO bdjardineria.pais VALUES ('Bulgaria');
INSERT INTO bdjardineria.pais VALUES ('Burkina Faso');
INSERT INTO bdjardineria.pais VALUES ('Burundi');
INSERT INTO bdjardineria.pais VALUES ('Cabo Verde');
INSERT INTO bdjardineria.pais VALUES ('Camboya');
INSERT INTO bdjardineria.pais VALUES ('Camerún');
INSERT INTO bdjardineria.pais VALUES ('Canadá');
INSERT INTO bdjardineria.pais VALUES ('Chad');
INSERT INTO bdjardineria.pais VALUES ('Chile');
INSERT INTO bdjardineria.pais VALUES ('China');
INSERT INTO bdjardineria.pais VALUES ('Chipre');
INSERT INTO bdjardineria.pais VALUES ('Ciudad del Vaticano');
INSERT INTO bdjardineria.pais VALUES ('Colombia');
INSERT INTO bdjardineria.pais VALUES ('Comoras');
INSERT INTO bdjardineria.pais VALUES ('Corea del Norte');
INSERT INTO bdjardineria.pais VALUES ('Corea del Sur');
INSERT INTO bdjardineria.pais VALUES ('Costa de Marfil');
INSERT INTO bdjardineria.pais VALUES ('Costa Rica');
INSERT INTO bdjardineria.pais VALUES ('Croacia');
INSERT INTO bdjardineria.pais VALUES ('Cuba');
INSERT INTO bdjardineria.pais VALUES ('Curazao');
INSERT INTO bdjardineria.pais VALUES ('Dinamarca');
INSERT INTO bdjardineria.pais VALUES ('Dominica');
INSERT INTO bdjardineria.pais VALUES ('Ecuador');
INSERT INTO bdjardineria.pais VALUES ('EEUU');
INSERT INTO bdjardineria.pais VALUES ('Egipto');
INSERT INTO bdjardineria.pais VALUES ('El Salvador');
INSERT INTO bdjardineria.pais VALUES ('Emiratos Árabes Unidos');
INSERT INTO bdjardineria.pais VALUES ('Eritrea');
INSERT INTO bdjardineria.pais VALUES ('Eslovaquia');
INSERT INTO bdjardineria.pais VALUES ('Eslovenia');
INSERT INTO bdjardineria.pais VALUES ('España');
INSERT INTO bdjardineria.pais VALUES ('Estonia');
INSERT INTO bdjardineria.pais VALUES ('Etiopía');
INSERT INTO bdjardineria.pais VALUES ('Filipinas');
INSERT INTO bdjardineria.pais VALUES ('Finlandia');
INSERT INTO bdjardineria.pais VALUES ('Fiyi');
INSERT INTO bdjardineria.pais VALUES ('Francia');
INSERT INTO bdjardineria.pais VALUES ('Gabón');
INSERT INTO bdjardineria.pais VALUES ('Gambia');
INSERT INTO bdjardineria.pais VALUES ('Georgia');
INSERT INTO bdjardineria.pais VALUES ('Ghana');
INSERT INTO bdjardineria.pais VALUES ('Gibraltar');
INSERT INTO bdjardineria.pais VALUES ('Granada');
INSERT INTO bdjardineria.pais VALUES ('Grecia');
INSERT INTO bdjardineria.pais VALUES ('Groenlandia');
INSERT INTO bdjardineria.pais VALUES ('Guadalupe');
INSERT INTO bdjardineria.pais VALUES ('Guam');
INSERT INTO bdjardineria.pais VALUES ('Guatemala');
INSERT INTO bdjardineria.pais VALUES ('Guayana Francesa');
INSERT INTO bdjardineria.pais VALUES ('Guernsey');
INSERT INTO bdjardineria.pais VALUES ('Guinea');
INSERT INTO bdjardineria.pais VALUES ('Guinea Ecuatorial');
INSERT INTO bdjardineria.pais VALUES ('Guinea-Bissau');
INSERT INTO bdjardineria.pais VALUES ('Guyana');
INSERT INTO bdjardineria.pais VALUES ('Haití');
INSERT INTO bdjardineria.pais VALUES ('Honduras');
INSERT INTO bdjardineria.pais VALUES ('Hong kong');
INSERT INTO bdjardineria.pais VALUES ('Hungría');
INSERT INTO bdjardineria.pais VALUES ('India');
INSERT INTO bdjardineria.pais VALUES ('Indonesia');
INSERT INTO bdjardineria.pais VALUES ('Irak');
INSERT INTO bdjardineria.pais VALUES ('Irán');
INSERT INTO bdjardineria.pais VALUES ('Irlanda');
INSERT INTO bdjardineria.pais VALUES ('Isla Bouvet');
INSERT INTO bdjardineria.pais VALUES ('Isla de Man');
INSERT INTO bdjardineria.pais VALUES ('Isla de Navidad');
INSERT INTO bdjardineria.pais VALUES ('Isla Norfolk');
INSERT INTO bdjardineria.pais VALUES ('Islandia');
INSERT INTO bdjardineria.pais VALUES ('Islas Bermudas');
INSERT INTO bdjardineria.pais VALUES ('Islas Caimán');
INSERT INTO bdjardineria.pais VALUES ('Islas Cocos (Keeling)');
INSERT INTO bdjardineria.pais VALUES ('Islas Cook');
INSERT INTO bdjardineria.pais VALUES ('Islas de Åland');
INSERT INTO bdjardineria.pais VALUES ('Islas Feroe');
INSERT INTO bdjardineria.pais VALUES ('Islas Georgias del Sur y Sandwich del Sur');
INSERT INTO bdjardineria.pais VALUES ('Islas Heard y McDonald');
INSERT INTO bdjardineria.pais VALUES ('Islas Maldivas');
INSERT INTO bdjardineria.pais VALUES ('Islas Malvinas');
INSERT INTO bdjardineria.pais VALUES ('Islas Marianas del Norte');
INSERT INTO bdjardineria.pais VALUES ('Islas Marshall');
INSERT INTO bdjardineria.pais VALUES ('Islas Pitcairn');
INSERT INTO bdjardineria.pais VALUES ('Islas Salomón');
INSERT INTO bdjardineria.pais VALUES ('Islas Turcas y Caicos');
INSERT INTO bdjardineria.pais VALUES ('Islas Ultramarinas Menores de Estados Unidos');
INSERT INTO bdjardineria.pais VALUES ('Islas Vírgenes Británicas');
INSERT INTO bdjardineria.pais VALUES ('Islas Vírgenes de los Estados Unidos');
INSERT INTO bdjardineria.pais VALUES ('Israel');
INSERT INTO bdjardineria.pais VALUES ('Italia');
INSERT INTO bdjardineria.pais VALUES ('Jamaica');
INSERT INTO bdjardineria.pais VALUES ('Japón');
INSERT INTO bdjardineria.pais VALUES ('Jersey');
INSERT INTO bdjardineria.pais VALUES ('Jordania');
INSERT INTO bdjardineria.pais VALUES ('Kazajistán');
INSERT INTO bdjardineria.pais VALUES ('Kenia');
INSERT INTO bdjardineria.pais VALUES ('Kirguistán');
INSERT INTO bdjardineria.pais VALUES ('Kiribati');
INSERT INTO bdjardineria.pais VALUES ('Kuwait');
INSERT INTO bdjardineria.pais VALUES ('Laos');
INSERT INTO bdjardineria.pais VALUES ('Lesoto');
INSERT INTO bdjardineria.pais VALUES ('Letonia');
INSERT INTO bdjardineria.pais VALUES ('Líbano');
INSERT INTO bdjardineria.pais VALUES ('Liberia');
INSERT INTO bdjardineria.pais VALUES ('Libia');
INSERT INTO bdjardineria.pais VALUES ('Liechtenstein');
INSERT INTO bdjardineria.pais VALUES ('Lituania');
INSERT INTO bdjardineria.pais VALUES ('Luxemburgo');
INSERT INTO bdjardineria.pais VALUES ('Macao');
INSERT INTO bdjardineria.pais VALUES ('Macedônia');
INSERT INTO bdjardineria.pais VALUES ('Madagascar');
INSERT INTO bdjardineria.pais VALUES ('Malasia');
INSERT INTO bdjardineria.pais VALUES ('Malawi');
INSERT INTO bdjardineria.pais VALUES ('Mali');
INSERT INTO bdjardineria.pais VALUES ('Malta');
INSERT INTO bdjardineria.pais VALUES ('Marruecos');
INSERT INTO bdjardineria.pais VALUES ('Martinica');
INSERT INTO bdjardineria.pais VALUES ('Mauricio');
INSERT INTO bdjardineria.pais VALUES ('Mauritania');
INSERT INTO bdjardineria.pais VALUES ('Mayotte');
INSERT INTO bdjardineria.pais VALUES ('México');
INSERT INTO bdjardineria.pais VALUES ('Micronesia');
INSERT INTO bdjardineria.pais VALUES ('Moldavia');
INSERT INTO bdjardineria.pais VALUES ('Mónaco');
INSERT INTO bdjardineria.pais VALUES ('Mongolia');
INSERT INTO bdjardineria.pais VALUES ('Montenegro');
INSERT INTO bdjardineria.pais VALUES ('Montserrat');
INSERT INTO bdjardineria.pais VALUES ('Mozambique');
INSERT INTO bdjardineria.pais VALUES ('Namibia');
INSERT INTO bdjardineria.pais VALUES ('Nauru');
INSERT INTO bdjardineria.pais VALUES ('Nepal');
INSERT INTO bdjardineria.pais VALUES ('Nicaragua');
INSERT INTO bdjardineria.pais VALUES ('Niger');
INSERT INTO bdjardineria.pais VALUES ('Nigeria');
INSERT INTO bdjardineria.pais VALUES ('Niue');
INSERT INTO bdjardineria.pais VALUES ('Noruega');
INSERT INTO bdjardineria.pais VALUES ('Nueva Caledonia');
INSERT INTO bdjardineria.pais VALUES ('Nueva Zelanda');
INSERT INTO bdjardineria.pais VALUES ('Omán');
INSERT INTO bdjardineria.pais VALUES ('Países Bajos');
INSERT INTO bdjardineria.pais VALUES ('Pakistán');
INSERT INTO bdjardineria.pais VALUES ('Palau');
INSERT INTO bdjardineria.pais VALUES ('Palestina');
INSERT INTO bdjardineria.pais VALUES ('Panamá');
INSERT INTO bdjardineria.pais VALUES ('Papúa Nueva Guinea');
INSERT INTO bdjardineria.pais VALUES ('Paraguay');
INSERT INTO bdjardineria.pais VALUES ('Perú');
INSERT INTO bdjardineria.pais VALUES ('Polinesia Francesa');
INSERT INTO bdjardineria.pais VALUES ('Polonia');
INSERT INTO bdjardineria.pais VALUES ('Portugal');
INSERT INTO bdjardineria.pais VALUES ('Puerto Rico');
INSERT INTO bdjardineria.pais VALUES ('Qatar');
INSERT INTO bdjardineria.pais VALUES ('Reino Unido');
INSERT INTO bdjardineria.pais VALUES ('República Centroafricana');
INSERT INTO bdjardineria.pais VALUES ('República Checa');
INSERT INTO bdjardineria.pais VALUES ('República de Sudán del Sur');
INSERT INTO bdjardineria.pais VALUES ('República del Congo');
INSERT INTO bdjardineria.pais VALUES ('República Democrática del Congo');
INSERT INTO bdjardineria.pais VALUES ('República Dominicana');
INSERT INTO bdjardineria.pais VALUES ('Reunión');
INSERT INTO bdjardineria.pais VALUES ('Ruanda');
INSERT INTO bdjardineria.pais VALUES ('Rumanía');
INSERT INTO bdjardineria.pais VALUES ('Rusia');
INSERT INTO bdjardineria.pais VALUES ('Sahara Occidental');
INSERT INTO bdjardineria.pais VALUES ('Samoa');
INSERT INTO bdjardineria.pais VALUES ('Samoa Americana');
INSERT INTO bdjardineria.pais VALUES ('San Bartolomé');
INSERT INTO bdjardineria.pais VALUES ('San Cristóbal y Nieves');
INSERT INTO bdjardineria.pais VALUES ('San Marino');
INSERT INTO bdjardineria.pais VALUES ('San Martín (Francia)');
INSERT INTO bdjardineria.pais VALUES ('San Pedro y Miquelón');
INSERT INTO bdjardineria.pais VALUES ('San Vicente y las Granadinas');
INSERT INTO bdjardineria.pais VALUES ('Santa Elena');
INSERT INTO bdjardineria.pais VALUES ('Santa Lucía');
INSERT INTO bdjardineria.pais VALUES ('Santo Tomé y Príncipe');
INSERT INTO bdjardineria.pais VALUES ('Senegal');
INSERT INTO bdjardineria.pais VALUES ('Serbia');
INSERT INTO bdjardineria.pais VALUES ('Seychelles');
INSERT INTO bdjardineria.pais VALUES ('Sierra Leona');
INSERT INTO bdjardineria.pais VALUES ('Singapur');
INSERT INTO bdjardineria.pais VALUES ('Sint Maarten');
INSERT INTO bdjardineria.pais VALUES ('Siria');
INSERT INTO bdjardineria.pais VALUES ('Somalia');
INSERT INTO bdjardineria.pais VALUES ('Sri lanka');
INSERT INTO bdjardineria.pais VALUES ('Sudáfrica');
INSERT INTO bdjardineria.pais VALUES ('Sudán');
INSERT INTO bdjardineria.pais VALUES ('Suecia');
INSERT INTO bdjardineria.pais VALUES ('Suiza');
INSERT INTO bdjardineria.pais VALUES ('Surinám');
INSERT INTO bdjardineria.pais VALUES ('Svalbard y Jan Mayen');
INSERT INTO bdjardineria.pais VALUES ('Swazilandia');
INSERT INTO bdjardineria.pais VALUES ('Tailandia');
INSERT INTO bdjardineria.pais VALUES ('Taiwán');
INSERT INTO bdjardineria.pais VALUES ('Tanzania');
INSERT INTO bdjardineria.pais VALUES ('Tayikistán');
INSERT INTO bdjardineria.pais VALUES ('Territorio Británico del Océano Índico');
INSERT INTO bdjardineria.pais VALUES ('Territorios Australes y Antárticas Franceses');
INSERT INTO bdjardineria.pais VALUES ('Timor Oriental');
INSERT INTO bdjardineria.pais VALUES ('Togo');
INSERT INTO bdjardineria.pais VALUES ('Tokelau');
INSERT INTO bdjardineria.pais VALUES ('Tonga');
INSERT INTO bdjardineria.pais VALUES ('Trinidad y Tobago');
INSERT INTO bdjardineria.pais VALUES ('Tunez');
INSERT INTO bdjardineria.pais VALUES ('Turkmenistán');
INSERT INTO bdjardineria.pais VALUES ('Turquía');
INSERT INTO bdjardineria.pais VALUES ('Tuvalu');
INSERT INTO bdjardineria.pais VALUES ('Ucrania');
INSERT INTO bdjardineria.pais VALUES ('Uganda');
INSERT INTO bdjardineria.pais VALUES ('Uruguay');
INSERT INTO bdjardineria.pais VALUES ('Uzbekistán');
INSERT INTO bdjardineria.pais VALUES ('Vanuatu');
INSERT INTO bdjardineria.pais VALUES ('Venezuela');
INSERT INTO bdjardineria.pais VALUES ('Vietnam');
INSERT INTO bdjardineria.pais VALUES ('Wallis y Futuna');
INSERT INTO bdjardineria.pais VALUES ('Yemen');
INSERT INTO bdjardineria.pais VALUES ('Yibuti');
INSERT INTO bdjardineria.pais VALUES ('Zambia');
INSERT INTO bdjardineria.pais VALUES ('Zimbabue');


--
-- TOC entry 2282 (class 0 OID 18506)
-- Dependencies: 210
-- Data for Name: pedido; Type: TABLE DATA; Schema: bdjardineria; Owner: batoi
--

INSERT INTO bdjardineria.pedido VALUES (1, '2016-01-17', '2016-01-19', '2016-01-19', 'Entregado', 'Pagado a plazos', 5);
INSERT INTO bdjardineria.pedido VALUES (2, '2017-10-23', '2017-10-28', '2017-10-26', 'Entregado', 'La entrega llego antes de lo esperado', 5);
INSERT INTO bdjardineria.pedido VALUES (3, '2018-06-20', '2018-06-25', NULL, 'Entregado', 'Limite de credito superado', 5);
INSERT INTO bdjardineria.pedido VALUES (4, '2019-01-20', '2019-01-26', NULL, 'Pendiente', NULL, 5);
INSERT INTO bdjardineria.pedido VALUES (8, '2018-11-09', '2018-11-14', '2018-11-14', 'Entregado', 'El cliente paga la mitad con tarjeta y la otra mitad con efectivo, se le realizan dos facturas', 1);
INSERT INTO bdjardineria.pedido VALUES (9, '2018-12-22', '2018-12-27', '2018-12-28', 'Entregado', 'El cliente comprueba la integridad del paquete, todo correcto', 1);
INSERT INTO bdjardineria.pedido VALUES (10, '2019-01-15', '2019-01-20', NULL, 'Pendiente', 'El cliente llama para confirmar la fecha - Esperando al proveedor', 3);
INSERT INTO bdjardineria.pedido VALUES (11, '2019-01-20', '2019-01-27', NULL, 'Pendiente', 'El cliente requiere que el pedido se le entregue de 16:00h a 22:00h', 1);
INSERT INTO bdjardineria.pedido VALUES (12, '2019-01-22', '2019-01-27', NULL, 'Pendiente', 'El cliente requiere que el pedido se le entregue de 9:00h a 13:00h', 1);
INSERT INTO bdjardineria.pedido VALUES (13, '2019-01-12', '2019-01-14', '2019-01-15', 'Entregado', NULL, 7);
INSERT INTO bdjardineria.pedido VALUES (14, '2019-01-02', '2019-01-02', NULL, 'Entregado', 'mal pago', 7);
INSERT INTO bdjardineria.pedido VALUES (15, '2019-01-09', '2019-01-12', '2019-01-11', 'Entregado', NULL, 7);
INSERT INTO bdjardineria.pedido VALUES (16, '2019-01-06', '2019-01-07', '2019-01-15', 'Entregado', NULL, 7);
INSERT INTO bdjardineria.pedido VALUES (17, '2019-01-08', '2019-01-09', '2019-01-11', 'Entregado', 'mal estado', 7);
INSERT INTO bdjardineria.pedido VALUES (18, '2019-01-05', '2019-01-06', '2019-01-07', 'Entregado', NULL, 9);
INSERT INTO bdjardineria.pedido VALUES (19, '2019-01-18', '2019-02-12', NULL, 'Pendiente', 'entregar en murcia', 9);
INSERT INTO bdjardineria.pedido VALUES (20, '2019-01-20', '2019-02-15', NULL, 'Pendiente', NULL, 9);
INSERT INTO bdjardineria.pedido VALUES (21, '2019-01-09', '2019-01-09', '2019-01-09', 'Rechazado', 'mal pago', 9);
INSERT INTO bdjardineria.pedido VALUES (22, '2019-01-11', '2019-01-11', '2019-01-13', 'Entregado', NULL, 9);
INSERT INTO bdjardineria.pedido VALUES (23, '2018-12-30', '2019-01-10', NULL, 'Entregado', 'Hacer entrega preferiblemente por la mañana', 5);
INSERT INTO bdjardineria.pedido VALUES (24, '2018-07-14', '2018-07-31', '2018-07-25', 'Entregado', NULL, 14);
INSERT INTO bdjardineria.pedido VALUES (25, '2019-02-02', '2019-02-08', NULL, 'Entregado', 'El cliente carece de saldo en la cuenta asociada', 1);
INSERT INTO bdjardineria.pedido VALUES (26, '2019-02-06', '2019-02-12', NULL, 'Rechazado', 'El cliente anula la operacion para adquirir mas producto', 3);
INSERT INTO bdjardineria.pedido VALUES (27, '2019-02-07', '2019-02-13', NULL, 'Entregado', 'El pedido aparece como entregado pero no sabemos en que fecha', 3);
INSERT INTO bdjardineria.pedido VALUES (28, '2019-02-10', '2019-02-17', '2019-02-20', 'Entregado', 'El cliente se queja bastante de la espera asociada al producto', 3);
INSERT INTO bdjardineria.pedido VALUES (29, '2018-08-01', '2018-09-01', '2018-09-01', 'Rechazado', 'El cliente no está conforme con el pedido', 14);
INSERT INTO bdjardineria.pedido VALUES (30, '2018-08-03', '2018-09-03', '2018-08-31', 'Entregado', NULL, 13);
INSERT INTO bdjardineria.pedido VALUES (31, '2018-09-04', '2018-09-30', '2018-10-04', 'Rechazado', 'El cliente ha rechazado por llegar 5 dias tarde', 13);
INSERT INTO bdjardineria.pedido VALUES (32, '2017-01-07', '2017-01-19', '2017-01-27', 'Entregado', 'Entrega tardia, el cliente puso reclamacion', 4);
INSERT INTO bdjardineria.pedido VALUES (33, '2017-05-20', '2017-05-28', NULL, 'Rechazado', 'El pedido fue anulado por el cliente', 4);
INSERT INTO bdjardineria.pedido VALUES (34, '2017-06-20', '2018-06-28', '2018-06-28', 'Entregado', NULL, 4);
INSERT INTO bdjardineria.pedido VALUES (35, '2018-03-10', '2019-03-20', NULL, 'Rechazado', 'Limite de credito superado', 4);
INSERT INTO bdjardineria.pedido VALUES (36, '2018-10-15', '2018-12-15', '2018-12-10', 'Entregado', NULL, 14);
INSERT INTO bdjardineria.pedido VALUES (37, '2018-11-03', '2019-11-13', NULL, 'Pendiente', NULL, 4);
INSERT INTO bdjardineria.pedido VALUES (38, '2019-03-05', '2019-03-06', '2019-03-07', 'Entregado', NULL, 19);
INSERT INTO bdjardineria.pedido VALUES (39, '2019-03-06', '2019-03-07', '2019-03-09', 'Pendiente', NULL, 19);
INSERT INTO bdjardineria.pedido VALUES (40, '2019-03-09', '2019-03-10', '2019-03-13', 'Entregado', NULL, 19);
INSERT INTO bdjardineria.pedido VALUES (41, '2019-03-12', '2019-03-13', '2019-03-13', 'Entregado', NULL, 19);
INSERT INTO bdjardineria.pedido VALUES (42, '2019-03-22', '2019-03-23', '2019-03-27', 'Entregado', NULL, 19);
INSERT INTO bdjardineria.pedido VALUES (43, '2019-03-25', '2019-03-26', '2019-03-28', 'Pendiente', NULL, 23);
INSERT INTO bdjardineria.pedido VALUES (44, '2019-03-26', '2019-03-27', '2019-03-30', 'Pendiente', NULL, 23);
INSERT INTO bdjardineria.pedido VALUES (45, '2019-04-01', '2019-03-04', '2019-03-07', 'Entregado', NULL, 23);
INSERT INTO bdjardineria.pedido VALUES (46, '2019-04-03', '2019-03-04', '2019-03-05', 'Entregado', NULL, 23);
INSERT INTO bdjardineria.pedido VALUES (47, '2019-04-15', '2019-03-17', '2019-03-17', 'Entregado', NULL, 23);
INSERT INTO bdjardineria.pedido VALUES (48, '2018-03-17', '2018-03-30', '2018-03-29', 'Entregado', 'Según el Cliente, el pedido llegó defectuoso', 26);
INSERT INTO bdjardineria.pedido VALUES (49, '2018-07-12', '2018-07-22', '2018-07-23', 'Entregado', 'El pedido llegó 1 día tarde, pero no hubo queja por parte de la empresa compradora', 26);
INSERT INTO bdjardineria.pedido VALUES (50, '2018-03-17', '2018-08-09', NULL, 'Pendiente', 'Al parecer, el pedido se ha extraviado a la altura de Sotalbo (Ávila)', 26);
INSERT INTO bdjardineria.pedido VALUES (51, '2018-10-01', '2018-10-14', '2018-10-14', 'Entregado', 'Todo se entregó a tiempo y en perfecto estado, a pesar del pésimo estado de las carreteras.', 26);
INSERT INTO bdjardineria.pedido VALUES (52, '2018-12-07', '2018-12-21', NULL, 'Pendiente', 'El transportista ha llamado a Eva María para indicarle que el pedido llegará más tarde de lo esperado.', 26);
INSERT INTO bdjardineria.pedido VALUES (53, '2018-10-15', '2018-11-15', '2018-11-09', 'Entregado', 'El pedido llega 6 dias antes', 13);
INSERT INTO bdjardineria.pedido VALUES (54, '2019-01-11', '2019-02-11', NULL, 'Pendiente', NULL, 14);
INSERT INTO bdjardineria.pedido VALUES (55, '2018-12-10', '2019-01-10', '2019-01-11', 'Entregado', 'Retrasado 1 dia por problemas de transporte', 14);
INSERT INTO bdjardineria.pedido VALUES (56, '2018-12-19', '2019-01-20', NULL, 'Rechazado', 'El cliente a anulado el pedido el dia 2009-01-10', 13);
INSERT INTO bdjardineria.pedido VALUES (57, '2019-01-05', '2019-02-05', NULL, 'Pendiente', NULL, 13);
INSERT INTO bdjardineria.pedido VALUES (58, '2019-01-24', '2019-01-31', '2019-01-30', 'Entregado', 'TODO CORRECTO', 3);
INSERT INTO bdjardineria.pedido VALUES (59, '2018-11-09', '2018-11-14', '2018-11-14', 'Entregado', 'El cliente paga la mitad con tarjeta y la otra mitad con efectivo, se le realizan dos facturas', 1);
INSERT INTO bdjardineria.pedido VALUES (60, '2018-12-22', '2018-12-27', '2018-12-28', 'Entregado', 'El cliente comprueba la integridad del paquete, todo correcto', 1);
INSERT INTO bdjardineria.pedido VALUES (61, '2019-01-15', '2019-01-20', NULL, 'Pendiente', 'El cliente llama para confirmar la fecha - Esperando al proveedor', 3);
INSERT INTO bdjardineria.pedido VALUES (62, '2019-01-20', '2019-01-27', NULL, 'Pendiente', 'El cliente requiere que el pedido se le entregue de 16:00h a 22:00h', 1);
INSERT INTO bdjardineria.pedido VALUES (63, '2019-01-22', '2019-01-27', NULL, 'Pendiente', 'El cliente requiere que el pedido se le entregue de 9:00h a 13:00h', 1);
INSERT INTO bdjardineria.pedido VALUES (64, '2019-01-24', '2019-01-31', '2019-01-30', 'Entregado', 'TODO CORRECTO', 1);
INSERT INTO bdjardineria.pedido VALUES (65, '2019-02-02', '2019-02-08', NULL, 'Rechazado', 'El cliente carece de saldo en la cuenta asociada', 1);
INSERT INTO bdjardineria.pedido VALUES (66, '2019-02-06', '2019-02-12', NULL, 'Rechazado', 'El cliente anula la operacion para adquirir mas producto', 3);
INSERT INTO bdjardineria.pedido VALUES (67, '2019-02-07', '2019-02-13', NULL, 'Entregado', 'El pedido aparece como entregado pero no sabemos en que fecha', 3);
INSERT INTO bdjardineria.pedido VALUES (68, '2019-02-10', '2019-02-17', '2019-02-20', 'Entregado', 'El cliente se queja bastante de la espera asociada al producto', 3);
INSERT INTO bdjardineria.pedido VALUES (74, '2019-01-14', '2019-01-22', NULL, 'Entregado', 'El pedido no llego el dia que queria el cliente por fallo del transporte', 15);
INSERT INTO bdjardineria.pedido VALUES (75, '2019-01-11', '2019-01-13', '2019-01-13', 'Entregado', 'El pedido llego perfectamente', 15);
INSERT INTO bdjardineria.pedido VALUES (76, '2018-11-15', '2018-11-23', '2018-11-23', 'Entregado', NULL, 15);
INSERT INTO bdjardineria.pedido VALUES (77, '2019-01-03', '2019-01-08', NULL, 'Pendiente', 'El pedido no pudo ser entregado por problemas meteorologicos', 15);
INSERT INTO bdjardineria.pedido VALUES (78, '2018-12-15', '2018-12-17', '2018-12-17', 'Entregado', 'Fue entregado, pero faltaba mercancia que sera entregada otro dia', 15);
INSERT INTO bdjardineria.pedido VALUES (79, '2019-01-12', '2019-01-13', '2019-01-13', 'Entregado', NULL, 28);
INSERT INTO bdjardineria.pedido VALUES (80, '2019-01-25', '2019-01-26', NULL, 'Pendiente', 'No terminó el pago', 28);
INSERT INTO bdjardineria.pedido VALUES (81, '2019-01-18', '2019-01-24', NULL, 'Rechazado', 'Los producto estaban en mal estado', 28);
INSERT INTO bdjardineria.pedido VALUES (82, '2019-01-20', '2019-01-29', '2019-01-29', 'Entregado', 'El pedido llego un poco mas tarde de la hora fijada', 28);
INSERT INTO bdjardineria.pedido VALUES (83, '2019-01-24', '2019-01-28', NULL, 'Entregado', NULL, 28);
INSERT INTO bdjardineria.pedido VALUES (89, '2017-10-05', '2017-12-13', '2017-12-10', 'Entregado', 'La entrega se realizo dias antes de la fecha esperada por lo que el cliente quedo satisfecho', 35);
INSERT INTO bdjardineria.pedido VALUES (90, '2019-02-07', '2018-02-17', NULL, 'Pendiente', 'Debido a la nevada caída en la sierra, el pedido no podrá llegar hasta el día ', 27);
INSERT INTO bdjardineria.pedido VALUES (91, '2019-03-18', '2019-03-29', '2019-03-27', 'Entregado', 'Todo se entregó a su debido tiempo, incluso con un día de antelación', 27);
INSERT INTO bdjardineria.pedido VALUES (92, '2019-04-19', '2019-04-30', '2019-05-03', 'Entregado', 'El pedido se entregó tarde debido a la festividad celebrada en España durante esas fechas', 27);
INSERT INTO bdjardineria.pedido VALUES (93, '2019-05-03', '2019-05-30', '2019-05-17', 'Entregado', 'El pedido se entregó antes de lo esperado.', 27);
INSERT INTO bdjardineria.pedido VALUES (94, '2019-10-18', '2019-11-01', NULL, 'Pendiente', 'El pedido está en camino.', 27);
INSERT INTO bdjardineria.pedido VALUES (95, '2018-01-04', '2018-01-19', '2018-01-19', 'Entregado', NULL, 35);
INSERT INTO bdjardineria.pedido VALUES (96, '2018-03-20', '2018-04-12', '2018-04-13', 'Entregado', 'La entrega se retraso un dia', 35);
INSERT INTO bdjardineria.pedido VALUES (97, '2018-10-08', '2018-11-25', '2018-11-25', 'Entregado', NULL, 35);
INSERT INTO bdjardineria.pedido VALUES (98, '2019-01-08', '2019-02-13', NULL, 'Pendiente', NULL, 35);
INSERT INTO bdjardineria.pedido VALUES (99, '2019-02-15', '2019-02-27', NULL, 'Pendiente', NULL, 16);
INSERT INTO bdjardineria.pedido VALUES (100, '2019-01-10', '2019-01-15', '2019-01-15', 'Entregado', 'El pedido llego perfectamente', 16);
INSERT INTO bdjardineria.pedido VALUES (101, '2019-03-07', '2019-03-27', NULL, 'Rechazado', 'El pedido fue rechazado por el cliente', 16);
INSERT INTO bdjardineria.pedido VALUES (102, '2018-12-28', '2019-01-08', '2019-01-08', 'Entregado', 'Pago pendiente', 16);
INSERT INTO bdjardineria.pedido VALUES (103, '2019-01-15', '2019-01-20', '2019-01-24', 'Pendiente', NULL, 30);
INSERT INTO bdjardineria.pedido VALUES (104, '2019-03-02', '2019-03-06', '2019-03-06', 'Entregado', NULL, 30);
INSERT INTO bdjardineria.pedido VALUES (105, '2019-02-14', '2019-02-20', NULL, 'Rechazado', 'el producto ha sido rechazado por la pesima calidad', 30);
INSERT INTO bdjardineria.pedido VALUES (106, '2019-05-13', '2019-05-15', '2019-05-20', 'Pendiente', NULL, 30);
INSERT INTO bdjardineria.pedido VALUES (107, '2019-04-06', '2019-04-10', '2019-04-10', 'Entregado', NULL, 30);
INSERT INTO bdjardineria.pedido VALUES (108, '2019-04-09', '2019-04-15', '2019-04-15', 'Entregado', NULL, 16);
INSERT INTO bdjardineria.pedido VALUES (109, '2016-05-25', '2016-07-28', '2016-07-28', 'Entregado', NULL, 38);
INSERT INTO bdjardineria.pedido VALUES (110, '2017-03-19', '2017-04-24', '2017-04-24', 'Entregado', NULL, 38);
INSERT INTO bdjardineria.pedido VALUES (111, '2018-03-05', '2018-03-30', '2018-03-30', 'Entregado', NULL, 36);
INSERT INTO bdjardineria.pedido VALUES (112, '2019-03-05', '2019-04-06', '2019-05-07', 'Pendiente', NULL, 36);
INSERT INTO bdjardineria.pedido VALUES (113, '2018-10-28', '2018-11-09', '2019-01-09', 'Rechazado', 'El producto ha sido rechazado por la tardanza de el envio', 36);
INSERT INTO bdjardineria.pedido VALUES (114, '2019-01-15', '2019-01-29', '2019-01-31', 'Entregado', 'El envio llego dos dias más tarde debido al mal tiempo', 36);
INSERT INTO bdjardineria.pedido VALUES (115, '2018-11-29', '2019-01-26', '2019-02-27', 'Pendiente', NULL, 36);
INSERT INTO bdjardineria.pedido VALUES (116, '2018-06-28', '2018-08-01', '2018-08-01', 'Entregado', NULL, 38);
INSERT INTO bdjardineria.pedido VALUES (117, '2018-08-25', '2018-10-01', NULL, 'Rechazado', 'El pedido ha sido rechazado por la acumulacion de pago pendientes del cliente', 38);
INSERT INTO bdjardineria.pedido VALUES (118, '2019-02-15', '2019-02-27', NULL, 'Pendiente', NULL, 16);
INSERT INTO bdjardineria.pedido VALUES (119, '2019-01-10', '2019-01-15', '2019-01-15', 'Entregado', 'El pedido llego perfectamente', 16);
INSERT INTO bdjardineria.pedido VALUES (120, '2019-03-07', '2019-03-27', NULL, 'Rechazado', 'El pedido fue rechazado por el cliente', 16);
INSERT INTO bdjardineria.pedido VALUES (121, '2018-12-28', '2019-01-08', '2019-01-08', 'Entregado', 'Pago pendiente', 16);
INSERT INTO bdjardineria.pedido VALUES (122, '2019-04-09', '2019-04-15', '2019-04-15', 'Entregado', NULL, 16);
INSERT INTO bdjardineria.pedido VALUES (123, '2019-01-15', '2019-01-20', '2019-01-24', 'Pendiente', NULL, 30);
INSERT INTO bdjardineria.pedido VALUES (124, '2019-03-02', '2019-03-06', '2019-03-06', 'Entregado', NULL, 30);
INSERT INTO bdjardineria.pedido VALUES (125, '2019-02-14', '2019-02-20', NULL, 'Rechazado', 'el producto ha sido rechazado por la pesima calidad', 30);
INSERT INTO bdjardineria.pedido VALUES (126, '2019-05-13', '2019-05-15', '2019-05-20', 'Pendiente', NULL, 30);
INSERT INTO bdjardineria.pedido VALUES (127, '2019-04-06', '2019-04-10', '2019-04-10', 'Entregado', NULL, 30);
INSERT INTO bdjardineria.pedido VALUES (128, '2018-11-10', '2018-12-10', '2018-12-29', 'Rechazado', 'El pedido ha sido rechazado por el cliente por el retraso en la entrega', 38);


--
-- TOC entry 2283 (class 0 OID 18512)
-- Dependencies: 211
-- Data for Name: producto; Type: TABLE DATA; Schema: bdjardineria; Owner: batoi
--

INSERT INTO bdjardineria.producto VALUES ('11679', 'Sierra de Poda 400MM', 'Herramientas', '0,258', 'HiperGarden Tools', 'Gracias a la poda se consigue manipular un poco la naturaleza, dándole la forma que más nos guste. Este trabajo básico de jardinería también facilita que las plantas crezcan de un modo más equilibrado, y que las flores y los frutos vuelvan cada año con regularidad. Lo mejor es dar forma cuando los ejemplares son jóvenes, de modo que exijan pocos cuidados cuando sean adultos. Además de saber cuándo y cómo hay que podar, tener unas herramientas adecuadas para esta labor es también de vital importancia.', 15, 14.00, 11.00);
INSERT INTO bdjardineria.producto VALUES ('21636', 'Pala', 'Herramientas', '0,156', 'HiperGarden Tools', 'Palas de acero con cresta de corte en la punta para cortar bien el terreno. Buena penetración en tierras muy compactas.', 15, 14.00, 13.00);
INSERT INTO bdjardineria.producto VALUES ('22225', 'Rastrillo de Jardín', 'Herramientas', '1,064', 'HiperGarden Tools', 'Fabuloso rastillo que le ayudará a eliminar piedras, hojas, ramas y otros elementos incómodos en su jardín.', 15, 12.00, 11.00);
INSERT INTO bdjardineria.producto VALUES ('30310', 'Azadón', 'Herramientas', '0,168', 'HiperGarden Tools', 'Longitud:24cm. Herramienta fabricada en acero y pintura epoxi,alargando su durabilidad y preveniendo la corrosión.Diseño pensado para el ahorro de trabajo.', 15, 12.00, 11.00);
INSERT INTO bdjardineria.producto VALUES ('AR-001', 'Ajedrea', 'Aromáticas', '15-20', 'Murcia Seasons', 'Planta aromática que fresca se utiliza para condimentar carnes y ensaladas, y seca, para pastas, sopas y guisantes', 140, 1.00, 0.00);
INSERT INTO bdjardineria.producto VALUES ('AR-002', 'Lavándula Dentata', 'Aromáticas', '15-20', 'Murcia Seasons', 'Espliego de jardín, Alhucema rizada, Alhucema dentada, Cantueso rizado. Familia: Lamiaceae.Origen: España y Portugal. Mata de unos 60 cm de alto. Las hojas son aromáticas, dentadas y de color verde grisáceas.  Produce compactas espigas de flores pequeñas, ligeramente aromáticas, tubulares,de color azulado y con brácteas púrpuras.  Frutos: nuececillas alargadas encerradas en el tubo del cáliz.  Se utiliza en jardineria y no en perfumeria como otros cantuesos, espliegos y lavandas.  Tiene propiedades aromatizantes y calmantes. Adecuadas para la formación de setos bajos. Se dice que su aroma ahuyenta pulgones y otros insectos perjudiciales para las plantas vecinas.', 140, 1.00, 0.00);
INSERT INTO bdjardineria.producto VALUES ('AR-003', 'Mejorana', 'Aromáticas', '15-20', 'Murcia Seasons', 'Origanum majorana. No hay que confundirlo con el orégano. Su sabor se parece más al tomillo, pero es más dulce y aromático.Se usan las hojas frescas o secas, picadas, machacadas o en polvo, en sopas, rellenos, quiches y tartas, tortillas, platos con papas y, como aderezo, en ramilletes de hierbas.El sabor delicado de la mejorana se elimina durante la cocción, de manera que es mejor agregarla cuando el plato esté en su punto o en aquéllos que apenas necesitan cocción.', 140, 1.00, 0.00);
INSERT INTO bdjardineria.producto VALUES ('AR-004', 'Melissa ', 'Aromáticas', '15-20', 'Murcia Seasons', 'Es una planta perenne (dura varios años) conocida por el agradable y característico olor a limón que desprenden en verano. Nunca debe faltar en la huerta o jardín por su agradable aroma y por los variados usos que tiene: planta olorosa, condimentaria y medicinal. Su cultivo es muy fácil. Le va bien un suelo ligero, con buen drenaje y riego sin exceso. A pleno sol o por lo menos 5 horas de sol por día. Cada año, su abonado mineral correspondiente.En otoño, la melisa pierde el agradable olor a limón que desprende en verano sus flores azules y blancas. En este momento se debe cortar a unos 20 cm. del suelo. Brotará de forma densa en primavera.', 140, 1.00, 0.00);
INSERT INTO bdjardineria.producto VALUES ('AR-005', 'Mentha Sativa', 'Aromáticas', '15-20', 'Murcia Seasons', 'Â¿Quién no conoce la Hierbabuena? Se trata de una plantita muy aromática, agradable y cultivada extensamente por toda España. Es hierba perenne (por tanto vive varios años, no es anual). Puedes cultivarla en maceta o plantarla en la tierra del jardín o en un rincón del huerto. Lo más importante es que cuente con bastante agua. En primavera debes aportar fertilizantes minerales. Vive mejor en semisombra que a pleno sol.Si ves orugas o los agujeros en hojas consecuencia de su ataque, retíralas una a una a mano; no uses insecticidas químicos.', 140, 1.00, 0.00);
INSERT INTO bdjardineria.producto VALUES ('AR-006', 'Petrosilium Hortense (Peregil)', 'Aromáticas', '15-20', 'Murcia Seasons', 'nombre científico o latino: Petroselinum hortense, Petroselinum crispum. nombre común o vulgar: Perejil, Perejil rizado Familia: Umbelliferae (Umbelíferas). Origen: el origen del perejil se encuentra en el Mediterraneo. Esta naturalizada en casi toda Europa. Se utiliza como condimento y para adorno, pero también en ensaladas. Se suele regalar en las fruterías y verdulerías.El perejil lo hay de 2 tipos: de hojas planas y de hojas rizadas.', 140, 1.00, 0.00);
INSERT INTO bdjardineria.producto VALUES ('AR-007', 'Salvia Mix', 'Aromáticas', '15-20', 'Murcia Seasons', 'La Salvia es un pequeño arbusto que llega hasta el metro de alto.Tiene una vida breve, de unos pocos años.En el jardín, como otras aromáticas, queda muy bien en una rocalla o para hacer una bordura perfumada a cada lado de un camino de Salvia. Abona después de cada corte y recorta el arbusto una vez pase la floración.', 140, 1.00, 0.00);
INSERT INTO bdjardineria.producto VALUES ('AR-008', 'Thymus Citriodra (Tomillo limón)', 'Aromáticas', '15-20', 'Murcia Seasons', 'nombre común o vulgar: Tomillo, Tremoncillo Familia: Labiatae (Labiadas).Origen: Región mediterránea.Arbustillo bajo, de 15 a 40 cm de altura. Las hojas son muy pequeñas, de unos 6 mm de longitud; según la variedad pueden ser verdes, verdes grisáceas, amarillas, o jaspeadas. Las flores aparecen de mediados de primavera hasta bien entrada la época estival y se presentan en racimos terminales que habitualmente son de color violeta o púrpura aunque también pueden ser blancas. Esta planta despide un intenso y típico aroma, que se incrementa con el roce. El tomillo resulta de gran belleza cuando está en flor. El tomillo atrae a avispas y abejas. En jardinería se usa como manchas, para hacer borduras, para aromatizar el ambiente, llenar huecos, cubrir rocas, para jardines en miniatura, etc. Arranque las flores y hojas secas del tallo y añadálos a un popurri, introdúzcalos en saquitos de hierbas o en la almohada.También puede usar las ramas secas con flores para añadir aroma y textura a cestos abiertos.', 140, 1.00, 0.00);
INSERT INTO bdjardineria.producto VALUES ('AR-009', 'Thymus Vulgaris', 'Aromáticas', '15-20', 'Murcia Seasons', 'nombre común o vulgar: Tomillo, Tremoncillo Familia: Labiatae (Labiadas). Origen: Región mediterránea. Arbustillo bajo, de 15 a 40 cm de altura. Las hojas son muy pequeñas, de unos 6 mm de longitud; según la variedad pueden ser verdes, verdes grisáceas, amarillas, o jaspeadas. Las flores aparecen de mediados de primavera hasta bien entrada la época estival y se presentan en racimos terminales que habitualmente son de color violeta o púrpura aunque también pueden ser blancas. Esta planta despide un intenso y típico aroma, que se incrementa con el roce. El tomillo resulta de gran belleza cuando está en flor. El tomillo atrae a avispas y abejas.
 En jardinería se usa como manchas, para hacer borduras, para aromatizar el ambiente, llenar huecos, cubrir rocas, para jardines en miniatura, etc. Arranque las flores y hojas secas del tallo y añadálos a un popurri, introdúzcalos en saquitos de hierbas o en la almohada. También puede usar las ramas secas con flores para añadir aroma y textura a cestos abiertos.', 140, 1.00, 0.00);
INSERT INTO bdjardineria.producto VALUES ('AR-010', 'Santolina Chamaecyparys', 'Aromáticas', '15-20', 'Murcia Seasons', '', 140, 1.00, 0.00);
INSERT INTO bdjardineria.producto VALUES ('FR-1', 'Expositor Cítricos Mix', 'Frutales', '100-120', 'Frutales Talavera S.A', '', 15, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('FR-31', 'Ciruelo Santa Rosa', 'Frutales', '', 'Frutales Talavera S.A', 'árbol de tamaño mediano que alcanza una altura máxima de 5-6 m. Tronco de corteza pardo-azulada, brillante, lisa o agrietada longitudinalmente. Produce ramas alternas, pequeñas, delgadas, unas veces lisas, glabras y otras pubescentes y vellosas', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-10', 'Limonero 2 años injerto', 'Frutales', '', 'NaranjasValencianas.com', 'El limonero, pertenece al grupo de los cítricos, teniendo su origen hace unos 20 millones de años en el sudeste asiático. Fue introducido por los árabes en el área mediterránea entre los años 1.000 a 1.200, habiendo experimentando numerosas modificaciones debidas tanto a la selección natural mediante hibridaciones espontáneas como a las producidas por el hombre, en este caso buscando las necesidades del mercado.', 15, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('FR-100', 'Nectarina', 'Frutales', '8/10', 'Frutales Talavera S.A', 'Se trata de un árbol derivado por mutación de los melocotoneros comunes, y los únicos caracteres diferenciales son la ausencia de tomentosidad en la piel del fruto. La planta, si se deja crecer libremente, adopta un porte globoso con unas dimensiones medias de 4-6 metros', 50, 11.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('FR-101', 'Nogal', 'Frutales', '8/10', 'Frutales Talavera S.A', '', 50, 13.00, 10.00);
INSERT INTO bdjardineria.producto VALUES ('FR-102', 'Olea-Olivos', 'Frutales', '8/10', 'Frutales Talavera S.A', 'Existen dos hipótesis sobre el origen del olivo, una que postula que proviene de las costas de Siria, Líbano e Israel y otra que considera que lo considera originario de Asia menor. La llegada a Europa probablemente tuvo lugar de mano de los Fenicios, en transito por Chipre, Creta, e Islas del Mar Egeo, pasando a Grecia y más tarde a Italia. Los primeros indicios de la presencia del olivo en las costas mediterráneas españolas coinciden con el dominio romano, aunque fueron posteriormente los árabes los que impulsaron su cultivo en Andalucía, convirtiendo a España en el primer país productor de aceite de oliva a nivel mundial.', 50, 18.00, 14.00);
INSERT INTO bdjardineria.producto VALUES ('FR-103', 'Olea-Olivos', 'Frutales', '10/12', 'Frutales Talavera S.A', 'Existen dos hipótesis sobre el origen del olivo, una que postula que proviene de las costas de Siria, Líbano e Israel y otra que considera que lo considera originario de Asia menor. La llegada a Europa probablemente tuvo lugar de mano de los Fenicios, en transito por Chipre, Creta, e Islas del Mar Egeo, pasando a Grecia y más tarde a Italia. Los primeros indicios de la presencia del olivo en las costas mediterráneas españolas coinciden con el dominio romano, aunque fueron posteriormente los árabes los que impulsaron su cultivo en Andalucía, convirtiendo a España en el primer país productor de aceite de oliva a nivel mundial.', 50, 25.00, 20.00);
INSERT INTO bdjardineria.producto VALUES ('FR-104', 'Olea-Olivos', 'Frutales', '12/4', 'Frutales Talavera S.A', 'Existen dos hipótesis sobre el origen del olivo, una que postula que proviene de las costas de Siria, Líbano e Israel y otra que considera que lo considera originario de Asia menor. La llegada a Europa probablemente tuvo lugar de mano de los Fenicios, en transito por Chipre, Creta, e Islas del Mar Egeo, pasando a Grecia y más tarde a Italia. Los primeros indicios de la presencia del olivo en las costas mediterráneas españolas coinciden con el dominio romano, aunque fueron posteriormente los árabes los que impulsaron su cultivo en Andalucía, convirtiendo a España en el primer país productor de aceite de oliva a nivel mundial.', 50, 49.00, 39.00);
INSERT INTO bdjardineria.producto VALUES ('FR-105', 'Olea-Olivos', 'Frutales', '14/16', 'Frutales Talavera S.A', 'Existen dos hipótesis sobre el origen del olivo, una que postula que proviene de las costas de Siria, Líbano e Israel y otra que considera que lo considera originario de Asia menor. La llegada a Europa probablemente tuvo lugar de mano de los Fenicios, en transito por Chipre, Creta, e Islas del Mar Egeo, pasando a Grecia y más tarde a Italia. Los primeros indicios de la presencia del olivo en las costas mediterráneas españolas coinciden con el dominio romano, aunque fueron posteriormente los árabes los que impulsaron su cultivo en Andalucía, convirtiendo a España en el primer país productor de aceite de oliva a nivel mundial.', 50, 70.00, 56.00);
INSERT INTO bdjardineria.producto VALUES ('FR-106', 'Peral', 'Frutales', '8/10', 'Frutales Talavera S.A', 'Árbol piramidal, redondeado en su juventud, luego oval, que llega hasta 20 metros de altura y por término medio vive 65 años.Tronco alto, grueso, de corteza agrietada, gris, de la cual se destacan con frecuencia placas lenticulares.Las ramas se insertan formando ángulo agudo con el tronco (45º), de corteza lisa, primero verde y luego gris-violácea, con numerosas lenticelas.', 50, 11.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('FR-107', 'Peral', 'Frutales', '10/12', 'Frutales Talavera S.A', 'Árbol piramidal, redondeado en su juventud, luego oval, que llega hasta 20 metros de altura y por término medio vive 65 años.Tronco alto, grueso, de corteza agrietada, gris, de la cual se destacan con frecuencia placas lenticulares.Las ramas se insertan formando ángulo agudo con el tronco (45º), de corteza lisa, primero verde y luego gris-violácea, con numerosas lenticelas.', 50, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('FR-108', 'Peral', 'Frutales', '12/14', 'Frutales Talavera S.A', 'Árbol piramidal, redondeado en su juventud, luego oval, que llega hasta 20 metros de altura y por término medio vive 65 años.Tronco alto, grueso, de corteza agrietada, gris, de la cual se destacan con frecuencia placas lenticulares.Las ramas se insertan formando ángulo agudo con el tronco (45º), de corteza lisa, primero verde y luego gris-violácea, con numerosas lenticelas.', 50, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('FR-11', 'Limonero 30/40', 'Frutales', '', 'NaranjasValencianas.com', 'El limonero, pertenece al grupo de los cítricos, teniendo su origen hace unos 20 millones de años en el sudeste asiático. Fue introducido por los árabes en el área mediterránea entre los años 1.000 a 1.200, habiendo experimentando numerosas modificaciones debidas tanto a la selección natural mediante hibridaciones espontáneas como a las producidas por el', 15, 100.00, 80.00);
INSERT INTO bdjardineria.producto VALUES ('FR-12', 'Kunquat ', 'Frutales', '', 'NaranjasValencianas.com', 'su nombre científico se origina en honor a un hoticultor escocés que recolectó especímenes en China, ("Fortunella"), Robert Fortune (1812-1880), y "margarita", del latín margaritus-a-um = perla, en alusión a sus pequeños y brillantes frutos. Se trata de un arbusto o árbol pequeño de 2-3 m de altura, inerme o con escasas espinas.Hojas lanceoladas de 4-8 (-15) cm de longitud, con el ápice redondeado y la base cuneada.Tienen el margen crenulado en su mitad superior, el haz verde brillante y el envés más pálido.Pecíolo ligeramente marginado.Flores perfumadas solitarias o agrupadas en inflorescencias axilares, blancas.El fruto es lo más característico, es el más pequeño de todos los cítricos y el único cuya cáscara se puede comer.Frutos pequeños, con semillas, de corteza fina, dulce, aromática y comestible, y de pulpa naranja amarillenta y ligeramente ácida.Sus frutos son muy pequeños y tienen un carácter principalmente ornamental.', 15, 21.00, 16.00);
INSERT INTO bdjardineria.producto VALUES ('FR-13', 'Kunquat  EXTRA con FRUTA', 'Frutales', '150-170', 'NaranjasValencianas.com', 'su nombre científico se origina en honor a un hoticultor escocés que recolectó especímenes en China, ("Fortunella"), Robert Fortune (1812-1880), y "margarita", del latín margaritus-a-um = perla, en alusión a sus pequeños y brillantes frutos. Se trata de un arbusto o árbol pequeño de 2-3 m de altura, inerme o con escasas espinas.Hojas lanceoladas de 4-8 (-15) cm de longitud, con el ápice redondeado y la base cuneada.Tienen el margen crenulado en su mitad superior, el haz verde brillante y el envés más pálido.Pecíolo ligeramente marginado.Flores perfumadas solitarias o agrupadas en inflorescencias axilares, blancas.El fruto es lo más característico, es el más pequeño de todos los cítricos y el único cuya cáscara se puede comer.Frutos pequeños, con semillas, de corteza fina, dulce, aromática y comestible, y de pulpa naranja amarillenta y ligeramente ácida.Sus frutos son muy pequeños y tienen un carácter principalmente ornamental.', 15, 57.00, 45.00);
INSERT INTO bdjardineria.producto VALUES ('FR-14', 'Calamondin Mini', 'Frutales', '', 'Frutales Talavera S.A', 'Se trata de un pequeño arbolito de copa densa, con tendencia a la verticalidad, inerme o con cortas espinas. Sus hojas son pequeñas, elípticas de 5-10 cm de longitud, con los pecíolos estrechamente alados.Posee 1 o 2 flores en situación axilar, al final de las ramillas.Sus frutos son muy pequeños (3-3,5 cm de diámetro), con pocas semillas, esféricos u ovales, con la zona apical aplanada; corteza de color naranja-rojizo, muy fina y fácilmente separable de la pulpa, que es dulce, ácida y comestible..', 15, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('FR-15', 'Calamondin Copa ', 'Frutales', '', 'Frutales Talavera S.A', 'Se trata de un pequeño arbolito de copa densa, con tendencia a la verticalidad, inerme o con cortas espinas. Sus hojas son pequeñas, elípticas de 5-10 cm de longitud, con los pecíolos estrechamente alados.Posee 1 o 2 flores en situación axilar, al final de las ramillas.Sus frutos son muy pequeños (3-3,5 cm de diámetro), con pocas semillas, esféricos u ovales, con la zona apical aplanada; corteza de color naranja-rojizo, muy fina y fácilmente separable de la pulpa, que es dulce, ácida y comestible..', 15, 25.00, 20.00);
INSERT INTO bdjardineria.producto VALUES ('FR-16', 'Calamondin Copa EXTRA Con FRUTA', 'Frutales', '100-120', 'Frutales Talavera S.A', 'Se trata de un pequeño arbolito de copa densa, con tendencia a la verticalidad, inerme o con cortas espinas. Sus hojas son pequeñas, elípticas de 5-10 cm de longitud, con los pecíolos estrechamente alados.Posee 1 o 2 flores en situación axilar, al final de las ramillas.Sus frutos son muy pequeños (3-3,5 cm de diámetro), con pocas semillas, esféricos u ovales, con la zona apical aplanada; corteza de color naranja-rojizo, muy fina y fácilmente separable de la pulpa, que es dulce, ácida y comestible..', 15, 45.00, 36.00);
INSERT INTO bdjardineria.producto VALUES ('FR-17', 'Rosal bajo 1Âª -En maceta-inicio brotación', 'Frutales', '', 'Frutales Talavera S.A', '', 15, 2.00, 1.00);
INSERT INTO bdjardineria.producto VALUES ('FR-18', 'ROSAL TREPADOR', 'Frutales', '', 'Frutales Talavera S.A', '', 350, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('FR-19', 'Camelia Blanco, Chrysler Rojo, Soraya Naranja, ', 'Frutales', '', 'NaranjasValencianas.com', '', 350, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('FR-2', 'Naranjo -Plantón joven 1 año injerto', 'Frutales', '', 'NaranjasValencianas.com', 'El naranjo es un árbol pequeño, que no supera los 3-5 metros de altura, con una copa compacta, cónica, transformada en esérica gracias a la poda. Su tronco es de color gris y liso, y las hojas son perennes, coriáceas, de un verde intenso y brillante, con forma oval o elíptico-lanceolada. Poseen, en el caso del naranjo amargo, un típico peciolo alado en forma de ÂcorazónÂ, que en el naranjo dulce es más estrecho y menos patente.', 15, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('FR-20', 'Landora Amarillo, Rose Gaujard bicolor blanco-rojo', 'Frutales', '', 'Frutales Talavera S.A', '', 350, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('FR-21', 'Kordes Perfect bicolor rojo-amarillo, Roundelay rojo fuerte', 'Frutales', '', 'Frutales Talavera S.A', '', 350, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('FR-22', 'Pitimini rojo', 'Frutales', '', 'Frutales Talavera S.A', '', 350, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('FR-23', 'Rosal copa ', 'Frutales', '', 'Frutales Talavera S.A', '', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-24', 'Albaricoquero Corbato', 'Frutales', '', 'Melocotones de Cieza S.A.', 'árbol que puede pasar de los 6 m de altura, en la región mediterránea con ramas formando una copa redondeada. La corteza del tronco es pardo-violácea, agrietada; las ramas son rojizas y extendidas cuando jóvenes y las ramas secundarias son cortas, divergentes y escasas. Las yemas latentes son frecuentes especialmente sobre las ramas viejas.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-25', 'Albaricoquero Moniqui', 'Frutales', '', 'Melocotones de Cieza S.A.', 'árbol que puede pasar de los 6 m de altura, en la región mediterránea con ramas formando una copa redondeada. La corteza del tronco es pardo-violácea, agrietada; las ramas son rojizas y extendidas cuando jóvenes y las ramas secundarias son cortas, divergentes y escasas. Las yemas latentes son frecuentes especialmente sobre las ramas viejas.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-26', 'Albaricoquero Kurrot', 'Frutales', '', 'Melocotones de Cieza S.A.', 'árbol que puede pasar de los 6 m de altura, en la región mediterránea con ramas formando una copa redondeada. La corteza del tronco es pardo-violácea, agrietada; las ramas son rojizas y extendidas cuando jóvenes y las ramas secundarias son cortas, divergentes y escasas. Las yemas latentes son frecuentes especialmente sobre las ramas viejas.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-27', 'Cerezo Burlat', 'Frutales', '', 'Jerte Distribuciones S.L.', 'Las principales especies de cerezo cultivadas en el mundo son el cerezo dulce (Prunus avium), el guindo (P. cerasus) y el cerezo "Duke", híbrido de los anteriores. Ambas especies son naturales del sureste de Europa y oeste de Asia. El cerezo dulce tuvo su origen probablemente en el mar Negro y en el mar Caspio, difundiéndose después hacia Europa y Asia, llevado por los pájaros y las migraciones humanas. Fue uno de los frutales más apreciados por los griegos y con el Imperio Romano se extendió a regiones muy diversas. En la actualidad, el cerezo se encuentra difundido por numerosas regiones y países del mundo con clima templado', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-28', 'Cerezo Picota', 'Frutales', '', 'Jerte Distribuciones S.L.', 'Las principales especies de cerezo cultivadas en el mundo son el cerezo dulce (Prunus avium), el guindo (P. cerasus) y el cerezo "Duke", híbrido de los anteriores. Ambas especies son naturales del sureste de Europa y oeste de Asia. El cerezo dulce tuvo su origen probablemente en el mar Negro y en el mar Caspio, difundiéndose después hacia Europa y Asia, llevado por los pájaros y las migraciones humanas. Fue uno de los frutales más apreciados por los griegos y con el Imperio Romano se extendió a regiones muy diversas. En la actualidad, el cerezo se encuentra difundido por numerosas regiones y países del mundo con clima templado', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-29', 'Cerezo Napoleón', 'Frutales', '', 'Jerte Distribuciones S.L.', 'Las principales especies de cerezo cultivadas en el mundo son el cerezo dulce (Prunus avium), el guindo (P. cerasus) y el cerezo "Duke", híbrido de los anteriores. Ambas especies son naturales del sureste de Europa y oeste de Asia. El cerezo dulce tuvo su origen probablemente en el mar Negro y en el mar Caspio, difundiéndose después hacia Europa y Asia, llevado por los pájaros y las migraciones humanas. Fue uno de los frutales más apreciados por los griegos y con el Imperio Romano se extendió a regiones muy diversas. En la actualidad, el cerezo se encuentra difundido por numerosas regiones y países del mundo con clima templado', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-3', 'Naranjo 2 años injerto', 'Frutales', '', 'NaranjasValencianas.com', 'El naranjo es un árbol pequeño, que no supera los 3-5 metros de altura, con una copa compacta, cónica, transformada en esérica gracias a la poda. Su tronco es de color gris y liso, y las hojas son perennes, coriáceas, de un verde intenso y brillante, con forma oval o elíptico-lanceolada. Poseen, en el caso del naranjo amargo, un típico peciolo alado en forma de ÂcorazónÂ, que en el naranjo dulce es más estrecho y menos patente.', 15, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('FR-30', 'Ciruelo R. Claudia Verde   ', 'Frutales', '', 'Frutales Talavera S.A', 'árbol de tamaño mediano que alcanza una altura máxima de 5-6 m. Tronco de corteza pardo-azulada, brillante, lisa o agrietada longitudinalmente. Produce ramas alternas, pequeñas, delgadas, unas veces lisas, glabras y otras pubescentes y vellosas', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('OR-120', 'Lonicera Nitida ', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-121', 'Lonicera Nitida "Maigrum"', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-122', 'Lonicera Pileata', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('FR-32', 'Ciruelo Golden Japan', 'Frutales', '', 'Frutales Talavera S.A', 'árbol de tamaño mediano que alcanza una altura máxima de 5-6 m. Tronco de corteza pardo-azulada, brillante, lisa o agrietada longitudinalmente. Produce ramas alternas, pequeñas, delgadas, unas veces lisas, glabras y otras pubescentes y vellosas', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-33', 'Ciruelo Friar', 'Frutales', '', 'Frutales Talavera S.A', 'árbol de tamaño mediano que alcanza una altura máxima de 5-6 m. Tronco de corteza pardo-azulada, brillante, lisa o agrietada longitudinalmente. Produce ramas alternas, pequeñas, delgadas, unas veces lisas, glabras y otras pubescentes y vellosas', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-34', 'Ciruelo Reina C. De Ollins', 'Frutales', '', 'Frutales Talavera S.A', 'árbol de tamaño mediano que alcanza una altura máxima de 5-6 m. Tronco de corteza pardo-azulada, brillante, lisa o agrietada longitudinalmente. Produce ramas alternas, pequeñas, delgadas, unas veces lisas, glabras y otras pubescentes y vellosas', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-35', 'Ciruelo Claudia Negra', 'Frutales', '', 'Frutales Talavera S.A', 'árbol de tamaño mediano que alcanza una altura máxima de 5-6 m. Tronco de corteza pardo-azulada, brillante, lisa o agrietada longitudinalmente. Produce ramas alternas, pequeñas, delgadas, unas veces lisas, glabras y otras pubescentes y vellosas', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-36', 'Granado Mollar de Elche', 'Frutales', '', 'Frutales Talavera S.A', 'pequeño árbol caducifolio, a veces con porte arbustivo, de 3 a 6 m de altura, con el tronco retorcido. Madera dura y corteza escamosa de color grisáceo. Las ramitas jóvenes son más o menos cuadrangulares o angostas y de cuatro alas, posteriormente se vuelven redondas con corteza de color café grisáceo, la mayoría de las ramas, pero especialmente las pequeñas ramitas axilares, son en forma de espina o terminan en una espina aguda; la copa es extendida.', 400, 9.00, 7.00);
INSERT INTO bdjardineria.producto VALUES ('FR-37', 'Higuera Napolitana', 'Frutales', '', 'Frutales Talavera S.A', 'La higuera (Ficus carica L.) es un árbol típico de secano en los países mediterráneos. Su rusticidad y su fácil multiplicación hacen de la higuera un frutal muy apropiado para el cultivo extensivo.. Siempre ha sido considerado como árbol que no requiere cuidado alguno una vez plantado y arraigado, limitándose el hombre a recoger de él los frutos cuando maduran, unos para consumo en fresco y otros para conserva. Las únicas higueras con cuidados culturales esmerados, en muchas comarcas, son las brevales, por el interés económico de su primera cosecha, la de brevas.', 400, 9.00, 7.00);
INSERT INTO bdjardineria.producto VALUES ('FR-38', 'Higuera Verdal', 'Frutales', '', 'Frutales Talavera S.A', 'La higuera (Ficus carica L.) es un árbol típico de secano en los países mediterráneos. Su rusticidad y su fácil multiplicación hacen de la higuera un frutal muy apropiado para el cultivo extensivo.. Siempre ha sido considerado como árbol que no requiere cuidado alguno una vez plantado y arraigado, limitándose el hombre a recoger de él los frutos cuando maduran, unos para consumo en fresco y otros para conserva. Las únicas higueras con cuidados culturales esmerados, en muchas comarcas, son las brevales, por el interés económico de su primera cosecha, la de brevas.', 400, 9.00, 7.00);
INSERT INTO bdjardineria.producto VALUES ('FR-39', 'Higuera Breva', 'Frutales', '', 'Frutales Talavera S.A', 'La higuera (Ficus carica L.) es un árbol típico de secano en los países mediterráneos. Su rusticidad y su fácil multiplicación hacen de la higuera un frutal muy apropiado para el cultivo extensivo.. Siempre ha sido considerado como árbol que no requiere cuidado alguno una vez plantado y arraigado, limitándose el hombre a recoger de él los frutos cuando maduran, unos para consumo en fresco y otros para conserva. Las únicas higueras con cuidados culturales esmerados, en muchas comarcas, son las brevales, por el interés económico de su primera cosecha, la de brevas.', 400, 9.00, 7.00);
INSERT INTO bdjardineria.producto VALUES ('FR-4', 'Naranjo calibre 8/10', 'Frutales', '', 'NaranjasValencianas.com', 'El naranjo es un árbol pequeño, que no supera los 3-5 metros de altura, con una copa compacta, cónica, transformada en esérica gracias a la poda. Su tronco es de color gris y liso, y las hojas son perennes, coriáceas, de un verde intenso y brillante, con forma oval o elíptico-lanceolada. Poseen, en el caso del naranjo amargo, un típico peciolo alado en forma de ÂcorazónÂ, que en el naranjo dulce es más estrecho y menos patente.', 15, 29.00, 23.00);
INSERT INTO bdjardineria.producto VALUES ('FR-40', 'Manzano Starking Delicious', 'Frutales', '', 'Frutales Talavera S.A', 'alcanza como máximo 10 m. de altura y tiene una copa globosa. Tronco derecho que normalmente alcanza de 2 a 2,5 m. de altura, con corteza cubierta de lenticelas, lisa, adherida, de color ceniciento verdoso sobre los ramos y escamosa y gris parda sobre las partes viejas del árbol. Tiene una vida de unos 60-80 años. Las ramas se insertan en ángulo abierto sobre el tallo, de color verde oscuro, a veces tendiendo a negruzco o violáceo. Los brotes jóvenes terminan con frecuencia en una espina', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-41', 'Manzano Reineta', 'Frutales', '', 'Frutales Talavera S.A', 'alcanza como máximo 10 m. de altura y tiene una copa globosa. Tronco derecho que normalmente alcanza de 2 a 2,5 m. de altura, con corteza cubierta de lenticelas, lisa, adherida, de color ceniciento verdoso sobre los ramos y escamosa y gris parda sobre las partes viejas del árbol. Tiene una vida de unos 60-80 años. Las ramas se insertan en ángulo abierto sobre el tallo, de color verde oscuro, a veces tendiendo a negruzco o violáceo. Los brotes jóvenes terminan con frecuencia en una espina', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-42', 'Manzano Golden Delicious', 'Frutales', '', 'Frutales Talavera S.A', 'alcanza como máximo 10 m. de altura y tiene una copa globosa. Tronco derecho que normalmente alcanza de 2 a 2,5 m. de altura, con corteza cubierta de lenticelas, lisa, adherida, de color ceniciento verdoso sobre los ramos y escamosa y gris parda sobre las partes viejas del árbol. Tiene una vida de unos 60-80 años. Las ramas se insertan en ángulo abierto sobre el tallo, de color verde oscuro, a veces tendiendo a negruzco o violáceo. Los brotes jóvenes terminan con frecuencia en una espina', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-43', 'Membrillero Gigante de Wranja', 'Frutales', '', 'Frutales Talavera S.A', '', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-44', 'Melocotonero Spring Crest', 'Frutales', '', 'Melocotones de Cieza S.A.', 'Árbol caducifolio de porte bajo con corteza lisa, de color ceniciento. Sus hojas son alargadas con el margen ligeramente aserrado, de color verde brillante, algo más claras por el envés. El melocotonero está muy arraigado en la cultura asiática.
En Japón, el noble heroe Momotaro, una especie de Cid japonés, nació del interior de un enorme melocotón que flotaba río abajo.
En China se piensa que comer melocotón confiere longevidad al ser humano, ya que formaba parte de la dieta de sus dioses inmortales.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-45', 'Melocotonero Amarillo de Agosto', 'Frutales', '', 'Melocotones de Cieza S.A.', 'Árbol caducifolio de porte bajo con corteza lisa, de color ceniciento. Sus hojas son alargadas con el margen ligeramente aserrado, de color verde brillante, algo más claras por el envés. El melocotonero está muy arraigado en la cultura asiática.
En Japón, el noble heroe Momotaro, una especie de Cid japonés, nació del interior de un enorme melocotón que flotaba río abajo.
En China se piensa que comer melocotón confiere longevidad al ser humano, ya que formaba parte de la dieta de sus dioses inmortales.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('OR-123', 'Philadelphus "Virginal"', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-124', 'Prunus pisardii  ', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-125', 'Viburnum Tinus "Eve Price"', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('FR-46', 'Melocotonero Federica', 'Frutales', '', 'Melocotones de Cieza S.A.', 'Árbol caducifolio de porte bajo con corteza lisa, de color ceniciento. Sus hojas son alargadas con el margen ligeramente aserrado, de color verde brillante, algo más claras por el envés. El melocotonero está muy arraigado en la cultura asiática.
En Japón, el noble heroe Momotaro, una especie de Cid japonés, nació del interior de un enorme melocotón que flotaba río abajo.
En China se piensa que comer melocotón confiere longevidad al ser humano, ya que formaba parte de la dieta de sus dioses inmortales.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-47', 'Melocotonero Paraguayo', 'Frutales', '', 'Melocotones de Cieza S.A.', 'Árbol caducifolio de porte bajo con corteza lisa, de color ceniciento. Sus hojas son alargadas con el margen ligeramente aserrado, de color verde brillante, algo más claras por el envés. El melocotonero está muy arraigado en la cultura asiática.
En Japón, el noble heroe Momotaro, una especie de Cid japonés, nació del interior de un enorme melocotón que flotaba río abajo.
En China se piensa que comer melocotón confiere longevidad al ser humano, ya que formaba parte de la dieta de sus dioses inmortales.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-48', 'Nogal Común', 'Frutales', '', 'Frutales Talavera S.A', '', 400, 9.00, 7.00);
INSERT INTO bdjardineria.producto VALUES ('FR-49', 'Parra Uva de Mesa', 'Frutales', '', 'Frutales Talavera S.A', '', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-5', 'Mandarino -Plantón joven', 'Frutales', '', 'Frutales Talavera S.A', '', 15, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('FR-50', 'Peral Castell', 'Frutales', '', 'Frutales Talavera S.A', 'Árbol piramidal, redondeado en su juventud, luego oval, que llega hasta 20 metros de altura y por término medio vive 65 años.Tronco alto, grueso, de corteza agrietada, gris, de la cual se destacan con frecuencia placas lenticulares.Las ramas se insertan formando ángulo agudo con el tronco (45º), de corteza lisa, primero verde y luego gris-violácea, con numerosas lenticelas.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-51', 'Peral Williams', 'Frutales', '', 'Frutales Talavera S.A', 'Árbol piramidal, redondeado en su juventud, luego oval, que llega hasta 20 metros de altura y por término medio vive 65 años.Tronco alto, grueso, de corteza agrietada, gris, de la cual se destacan con frecuencia placas lenticulares.Las ramas se insertan formando ángulo agudo con el tronco (45º), de corteza lisa, primero verde y luego gris-violácea, con numerosas lenticelas.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-52', 'Peral Conference', 'Frutales', '', 'Frutales Talavera S.A', 'Árbol piramidal, redondeado en su juventud, luego oval, que llega hasta 20 metros de altura y por término medio vive 65 años.Tronco alto, grueso, de corteza agrietada, gris, de la cual se destacan con frecuencia placas lenticulares.Las ramas se insertan formando ángulo agudo con el tronco (45º), de corteza lisa, primero verde y luego gris-violácea, con numerosas lenticelas.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-53', 'Peral Blanq. de Aranjuez', 'Frutales', '', 'Frutales Talavera S.A', 'Árbol piramidal, redondeado en su juventud, luego oval, que llega hasta 20 metros de altura y por término medio vive 65 años.Tronco alto, grueso, de corteza agrietada, gris, de la cual se destacan con frecuencia placas lenticulares.Las ramas se insertan formando ángulo agudo con el tronco (45º), de corteza lisa, primero verde y luego gris-violácea, con numerosas lenticelas.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-54', 'Níspero Tanaca', 'Frutales', '', 'Frutales Talavera S.A', 'Aunque originario del Sudeste de China, el níspero llegó a Europa procedente de Japón en el siglo XVIII como árbol ornamental. En el siglo XIX se inició el consumo de los frutos en toda el área mediterránea, donde se adaptó muy bien a las zonas de cultivo de los cítricos.El cultivo intensivo comenzó a desarrollarse a finales de los años 60 y principios de los 70, cuando comenzaron a implantarse las variedades y técnicas de cultivo actualmente utilizadas.', 400, 9.00, 7.00);
INSERT INTO bdjardineria.producto VALUES ('FR-55', 'Olivo Cipresino', 'Frutales', '', 'Frutales Talavera S.A', 'Existen dos hipótesis sobre el origen del olivo, una que postula que proviene de las costas de Siria, Líbano e Israel y otra que considera que lo considera originario de Asia menor. La llegada a Europa probablemente tuvo lugar de mano de los Fenicios, en transito por Chipre, Creta, e Islas del Mar Egeo, pasando a Grecia y más tarde a Italia. Los primeros indicios de la presencia del olivo en las costas mediterráneas españolas coinciden con el dominio romano, aunque fueron posteriormente los árabes los que impulsaron su cultivo en Andalucía, convirtiendo a España en el primer país productor de aceite de oliva a nivel mundial.', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-56', 'Nectarina', 'Frutales', '', 'Frutales Talavera S.A', '', 400, 8.00, 6.00);
INSERT INTO bdjardineria.producto VALUES ('FR-57', 'Kaki Rojo Brillante', 'Frutales', '', 'NaranjasValencianas.com', 'De crecimiento algo lento los primeros años, llega a alcanzar hasta doce metros de altura o más, aunque en cultivo se prefiere algo más bajo (5-6). Tronco corto y copa extendida. Ramifica muy poco debido a la dominancia apical. Porte más o menos piramidal, aunque con la edad se hace más globoso.', 400, 9.00, 7.00);
INSERT INTO bdjardineria.producto VALUES ('FR-58', 'Albaricoquero', 'Frutales', '8/10', 'Melocotones de Cieza S.A.', 'árbol que puede pasar de los 6 m de altura, en la región mediterránea con ramas formando una copa redondeada. La corteza del tronco es pardo-violácea, agrietada; las ramas son rojizas y extendidas cuando jóvenes y las ramas secundarias son cortas, divergentes y escasas. Las yemas latentes son frecuentes especialmente sobre las ramas viejas.', 200, 11.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('FR-59', 'Albaricoquero', 'Frutales', '10/12', 'Melocotones de Cieza S.A.', 'árbol que puede pasar de los 6 m de altura, en la región mediterránea con ramas formando una copa redondeada. La corteza del tronco es pardo-violácea, agrietada; las ramas son rojizas y extendidas cuando jóvenes y las ramas secundarias son cortas, divergentes y escasas. Las yemas latentes son frecuentes especialmente sobre las ramas viejas.', 200, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('FR-6', 'Mandarino 2 años injerto', 'Frutales', '', 'Frutales Talavera S.A', '', 15, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('FR-60', 'Albaricoquero', 'Frutales', '12/14', 'Melocotones de Cieza S.A.', 'árbol que puede pasar de los 6 m de altura, en la región mediterránea con ramas formando una copa redondeada. La corteza del tronco es pardo-violácea, agrietada; las ramas son rojizas y extendidas cuando jóvenes y las ramas secundarias son cortas, divergentes y escasas. Las yemas latentes son frecuentes especialmente sobre las ramas viejas.', 200, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('FR-61', 'Albaricoquero', 'Frutales', '14/16', 'Melocotones de Cieza S.A.', 'árbol que puede pasar de los 6 m de altura, en la región mediterránea con ramas formando una copa redondeada. La corteza del tronco es pardo-violácea, agrietada; las ramas son rojizas y extendidas cuando jóvenes y las ramas secundarias son cortas, divergentes y escasas. Las yemas latentes son frecuentes especialmente sobre las ramas viejas.', 200, 49.00, 39.00);
INSERT INTO bdjardineria.producto VALUES ('FR-62', 'Albaricoquero', 'Frutales', '16/18', 'Melocotones de Cieza S.A.', 'árbol que puede pasar de los 6 m de altura, en la región mediterránea con ramas formando una copa redondeada. La corteza del tronco es pardo-violácea, agrietada; las ramas son rojizas y extendidas cuando jóvenes y las ramas secundarias son cortas, divergentes y escasas. Las yemas latentes son frecuentes especialmente sobre las ramas viejas.', 200, 70.00, 56.00);
INSERT INTO bdjardineria.producto VALUES ('OR-126', 'Weigelia "Bristol Ruby"', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-191', 'Phylostachys Bambusa Spectabilis', 'Ornamentales', '180-200', 'Viveros EL OASIS', '', 80, 24.00, 19.00);
INSERT INTO bdjardineria.producto VALUES ('OR-192', 'Phylostachys biseti', 'Ornamentales', '160-170', 'Viveros EL OASIS', '', 80, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('FR-63', 'Cerezo', 'Frutales', '8/10', 'Jerte Distribuciones S.L.', 'Las principales especies de cerezo cultivadas en el mundo son el cerezo dulce (Prunus avium), el guindo (P. cerasus) y el cerezo "Duke", híbrido de los anteriores. Ambas especies son naturales del sureste de Europa y oeste de Asia. El cerezo dulce tuvo su origen probablemente en el mar Negro y en el mar Caspio, difundiéndose después hacia Europa y Asia, llevado por los pájaros y las migraciones humanas. Fue uno de los frutales más apreciados por los griegos y con el Imperio Romano se extendió a regiones muy diversas. En la actualidad, el cerezo se encuentra difundido por numerosas regiones y países del mundo con clima templado', 300, 11.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('FR-64', 'Cerezo', 'Frutales', '10/12', 'Jerte Distribuciones S.L.', 'Las principales especies de cerezo cultivadas en el mundo son el cerezo dulce (Prunus avium), el guindo (P. cerasus) y el cerezo "Duke", híbrido de los anteriores. Ambas especies son naturales del sureste de Europa y oeste de Asia. El cerezo dulce tuvo su origen probablemente en el mar Negro y en el mar Caspio, difundiéndose después hacia Europa y Asia, llevado por los pájaros y las migraciones humanas. Fue uno de los frutales más apreciados por los griegos y con el Imperio Romano se extendió a regiones muy diversas. En la actualidad, el cerezo se encuentra difundido por numerosas regiones y países del mundo con clima templado', 15, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('FR-65', 'Cerezo', 'Frutales', '12/14', 'Jerte Distribuciones S.L.', 'Las principales especies de cerezo cultivadas en el mundo son el cerezo dulce (Prunus avium), el guindo (P. cerasus) y el cerezo "Duke", híbrido de los anteriores. Ambas especies son naturales del sureste de Europa y oeste de Asia. El cerezo dulce tuvo su origen probablemente en el mar Negro y en el mar Caspio, difundiéndose después hacia Europa y Asia, llevado por los pájaros y las migraciones humanas. Fue uno de los frutales más apreciados por los griegos y con el Imperio Romano se extendió a regiones muy diversas. En la actualidad, el cerezo se encuentra difundido por numerosas regiones y países del mundo con clima templado', 200, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('FR-66', 'Cerezo', 'Frutales', '14/16', 'Jerte Distribuciones S.L.', 'Las principales especies de cerezo cultivadas en el mundo son el cerezo dulce (Prunus avium), el guindo (P. cerasus) y el cerezo "Duke", híbrido de los anteriores. Ambas especies son naturales del sureste de Europa y oeste de Asia. El cerezo dulce tuvo su origen probablemente en el mar Negro y en el mar Caspio, difundiéndose después hacia Europa y Asia, llevado por los pájaros y las migraciones humanas. Fue uno de los frutales más apreciados por los griegos y con el Imperio Romano se extendió a regiones muy diversas. En la actualidad, el cerezo se encuentra difundido por numerosas regiones y países del mundo con clima templado', 50, 49.00, 39.00);
INSERT INTO bdjardineria.producto VALUES ('FR-67', 'Cerezo', 'Frutales', '16/18', 'Jerte Distribuciones S.L.', 'Las principales especies de cerezo cultivadas en el mundo son el cerezo dulce (Prunus avium), el guindo (P. cerasus) y el cerezo "Duke", híbrido de los anteriores. Ambas especies son naturales del sureste de Europa y oeste de Asia. El cerezo dulce tuvo su origen probablemente en el mar Negro y en el mar Caspio, difundiéndose después hacia Europa y Asia, llevado por los pájaros y las migraciones humanas. Fue uno de los frutales más apreciados por los griegos y con el Imperio Romano se extendió a regiones muy diversas. En la actualidad, el cerezo se encuentra difundido por numerosas regiones y países del mundo con clima templado', 50, 70.00, 56.00);
INSERT INTO bdjardineria.producto VALUES ('FR-68', 'Cerezo', 'Frutales', '18/20', 'Jerte Distribuciones S.L.', 'Las principales especies de cerezo cultivadas en el mundo son el cerezo dulce (Prunus avium), el guindo (P. cerasus) y el cerezo "Duke", híbrido de los anteriores. Ambas especies son naturales del sureste de Europa y oeste de Asia. El cerezo dulce tuvo su origen probablemente en el mar Negro y en el mar Caspio, difundiéndose después hacia Europa y Asia, llevado por los pájaros y las migraciones humanas. Fue uno de los frutales más apreciados por los griegos y con el Imperio Romano se extendió a regiones muy diversas. En la actualidad, el cerezo se encuentra difundido por numerosas regiones y países del mundo con clima templado', 50, 80.00, 64.00);
INSERT INTO bdjardineria.producto VALUES ('FR-69', 'Cerezo', 'Frutales', '20/25', 'Jerte Distribuciones S.L.', 'Las principales especies de cerezo cultivadas en el mundo son el cerezo dulce (Prunus avium), el guindo (P. cerasus) y el cerezo "Duke", híbrido de los anteriores. Ambas especies son naturales del sureste de Europa y oeste de Asia. El cerezo dulce tuvo su origen probablemente en el mar Negro y en el mar Caspio, difundiéndose después hacia Europa y Asia, llevado por los pájaros y las migraciones humanas. Fue uno de los frutales más apreciados por los griegos y con el Imperio Romano se extendió a regiones muy diversas. En la actualidad, el cerezo se encuentra difundido por numerosas regiones y países del mundo con clima templado', 50, 91.00, 72.00);
INSERT INTO bdjardineria.producto VALUES ('FR-7', 'Mandarino calibre 8/10', 'Frutales', '', 'Frutales Talavera S.A', '', 15, 29.00, 23.00);
INSERT INTO bdjardineria.producto VALUES ('FR-70', 'Ciruelo', 'Frutales', '8/10', 'Frutales Talavera S.A', 'árbol de tamaño mediano que alcanza una altura máxima de 5-6 m. Tronco de corteza pardo-azulada, brillante, lisa o agrietada longitudinalmente. Produce ramas alternas, pequeñas, delgadas, unas veces lisas, glabras y otras pubescentes y vellosas', 50, 11.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('FR-71', 'Ciruelo', 'Frutales', '10/12', 'Frutales Talavera S.A', 'árbol de tamaño mediano que alcanza una altura máxima de 5-6 m. Tronco de corteza pardo-azulada, brillante, lisa o agrietada longitudinalmente. Produce ramas alternas, pequeñas, delgadas, unas veces lisas, glabras y otras pubescentes y vellosas', 50, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('FR-72', 'Ciruelo', 'Frutales', '12/14', 'Frutales Talavera S.A', 'árbol de tamaño mediano que alcanza una altura máxima de 5-6 m. Tronco de corteza pardo-azulada, brillante, lisa o agrietada longitudinalmente. Produce ramas alternas, pequeñas, delgadas, unas veces lisas, glabras y otras pubescentes y vellosas', 50, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('FR-73', 'Granado', 'Frutales', '8/10', 'Frutales Talavera S.A', 'pequeño árbol caducifolio, a veces con porte arbustivo, de 3 a 6 m de altura, con el tronco retorcido. Madera dura y corteza escamosa de color grisáceo. Las ramitas jóvenes son más o menos cuadrangulares o angostas y de cuatro alas, posteriormente se vuelven redondas con corteza de color café grisáceo, la mayoría de las ramas, pero especialmente las pequeñas ramitas axilares, son en forma de espina o terminan en una espina aguda; la copa es extendida.', 50, 13.00, 10.00);
INSERT INTO bdjardineria.producto VALUES ('FR-74', 'Granado', 'Frutales', '10/12', 'Frutales Talavera S.A', 'pequeño árbol caducifolio, a veces con porte arbustivo, de 3 a 6 m de altura, con el tronco retorcido. Madera dura y corteza escamosa de color grisáceo. Las ramitas jóvenes son más o menos cuadrangulares o angostas y de cuatro alas, posteriormente se vuelven redondas con corteza de color café grisáceo, la mayoría de las ramas, pero especialmente las pequeñas ramitas axilares, son en forma de espina o terminan en una espina aguda; la copa es extendida.', 50, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('FR-75', 'Granado', 'Frutales', '12/14', 'Frutales Talavera S.A', 'pequeño árbol caducifolio, a veces con porte arbustivo, de 3 a 6 m de altura, con el tronco retorcido. Madera dura y corteza escamosa de color grisáceo. Las ramitas jóvenes son más o menos cuadrangulares o angostas y de cuatro alas, posteriormente se vuelven redondas con corteza de color café grisáceo, la mayoría de las ramas, pero especialmente las pequeñas ramitas axilares, son en forma de espina o terminan en una espina aguda; la copa es extendida.', 50, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('FR-76', 'Granado', 'Frutales', '14/16', 'Frutales Talavera S.A', 'pequeño árbol caducifolio, a veces con porte arbustivo, de 3 a 6 m de altura, con el tronco retorcido. Madera dura y corteza escamosa de color grisáceo. Las ramitas jóvenes son más o menos cuadrangulares o angostas y de cuatro alas, posteriormente se vuelven redondas con corteza de color café grisáceo, la mayoría de las ramas, pero especialmente las pequeñas ramitas axilares, son en forma de espina o terminan en una espina aguda; la copa es extendida.', 50, 49.00, 39.00);
INSERT INTO bdjardineria.producto VALUES ('FR-77', 'Granado', 'Frutales', '16/18', 'Frutales Talavera S.A', 'pequeño árbol caducifolio, a veces con porte arbustivo, de 3 a 6 m de altura, con el tronco retorcido. Madera dura y corteza escamosa de color grisáceo. Las ramitas jóvenes son más o menos cuadrangulares o angostas y de cuatro alas, posteriormente se vuelven redondas con corteza de color café grisáceo, la mayoría de las ramas, pero especialmente las pequeñas ramitas axilares, son en forma de espina o terminan en una espina aguda; la copa es extendida.', 50, 70.00, 56.00);
INSERT INTO bdjardineria.producto VALUES ('FR-78', 'Higuera', 'Frutales', '8/10', 'Frutales Talavera S.A', 'La higuera (Ficus carica L.) es un árbol típico de secano en los países mediterráneos. Su rusticidad y su fácil multiplicación hacen de la higuera un frutal muy apropiado para el cultivo extensivo.. Siempre ha sido considerado como árbol que no requiere cuidado alguno una vez plantado y arraigado, limitándose el hombre a recoger de él los frutos cuando maduran, unos para consumo en fresco y otros para conserva. Las únicas higueras con cuidados culturales esmerados, en muchas comarcas, son las brevales, por el interés económico de su primera cosecha, la de brevas.', 50, 15.00, 12.00);
INSERT INTO bdjardineria.producto VALUES ('FR-79', 'Higuera', 'Frutales', '10/12', 'Frutales Talavera S.A', 'La higuera (Ficus carica L.) es un árbol típico de secano en los países mediterráneos. Su rusticidad y su fácil multiplicación hacen de la higuera un frutal muy apropiado para el cultivo extensivo.. Siempre ha sido considerado como árbol que no requiere cuidado alguno una vez plantado y arraigado, limitándose el hombre a recoger de él los frutos cuando maduran, unos para consumo en fresco y otros para conserva. Las únicas higueras con cuidados culturales esmerados, en muchas comarcas, son las brevales, por el interés económico de su primera cosecha, la de brevas.', 50, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('FR-8', 'Limonero -Plantón joven', 'Frutales', '', 'NaranjasValencianas.com', 'El limonero, pertenece al grupo de los cítricos, teniendo su origen hace unos 20 millones de años en el sudeste asiático. Fue introducido por los árabes en el área mediterránea entre los años 1.000 a 1.200, habiendo experimentando numerosas modificaciones debidas tanto a la selección natural mediante hibridaciones espontáneas como a las producidas por el', 15, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('FR-80', 'Higuera', 'Frutales', '12/14', 'Frutales Talavera S.A', 'La higuera (Ficus carica L.) es un árbol típico de secano en los países mediterráneos. Su rusticidad y su fácil multiplicación hacen de la higuera un frutal muy apropiado para el cultivo extensivo.. Siempre ha sido considerado como árbol que no requiere cuidado alguno una vez plantado y arraigado, limitándose el hombre a recoger de él los frutos cuando maduran, unos para consumo en fresco y otros para conserva. Las únicas higueras con cuidados culturales esmerados, en muchas comarcas, son las brevales, por el interés económico de su primera cosecha, la de brevas.', 50, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('FR-81', 'Higuera', 'Frutales', '14/16', 'Frutales Talavera S.A', 'La higuera (Ficus carica L.) es un árbol típico de secano en los países mediterráneos. Su rusticidad y su fácil multiplicación hacen de la higuera un frutal muy apropiado para el cultivo extensivo.. Siempre ha sido considerado como árbol que no requiere cuidado alguno una vez plantado y arraigado, limitándose el hombre a recoger de él los frutos cuando maduran, unos para consumo en fresco y otros para conserva. Las únicas higueras con cuidados culturales esmerados, en muchas comarcas, son las brevales, por el interés económico de su primera cosecha, la de brevas.', 50, 49.00, 39.00);
INSERT INTO bdjardineria.producto VALUES ('FR-82', 'Higuera', 'Frutales', '16/18', 'Frutales Talavera S.A', 'La higuera (Ficus carica L.) es un árbol típico de secano en los países mediterráneos. Su rusticidad y su fácil multiplicación hacen de la higuera un frutal muy apropiado para el cultivo extensivo.. Siempre ha sido considerado como árbol que no requiere cuidado alguno una vez plantado y arraigado, limitándose el hombre a recoger de él los frutos cuando maduran, unos para consumo en fresco y otros para conserva. Las únicas higueras con cuidados culturales esmerados, en muchas comarcas, son las brevales, por el interés económico de su primera cosecha, la de brevas.', 50, 70.00, 56.00);
INSERT INTO bdjardineria.producto VALUES ('FR-83', 'Higuera', 'Frutales', '18/20', 'Frutales Talavera S.A', 'La higuera (Ficus carica L.) es un árbol típico de secano en los países mediterráneos. Su rusticidad y su fácil multiplicación hacen de la higuera un frutal muy apropiado para el cultivo extensivo.. Siempre ha sido considerado como árbol que no requiere cuidado alguno una vez plantado y arraigado, limitándose el hombre a recoger de él los frutos cuando maduran, unos para consumo en fresco y otros para conserva. Las únicas higueras con cuidados culturales esmerados, en muchas comarcas, son las brevales, por el interés económico de su primera cosecha, la de brevas.', 50, 80.00, 64.00);
INSERT INTO bdjardineria.producto VALUES ('FR-84', 'Kaki', 'Frutales', '8/10', 'NaranjasValencianas.com', 'De crecimiento algo lento los primeros años, llega a alcanzar hasta doce metros de altura o más, aunque en cultivo se prefiere algo más bajo (5-6). Tronco corto y copa extendida. Ramifica muy poco debido a la dominancia apical. Porte más o menos piramidal, aunque con la edad se hace más globoso.', 50, 13.00, 10.00);
INSERT INTO bdjardineria.producto VALUES ('FR-85', 'Kaki', 'Frutales', '16/18', 'NaranjasValencianas.com', 'De crecimiento algo lento los primeros años, llega a alcanzar hasta doce metros de altura o más, aunque en cultivo se prefiere algo más bajo (5-6). Tronco corto y copa extendida. Ramifica muy poco debido a la dominancia apical. Porte más o menos piramidal, aunque con la edad se hace más globoso.', 50, 70.00, 56.00);
INSERT INTO bdjardineria.producto VALUES ('FR-86', 'Manzano', 'Frutales', '8/10', 'Frutales Talavera S.A', 'alcanza como máximo 10 m. de altura y tiene una copa globosa. Tronco derecho que normalmente alcanza de 2 a 2,5 m. de altura, con corteza cubierta de lenticelas, lisa, adherida, de color ceniciento verdoso sobre los ramos y escamosa y gris parda sobre las partes viejas del árbol. Tiene una vida de unos 60-80 años. Las ramas se insertan en ángulo abierto sobre el tallo, de color verde oscuro, a veces tendiendo a negruzco o violáceo. Los brotes jóvenes terminan con frecuencia en una espina', 50, 11.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('FR-87', 'Manzano', 'Frutales', '10/12', 'Frutales Talavera S.A', 'alcanza como máximo 10 m. de altura y tiene una copa globosa. Tronco derecho que normalmente alcanza de 2 a 2,5 m. de altura, con corteza cubierta de lenticelas, lisa, adherida, de color ceniciento verdoso sobre los ramos y escamosa y gris parda sobre las partes viejas del árbol. Tiene una vida de unos 60-80 años. Las ramas se insertan en ángulo abierto sobre el tallo, de color verde oscuro, a veces tendiendo a negruzco o violáceo. Los brotes jóvenes terminan con frecuencia en una espina', 50, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('OR-127', 'Camelia japonica', 'Ornamentales', '40-60', 'Viveros EL OASIS', 'Arbusto excepcional por su floración otoñal, invernal o primaveral. Flores: Las flores son solitarias, aparecen en el ápice de cada rama, y son con una corola simple o doble, y comprendiendo varios colores. Suelen medir unos 7-12 cm de diÃ metro y tienen 5 sépalos y 5 pétalos. Estambres numerosos unidos en la mitad o en 2/3 de su longitud.', 50, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('FR-88', 'Manzano', 'Frutales', '12/14', 'Frutales Talavera S.A', 'alcanza como máximo 10 m. de altura y tiene una copa globosa. Tronco derecho que normalmente alcanza de 2 a 2,5 m. de altura, con corteza cubierta de lenticelas, lisa, adherida, de color ceniciento verdoso sobre los ramos y escamosa y gris parda sobre las partes viejas del árbol. Tiene una vida de unos 60-80 años. Las ramas se insertan en ángulo abierto sobre el tallo, de color verde oscuro, a veces tendiendo a negruzco o violáceo. Los brotes jóvenes terminan con frecuencia en una espina', 50, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('FR-89', 'Manzano', 'Frutales', '14/16', 'Frutales Talavera S.A', 'alcanza como máximo 10 m. de altura y tiene una copa globosa. Tronco derecho que normalmente alcanza de 2 a 2,5 m. de altura, con corteza cubierta de lenticelas, lisa, adherida, de color ceniciento verdoso sobre los ramos y escamosa y gris parda sobre las partes viejas del árbol. Tiene una vida de unos 60-80 años. Las ramas se insertan en ángulo abierto sobre el tallo, de color verde oscuro, a veces tendiendo a negruzco o violáceo. Los brotes jóvenes terminan con frecuencia en una espina', 50, 49.00, 39.00);
INSERT INTO bdjardineria.producto VALUES ('FR-9', 'Limonero calibre 8/10', 'Frutales', '', 'NaranjasValencianas.com', 'El limonero, pertenece al grupo de los cítricos, teniendo su origen hace unos 20 millones de años en el sudeste asiático. Fue introducido por los árabes en el área mediterránea entre los años 1.000 a 1.200, habiendo experimentando numerosas modificaciones debidas tanto a la selección natural mediante hibridaciones espontáneas como a las producidas por el', 15, 29.00, 23.00);
INSERT INTO bdjardineria.producto VALUES ('FR-90', 'Níspero', 'Frutales', '16/18', 'Frutales Talavera S.A', 'Aunque originario del Sudeste de China, el níspero llegó a Europa procedente de Japón en el siglo XVIII como árbol ornamental. En el siglo XIX se inició el consumo de los frutos en toda el área mediterránea, donde se adaptó muy bien a las zonas de cultivo de los cítricos.El cultivo intensivo comenzó a desarrollarse a finales de los años 60 y principios de los 70, cuando comenzaron a implantarse las variedades y técnicas de cultivo actualmente utilizadas.', 50, 70.00, 56.00);
INSERT INTO bdjardineria.producto VALUES ('FR-91', 'Níspero', 'Frutales', '18/20', 'Frutales Talavera S.A', 'Aunque originario del Sudeste de China, el níspero llegó a Europa procedente de Japón en el siglo XVIII como árbol ornamental. En el siglo XIX se inició el consumo de los frutos en toda el área mediterránea, donde se adaptó muy bien a las zonas de cultivo de los cítricos.El cultivo intensivo comenzó a desarrollarse a finales de los años 60 y principios de los 70, cuando comenzaron a implantarse las variedades y técnicas de cultivo actualmente utilizadas.', 50, 80.00, 64.00);
INSERT INTO bdjardineria.producto VALUES ('FR-92', 'Melocotonero', 'Frutales', '8/10', 'Melocotones de Cieza S.A.', 'Árbol caducifolio de porte bajo con corteza lisa, de color ceniciento. Sus hojas son alargadas con el margen ligeramente aserrado, de color verde brillante, algo más claras por el envés. El melocotonero está muy arraigado en la cultura asiática.
En Japón, el noble heroe Momotaro, una especie de Cid japonés, nació del interior de un enorme melocotón que flotaba río abajo.
En China se piensa que comer melocotón confiere longevidad al ser humano, ya que formaba parte de la dieta de sus dioses inmortales.', 50, 11.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('FR-93', 'Melocotonero', 'Frutales', '10/12', 'Melocotones de Cieza S.A.', 'Árbol caducifolio de porte bajo con corteza lisa, de color ceniciento. Sus hojas son alargadas con el margen ligeramente aserrado, de color verde brillante, algo más claras por el envés. El melocotonero está muy arraigado en la cultura asiática.
En Japón, el noble heroe Momotaro, una especie de Cid japonés, nació del interior de un enorme melocotón que flotaba río abajo.
En China se piensa que comer melocotón confiere longevidad al ser humano, ya que formaba parte de la dieta de sus dioses inmortales.', 50, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('FR-94', 'Melocotonero', 'Frutales', '12/14', 'Melocotones de Cieza S.A.', 'Árbol caducifolio de porte bajo con corteza lisa, de color ceniciento. Sus hojas son alargadas con el margen ligeramente aserrado, de color verde brillante, algo más claras por el envés. El melocotonero está muy arraigado en la cultura asiática.
En Japón, el noble heroe Momotaro, una especie de Cid japonés, nació del interior de un enorme melocotón que flotaba río abajo.
En China se piensa que comer melocotón confiere longevidad al ser humano, ya que formaba parte de la dieta de sus dioses inmortales.', 50, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('FR-95', 'Melocotonero', 'Frutales', '14/16', 'Melocotones de Cieza S.A.', 'Árbol caducifolio de porte bajo con corteza lisa, de color ceniciento. Sus hojas son alargadas con el margen ligeramente aserrado, de color verde brillante, algo más claras por el envés. El melocotonero está muy arraigado en la cultura asiática.
En Japón, el noble heroe Momotaro, una especie de Cid japonés, nació del interior de un enorme melocotón que flotaba río abajo.
En China se piensa que comer melocotón confiere longevidad al ser humano, ya que formaba parte de la dieta de sus dioses inmortales.', 50, 49.00, 39.00);
INSERT INTO bdjardineria.producto VALUES ('FR-96', 'Membrillero', 'Frutales', '8/10', 'Frutales Talavera S.A', 'arbolito caducifolio de 4-6 m de altura con el tronco tortuoso y la corteza lisa, grisácea, que se desprende en escamas con la edad. Copa irregular, con ramas inermes, flexuosas, parduzcas, punteadas. Ramillas jóvenes tomentosas', 50, 11.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('FR-97', 'Membrillero', 'Frutales', '10/12', 'Frutales Talavera S.A', 'arbolito caducifolio de 4-6 m de altura con el tronco tortuoso y la corteza lisa, grisácea, que se desprende en escamas con la edad. Copa irregular, con ramas inermes, flexuosas, parduzcas, punteadas. Ramillas jóvenes tomentosas', 50, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('FR-98', 'Membrillero', 'Frutales', '12/14', 'Frutales Talavera S.A', 'arbolito caducifolio de 4-6 m de altura con el tronco tortuoso y la corteza lisa, grisácea, que se desprende en escamas con la edad. Copa irregular, con ramas inermes, flexuosas, parduzcas, punteadas. Ramillas jóvenes tomentosas', 50, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('FR-99', 'Membrillero', 'Frutales', '14/16', 'Frutales Talavera S.A', 'arbolito caducifolio de 4-6 m de altura con el tronco tortuoso y la corteza lisa, grisácea, que se desprende en escamas con la edad. Copa irregular, con ramas inermes, flexuosas, parduzcas, punteadas. Ramillas jóvenes tomentosas', 50, 49.00, 39.00);
INSERT INTO bdjardineria.producto VALUES ('OR-001', 'Arbustos Mix Maceta', 'Ornamentales', '40-60', 'Valencia Garden Service', '', 25, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-100', 'Mimosa Injerto CLASICA Dealbata ', 'Ornamentales', '100-110', 'Viveros EL OASIS', 'Acacia dealbata. nombre común o vulgar: Mimosa fina, Mimosa, Mimosa común, Mimosa plateada, Aromo francés. Familia: Mimosaceae. Origen: Australia, Sureste, (N. G. del Sur y Victoria). Arbol de follaje persistente muy usado en parques por su atractiva floración amarilla hacia fines del invierno. Altura: de 3 a 10 metros generalmente. Crecimiento rápido. Follaje perenne de tonos plateados, muy ornamental. Sus hojas son de textura fina, de color verde y sus flores amarillas que aparecen en racimos grandes. Florece de Enero a Marzo (Hemisferio Norte). Legumbre de 5-9 cm de longitud, recta o ligeramente curvada, con los bordes algo constreñidos entre las semillas, que se disponen en el fruto longitudinalmente...', 100, 12.00, 9.00);
INSERT INTO bdjardineria.producto VALUES ('OR-118', 'Hibiscus Syriacus "Pink Giant" Rosa', 'Ornamentales', '35-45', 'Viveros EL OASIS', 'Por su capacidad de soportar podas, pueden ser fácilmente moldeadas como bonsái en el transcurso de pocos años. Flores de muchos colores según la variedad, desde el blanco puro al rojo intenso, del amarillo al anaranjado. La flor apenas dura 1 día, pero continuamente aparecen nuevas y la floración se prolonga durante todo el periodo de crecimiento vegetativo.', 120, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('OR-101', 'Expositor Mimosa Semilla Mix', 'Ornamentales', '170-200', 'Viveros EL OASIS', 'Acacia dealbata. nombre común o vulgar: Mimosa fina, Mimosa, Mimosa común, Mimosa plateada, Aromo francés. Familia: Mimosaceae. Origen: Australia, Sureste, (N. G. del Sur y Victoria). Arbol de follaje persistente muy usado en parques por su atractiva floración amarilla hacia fines del invierno. Altura: de 3 a 10 metros generalmente. Crecimiento rápido. Follaje perenne de tonos plateados, muy ornamental. Sus hojas son de textura fina, de color verde y sus flores amarillas que aparecen en racimos grandes. Florece de Enero a Marzo (Hemisferio Norte). Legumbre de 5-9 cm de longitud, recta o ligeramente curvada, con los bordes algo constreñidos entre las semillas, que se disponen en el fruto longitudinalmente...', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-102', 'Mimosa Semilla Bayleyana  ', 'Ornamentales', '170-200', 'Viveros EL OASIS', 'Acacia dealbata. nombre común o vulgar: Mimosa fina, Mimosa, Mimosa común, Mimosa plateada, Aromo francés. Familia: Mimosaceae. Origen: Australia, Sureste, (N. G. del Sur y Victoria). Arbol de follaje persistente muy usado en parques por su atractiva floración amarilla hacia fines del invierno. Altura: de 3 a 10 metros generalmente. Crecimiento rápido. Follaje perenne de tonos plateados, muy ornamental. Sus hojas son de textura fina, de color verde y sus flores amarillas que aparecen en racimos grandes. Florece de Enero a Marzo (Hemisferio Norte). Legumbre de 5-9 cm de longitud, recta o ligeramente curvada, con los bordes algo constreñidos entre las semillas, que se disponen en el fruto longitudinalmente...', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-103', 'Mimosa Semilla Bayleyana   ', 'Ornamentales', '200-225', 'Viveros EL OASIS', 'Acacia dealbata. nombre común o vulgar: Mimosa fina, Mimosa, Mimosa común, Mimosa plateada, Aromo francés. Familia: Mimosaceae. Origen: Australia, Sureste, (N. G. del Sur y Victoria). Arbol de follaje persistente muy usado en parques por su atractiva floración amarilla hacia fines del invierno. Altura: de 3 a 10 metros generalmente. Crecimiento rápido. Follaje perenne de tonos plateados, muy ornamental. Sus hojas son de textura fina, de color verde y sus flores amarillas que aparecen en racimos grandes. Florece de Enero a Marzo (Hemisferio Norte). Legumbre de 5-9 cm de longitud, recta o ligeramente curvada, con los bordes algo constreñidos entre las semillas, que se disponen en el fruto longitudinalmente...', 100, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-104', 'Mimosa Semilla Cyanophylla    ', 'Ornamentales', '200-225', 'Viveros EL OASIS', 'Acacia dealbata. nombre común o vulgar: Mimosa fina, Mimosa, Mimosa común, Mimosa plateada, Aromo francés. Familia: Mimosaceae. Origen: Australia, Sureste, (N. G. del Sur y Victoria). Arbol de follaje persistente muy usado en parques por su atractiva floración amarilla hacia fines del invierno. Altura: de 3 a 10 metros generalmente. Crecimiento rápido. Follaje perenne de tonos plateados, muy ornamental. Sus hojas son de textura fina, de color verde y sus flores amarillas que aparecen en racimos grandes. Florece de Enero a Marzo (Hemisferio Norte). Legumbre de 5-9 cm de longitud, recta o ligeramente curvada, con los bordes algo constreñidos entre las semillas, que se disponen en el fruto longitudinalmente...', 100, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-105', 'Mimosa Semilla Espectabilis  ', 'Ornamentales', '160-170', 'Viveros EL OASIS', 'Acacia dealbata. nombre común o vulgar: Mimosa fina, Mimosa, Mimosa común, Mimosa plateada, Aromo francés. Familia: Mimosaceae. Origen: Australia, Sureste, (N. G. del Sur y Victoria). Arbol de follaje persistente muy usado en parques por su atractiva floración amarilla hacia fines del invierno. Altura: de 3 a 10 metros generalmente. Crecimiento rápido. Follaje perenne de tonos plateados, muy ornamental. Sus hojas son de textura fina, de color verde y sus flores amarillas que aparecen en racimos grandes. Florece de Enero a Marzo (Hemisferio Norte). Legumbre de 5-9 cm de longitud, recta o ligeramente curvada, con los bordes algo constreñidos entre las semillas, que se disponen en el fruto longitudinalmente...', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-106', 'Mimosa Semilla Longifolia   ', 'Ornamentales', '200-225', 'Viveros EL OASIS', 'Acacia dealbata. nombre común o vulgar: Mimosa fina, Mimosa, Mimosa común, Mimosa plateada, Aromo francés. Familia: Mimosaceae. Origen: Australia, Sureste, (N. G. del Sur y Victoria). Arbol de follaje persistente muy usado en parques por su atractiva floración amarilla hacia fines del invierno. Altura: de 3 a 10 metros generalmente. Crecimiento rápido. Follaje perenne de tonos plateados, muy ornamental. Sus hojas son de textura fina, de color verde y sus flores amarillas que aparecen en racimos grandes. Florece de Enero a Marzo (Hemisferio Norte). Legumbre de 5-9 cm de longitud, recta o ligeramente curvada, con los bordes algo constreñidos entre las semillas, que se disponen en el fruto longitudinalmente...', 100, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-107', 'Mimosa Semilla Floribunda 4 estaciones', 'Ornamentales', '120-140', 'Viveros EL OASIS', 'Acacia dealbata. nombre común o vulgar: Mimosa fina, Mimosa, Mimosa común, Mimosa plateada, Aromo francés. Familia: Mimosaceae. Origen: Australia, Sureste, (N. G. del Sur y Victoria). Arbol de follaje persistente muy usado en parques por su atractiva floración amarilla hacia fines del invierno. Altura: de 3 a 10 metros generalmente. Crecimiento rápido. Follaje perenne de tonos plateados, muy ornamental. Sus hojas son de textura fina, de color verde y sus flores amarillas que aparecen en racimos grandes. Florece de Enero a Marzo (Hemisferio Norte). Legumbre de 5-9 cm de longitud, recta o ligeramente curvada, con los bordes algo constreñidos entre las semillas, que se disponen en el fruto longitudinalmente...', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-108', 'Abelia Floribunda', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 100, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-109', 'Callistemom (Mix)', 'Ornamentales', '35-45', 'Viveros EL OASIS', 'Limpitatubos. arbolito de 6-7 m de altura. Ramas flexibles y colgantes (de ahí lo de "llorón")..', 100, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-110', 'Callistemom (Mix)', 'Ornamentales', '40-60', 'Viveros EL OASIS', 'Limpitatubos. arbolito de 6-7 m de altura. Ramas flexibles y colgantes (de ahí lo de "llorón")..', 100, 2.00, 1.00);
INSERT INTO bdjardineria.producto VALUES ('OR-111', 'Corylus Avellana "Contorta"', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 100, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-112', 'Escallonia (Mix)', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-113', 'Evonimus Emerald Gayeti', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-114', 'Evonimus Pulchellus', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-115', 'Forsytia Intermedia "Lynwood"', 'Ornamentales', '35-45', 'Viveros EL OASIS', '', 120, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('OR-116', 'Hibiscus Syriacus  "Diana" -Blanco Puro', 'Ornamentales', '35-45', 'Viveros EL OASIS', 'Por su capacidad de soportar podas, pueden ser fácilmente moldeadas como bonsái en el transcurso de pocos años. Flores de muchos colores según la variedad, desde el blanco puro al rojo intenso, del amarillo al anaranjado. La flor apenas dura 1 día, pero continuamente aparecen nuevas y la floración se prolonga durante todo el periodo de crecimiento vegetativo.', 120, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('OR-117', 'Hibiscus Syriacus  "Helene" -Blanco-C.rojo', 'Ornamentales', '35-45', 'Viveros EL OASIS', 'Por su capacidad de soportar podas, pueden ser fácilmente moldeadas como bonsái en el transcurso de pocos años. Flores de muchos colores según la variedad, desde el blanco puro al rojo intenso, del amarillo al anaranjado. La flor apenas dura 1 día, pero continuamente aparecen nuevas y la floración se prolonga durante todo el periodo de crecimiento vegetativo.', 120, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('OR-119', 'Laurus Nobilis Arbusto - Ramificado Bajo', 'Ornamentales', '40-50', 'Viveros EL OASIS', '', 120, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-128', 'Camelia japonica ejemplar', 'Ornamentales', '200-250', 'Viveros EL OASIS', 'Arbusto excepcional por su floración otoñal, invernal o primaveral. Flores: Las flores son solitarias, aparecen en el ápice de cada rama, y son con una corola simple o doble, y comprendiendo varios colores. Suelen medir unos 7-12 cm de diÃ metro y tienen 5 sépalos y 5 pétalos. Estambres numerosos unidos en la mitad o en 2/3 de su longitud.', 50, 98.00, 78.00);
INSERT INTO bdjardineria.producto VALUES ('OR-129', 'Camelia japonica ejemplar', 'Ornamentales', '250-300', 'Viveros EL OASIS', 'Arbusto excepcional por su floración otoñal, invernal o primaveral. Flores: Las flores son solitarias, aparecen en el ápice de cada rama, y son con una corola simple o doble, y comprendiendo varios colores. Suelen medir unos 7-12 cm de diÃ metro y tienen 5 sépalos y 5 pétalos. Estambres numerosos unidos en la mitad o en 2/3 de su longitud.', 50, 110.00, 88.00);
INSERT INTO bdjardineria.producto VALUES ('OR-130', 'Callistemom COPA', 'Ornamentales', '110/120', 'Viveros EL OASIS', 'Limpitatubos. arbolito de 6-7 m de altura. Ramas flexibles y colgantes (de ahí lo de "llorón")..', 50, 18.00, 14.00);
INSERT INTO bdjardineria.producto VALUES ('OR-131', 'Leptospermum formado PIRAMIDE', 'Ornamentales', '80-100', 'Viveros EL OASIS', '', 50, 18.00, 14.00);
INSERT INTO bdjardineria.producto VALUES ('OR-132', 'Leptospermum COPA', 'Ornamentales', '110/120', 'Viveros EL OASIS', '', 50, 18.00, 14.00);
INSERT INTO bdjardineria.producto VALUES ('OR-133', 'Nerium oleander-CALIDAD "GARDEN"', 'Ornamentales', '40-45', 'Viveros EL OASIS', '', 50, 2.00, 1.00);
INSERT INTO bdjardineria.producto VALUES ('OR-134', 'Nerium Oleander Arbusto GRANDE', 'Ornamentales', '160-200', 'Viveros EL OASIS', '', 100, 38.00, 30.00);
INSERT INTO bdjardineria.producto VALUES ('OR-135', 'Nerium oleander COPA  Calibre 6/8', 'Ornamentales', '50-60', 'Viveros EL OASIS', '', 100, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-136', 'Nerium oleander ARBOL Calibre 8/10', 'Ornamentales', '225-250', 'Viveros EL OASIS', '', 100, 18.00, 14.00);
INSERT INTO bdjardineria.producto VALUES ('OR-137', 'ROSAL TREPADOR', 'Ornamentales', '', 'Viveros EL OASIS', '', 100, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-138', 'Camelia Blanco, Chrysler Rojo, Soraya Naranja, ', 'Ornamentales', '', 'Viveros EL OASIS', '', 100, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-139', 'Landora Amarillo, Rose Gaujard bicolor blanco-rojo', 'Ornamentales', '', 'Viveros EL OASIS', '', 100, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-140', 'Kordes Perfect bicolor rojo-amarillo, Roundelay rojo fuerte', 'Ornamentales', '', 'Viveros EL OASIS', '', 100, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-141', 'Pitimini rojo', 'Ornamentales', '', 'Viveros EL OASIS', '', 100, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-142', 'Solanum Jazminoide', 'Ornamentales', '150-160', 'Viveros EL OASIS', '', 100, 2.00, 1.00);
INSERT INTO bdjardineria.producto VALUES ('OR-143', 'Wisteria Sinensis  azul, rosa, blanca', 'Ornamentales', '', 'Viveros EL OASIS', '', 100, 9.00, 7.00);
INSERT INTO bdjardineria.producto VALUES ('OR-144', 'Wisteria Sinensis INJERTADAS DECÃ', 'Ornamentales', '140-150', 'Viveros EL OASIS', '', 100, 12.00, 9.00);
INSERT INTO bdjardineria.producto VALUES ('OR-145', 'Bougamvillea Sanderiana Tutor', 'Ornamentales', '80-100', 'Viveros EL OASIS', '', 100, 2.00, 1.00);
INSERT INTO bdjardineria.producto VALUES ('OR-146', 'Bougamvillea Sanderiana Tutor', 'Ornamentales', '125-150', 'Viveros EL OASIS', '', 100, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-147', 'Bougamvillea Sanderiana Tutor', 'Ornamentales', '180-200', 'Viveros EL OASIS', '', 100, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('OR-148', 'Bougamvillea Sanderiana Espaldera', 'Ornamentales', '45-50', 'Viveros EL OASIS', '', 100, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('OR-149', 'Bougamvillea Sanderiana Espaldera', 'Ornamentales', '140-150', 'Viveros EL OASIS', '', 100, 17.00, 13.00);
INSERT INTO bdjardineria.producto VALUES ('OR-150', 'Bougamvillea roja, naranja', 'Ornamentales', '110-130', 'Viveros EL OASIS', '', 100, 2.00, 1.00);
INSERT INTO bdjardineria.producto VALUES ('OR-151', 'Bougamvillea Sanderiana, 3 tut. piramide', 'Ornamentales', '', 'Viveros EL OASIS', '', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-152', 'Expositor Árboles clima continental', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-153', 'Expositor Árboles clima mediterráneo', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-154', 'Expositor Árboles borde del mar', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-155', 'Acer Negundo  ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-156', 'Acer platanoides  ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 100, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-157', 'Acer Pseudoplatanus ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 100, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-158', 'Brachychiton Acerifolius  ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-159', 'Brachychiton Discolor  ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-160', 'Brachychiton Rupestris', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 100, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-161', 'Cassia Corimbosa  ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 100, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-162', 'Cassia Corimbosa ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 100, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-163', 'Chitalpa Summer Bells   ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-164', 'Erytrina Kafra', 'Ornamentales', '170-180', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-165', 'Erytrina Kafra', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-166', 'Eucalyptus Citriodora  ', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-167', 'Eucalyptus Ficifolia  ', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-168', 'Eucalyptus Ficifolia   ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-169', 'Hibiscus Syriacus  Var. Injertadas 1 Tallo ', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 80, 12.00, 9.00);
INSERT INTO bdjardineria.producto VALUES ('OR-170', 'Lagunaria Patersonii  ', 'Ornamentales', '140-150', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-171', 'Lagunaria Patersonii   ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-172', 'Lagunaria patersonii  calibre 8/10', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 18.00, 14.00);
INSERT INTO bdjardineria.producto VALUES ('OR-173', 'Morus Alba  ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-174', 'Morus Alba  calibre 8/10', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 18.00, 14.00);
INSERT INTO bdjardineria.producto VALUES ('OR-175', 'Platanus Acerifolia   ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-176', 'Prunus pisardii  ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-177', 'Robinia Pseudoacacia Casque Rouge   ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 15.00, 12.00);
INSERT INTO bdjardineria.producto VALUES ('OR-178', 'Salix Babylonica  Pendula  ', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-179', 'Sesbania Punicea   ', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-180', 'Tamarix  Ramosissima Pink Cascade   ', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-181', 'Tamarix  Ramosissima Pink Cascade   ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-182', 'Tecoma Stands   ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-183', 'Tecoma Stands  ', 'Ornamentales', '200-225', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-184', 'Tipuana Tipu  ', 'Ornamentales', '170-200', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-185', 'Pleioblastus distichus-Bambú enano', 'Ornamentales', '15-20', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-186', 'Sasa palmata ', 'Ornamentales', '20-30', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-187', 'Sasa palmata ', 'Ornamentales', '40-45', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-188', 'Sasa palmata ', 'Ornamentales', '50-60', 'Viveros EL OASIS', '', 80, 25.00, 20.00);
INSERT INTO bdjardineria.producto VALUES ('OR-189', 'Phylostachys aurea', 'Ornamentales', '180-200', 'Viveros EL OASIS', '', 80, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('OR-190', 'Phylostachys aurea', 'Ornamentales', '250-300', 'Viveros EL OASIS', '', 80, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('OR-193', 'Phylostachys biseti', 'Ornamentales', '160-180', 'Viveros EL OASIS', '', 80, 20.00, 16.00);
INSERT INTO bdjardineria.producto VALUES ('OR-194', 'Pseudosasa japonica (Metake)', 'Ornamentales', '225-250', 'Viveros EL OASIS', '', 80, 20.00, 16.00);
INSERT INTO bdjardineria.producto VALUES ('OR-195', 'Pseudosasa japonica (Metake) ', 'Ornamentales', '30-40', 'Viveros EL OASIS', '', 80, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-196', 'Cedrus Deodara ', 'Ornamentales', '80-100', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-197', 'Cedrus Deodara "Feeling Blue" Novedad', 'Ornamentales', 'rastrero', 'Viveros EL OASIS', '', 80, 12.00, 9.00);
INSERT INTO bdjardineria.producto VALUES ('OR-198', 'Juniperus chinensis "Blue Alps"', 'Ornamentales', '20-30', 'Viveros EL OASIS', '', 80, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-199', 'Juniperus Chinensis Stricta', 'Ornamentales', '20-30', 'Viveros EL OASIS', '', 80, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-200', 'Juniperus horizontalis Wiltonii', 'Ornamentales', '20-30', 'Viveros EL OASIS', '', 80, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-201', 'Juniperus squamata "Blue Star"', 'Ornamentales', '20-30', 'Viveros EL OASIS', '', 80, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-202', 'Juniperus x media Phitzeriana verde', 'Ornamentales', '20-30', 'Viveros EL OASIS', '', 80, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-203', 'Pinus Canariensis', 'Ornamentales', '80-100', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-204', 'Pinus Halepensis', 'Ornamentales', '160-180', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-205', 'Pinus Pinea -Pino Piñonero', 'Ornamentales', '70-80', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-206', 'Thuja Esmeralda ', 'Ornamentales', '80-100', 'Viveros EL OASIS', '', 80, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-207', 'Tuja Occidentalis Woodwardii', 'Ornamentales', '20-30', 'Viveros EL OASIS', '', 80, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-208', 'Tuja orientalis "Aurea nana"', 'Ornamentales', '20-30', 'Viveros EL OASIS', '', 80, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-209', 'Archontophoenix Cunninghamiana', 'Ornamentales', '80 - 100', 'Viveros EL OASIS', '', 80, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-210', 'Beucarnea Recurvata', 'Ornamentales', '130  - 150', 'Viveros EL OASIS', '', 2, 39.00, 31.00);
INSERT INTO bdjardineria.producto VALUES ('OR-211', 'Beucarnea Recurvata', 'Ornamentales', '180 - 200', 'Viveros EL OASIS', '', 5, 59.00, 47.00);
INSERT INTO bdjardineria.producto VALUES ('OR-212', 'Bismarckia Nobilis', 'Ornamentales', '200 - 220', 'Viveros EL OASIS', '', 4, 217.00, 173.00);
INSERT INTO bdjardineria.producto VALUES ('OR-213', 'Bismarckia Nobilis', 'Ornamentales', '240 - 260', 'Viveros EL OASIS', '', 4, 266.00, 212.00);
INSERT INTO bdjardineria.producto VALUES ('OR-214', 'Brahea Armata', 'Ornamentales', '45 - 60', 'Viveros EL OASIS', '', 0, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-215', 'Brahea Armata', 'Ornamentales', '120 - 140', 'Viveros EL OASIS', '', 100, 112.00, 89.00);
INSERT INTO bdjardineria.producto VALUES ('OR-216', 'Brahea Edulis', 'Ornamentales', '80 - 100', 'Viveros EL OASIS', '', 100, 19.00, 15.00);
INSERT INTO bdjardineria.producto VALUES ('OR-217', 'Brahea Edulis', 'Ornamentales', '140 - 160', 'Viveros EL OASIS', '', 100, 64.00, 51.00);
INSERT INTO bdjardineria.producto VALUES ('OR-218', 'Butia Capitata', 'Ornamentales', '70 - 90', 'Viveros EL OASIS', '', 100, 25.00, 20.00);
INSERT INTO bdjardineria.producto VALUES ('OR-219', 'Butia Capitata', 'Ornamentales', '90 - 110', 'Viveros EL OASIS', '', 100, 29.00, 23.00);
INSERT INTO bdjardineria.producto VALUES ('OR-220', 'Butia Capitata', 'Ornamentales', '90 - 120', 'Viveros EL OASIS', '', 100, 36.00, 28.00);
INSERT INTO bdjardineria.producto VALUES ('OR-221', 'Butia Capitata', 'Ornamentales', '85 - 105', 'Viveros EL OASIS', '', 100, 59.00, 47.00);
INSERT INTO bdjardineria.producto VALUES ('OR-222', 'Butia Capitata', 'Ornamentales', '130 - 150', 'Viveros EL OASIS', '', 100, 87.00, 69.00);
INSERT INTO bdjardineria.producto VALUES ('OR-223', 'Chamaerops Humilis', 'Ornamentales', '40 - 45', 'Viveros EL OASIS', '', 100, 4.00, 3.00);
INSERT INTO bdjardineria.producto VALUES ('OR-224', 'Chamaerops Humilis', 'Ornamentales', '50 - 60', 'Viveros EL OASIS', '', 100, 7.00, 5.00);
INSERT INTO bdjardineria.producto VALUES ('OR-225', 'Chamaerops Humilis', 'Ornamentales', '70 - 90', 'Viveros EL OASIS', '', 100, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-226', 'Chamaerops Humilis', 'Ornamentales', '115 - 130', 'Viveros EL OASIS', '', 100, 38.00, 30.00);
INSERT INTO bdjardineria.producto VALUES ('OR-227', 'Chamaerops Humilis', 'Ornamentales', '130 - 150', 'Viveros EL OASIS', '', 100, 64.00, 51.00);
INSERT INTO bdjardineria.producto VALUES ('OR-228', 'Chamaerops Humilis "Cerifera"', 'Ornamentales', '70 - 80', 'Viveros EL OASIS', '', 100, 32.00, 25.00);
INSERT INTO bdjardineria.producto VALUES ('OR-229', 'Chrysalidocarpus Lutescens -ARECA', 'Ornamentales', '130 - 150', 'Viveros EL OASIS', '', 100, 22.00, 17.00);
INSERT INTO bdjardineria.producto VALUES ('OR-230', 'Cordyline Australis -DRACAENA', 'Ornamentales', '190 - 210', 'Viveros EL OASIS', '', 100, 38.00, 30.00);
INSERT INTO bdjardineria.producto VALUES ('OR-231', 'Cycas Revoluta', 'Ornamentales', '55 - 65', 'Viveros EL OASIS', '', 100, 15.00, 12.00);
INSERT INTO bdjardineria.producto VALUES ('OR-232', 'Cycas Revoluta', 'Ornamentales', '80 - 90', 'Viveros EL OASIS', '', 100, 34.00, 27.00);
INSERT INTO bdjardineria.producto VALUES ('OR-233', 'Dracaena Drago', 'Ornamentales', '60 - 70', 'Viveros EL OASIS', '', 1, 13.00, 10.00);
INSERT INTO bdjardineria.producto VALUES ('OR-234', 'Dracaena Drago', 'Ornamentales', '130 - 150', 'Viveros EL OASIS', '', 2, 64.00, 51.00);
INSERT INTO bdjardineria.producto VALUES ('OR-235', 'Dracaena Drago', 'Ornamentales', '150 - 175', 'Viveros EL OASIS', '', 2, 92.00, 73.00);
INSERT INTO bdjardineria.producto VALUES ('OR-236', 'Jubaea Chilensis', 'Ornamentales', '', 'Viveros EL OASIS', '', 100, 49.00, 39.00);
INSERT INTO bdjardineria.producto VALUES ('OR-237', 'Livistonia Australis', 'Ornamentales', '100 - 125', 'Viveros EL OASIS', '', 50, 19.00, 15.00);
INSERT INTO bdjardineria.producto VALUES ('OR-238', 'Livistonia Decipiens', 'Ornamentales', '90 - 110', 'Viveros EL OASIS', '', 50, 19.00, 15.00);
INSERT INTO bdjardineria.producto VALUES ('OR-239', 'Livistonia Decipiens', 'Ornamentales', '180 - 200', 'Viveros EL OASIS', '', 50, 49.00, 39.00);
INSERT INTO bdjardineria.producto VALUES ('OR-240', 'Phoenix Canariensis', 'Ornamentales', '110 - 130', 'Viveros EL OASIS', '', 50, 6.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-241', 'Phoenix Canariensis', 'Ornamentales', '180 - 200', 'Viveros EL OASIS', '', 50, 19.00, 15.00);
INSERT INTO bdjardineria.producto VALUES ('OR-242', 'Rhaphis Excelsa', 'Ornamentales', '80 - 100', 'Viveros EL OASIS', '', 50, 21.00, 16.00);
INSERT INTO bdjardineria.producto VALUES ('OR-243', 'Rhaphis Humilis', 'Ornamentales', '150- 170', 'Viveros EL OASIS', '', 50, 64.00, 51.00);
INSERT INTO bdjardineria.producto VALUES ('OR-244', 'Sabal Minor', 'Ornamentales', '60 - 75', 'Viveros EL OASIS', '', 50, 11.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-245', 'Sabal Minor', 'Ornamentales', '120 - 140', 'Viveros EL OASIS', '', 50, 34.00, 27.00);
INSERT INTO bdjardineria.producto VALUES ('OR-246', 'Trachycarpus Fortunei', 'Ornamentales', '90 - 105', 'Viveros EL OASIS', '', 50, 18.00, 14.00);
INSERT INTO bdjardineria.producto VALUES ('OR-247', 'Trachycarpus Fortunei', 'Ornamentales', '250-300', 'Viveros EL OASIS', '', 2, 462.00, 369.00);
INSERT INTO bdjardineria.producto VALUES ('OR-248', 'Washingtonia Robusta', 'Ornamentales', '60 - 70', 'Viveros EL OASIS', '', 15, 3.00, 2.00);
INSERT INTO bdjardineria.producto VALUES ('OR-249', 'Washingtonia Robusta', 'Ornamentales', '130 - 150', 'Viveros EL OASIS', '', 15, 5.00, 4.00);
INSERT INTO bdjardineria.producto VALUES ('OR-250', 'Yucca Jewel', 'Ornamentales', '80 - 105', 'Viveros EL OASIS', '', 15, 10.00, 8.00);
INSERT INTO bdjardineria.producto VALUES ('OR-251', 'Zamia Furfuracaea', 'Ornamentales', '90 - 110', 'Viveros EL OASIS', '', 15, 168.00, 134.00);
INSERT INTO bdjardineria.producto VALUES ('OR-99', 'Mimosa DEALBATA Gaulois Astier  ', 'Ornamentales', '200-225', 'Viveros EL OASIS', 'Acacia dealbata. nombre común o vulgar: Mimosa fina, Mimosa, Mimosa común, Mimosa plateada, Aromo francés. Familia: Mimosaceae. Origen: Australia, Sureste, (N. G. del Sur y Victoria). Arbol de follaje persistente muy usado en parques por su atractiva floración amarilla hacia fines del invierno. Altura: de 3 a 10 metros generalmente. Crecimiento rápido. Follaje perenne de tonos plateados, muy ornamental. Sus hojas son de textura fina, de color verde y sus flores amarillas que aparecen en racimos grandes. Florece de Enero a Marzo (Hemisferio Norte). Legumbre de 5-9 cm de longitud, recta o ligeramente curvada, con los bordes algo constreñidos entre las semillas, que se disponen en el fruto longitudinalmente...', 100, 14.00, 11.00);


--
-- TOC entry 2110 (class 2606 OID 18540)
-- Name: categoria idx_18481_primary; Type: CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.categoria
    ADD CONSTRAINT idx_18481_primary PRIMARY KEY (categoria_nombre);


--
-- TOC entry 2114 (class 2606 OID 18543)
-- Name: cliente idx_18487_primary; Type: CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.cliente
    ADD CONSTRAINT idx_18487_primary PRIMARY KEY (cliente_cod);


--
-- TOC entry 2117 (class 2606 OID 18547)
-- Name: detallepedido idx_18490_primary; Type: CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.detallepedido
    ADD CONSTRAINT idx_18490_primary PRIMARY KEY (pedido_cod, producto_cod);


--
-- TOC entry 2121 (class 2606 OID 18541)
-- Name: empleado idx_18493_primary; Type: CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.empleado
    ADD CONSTRAINT idx_18493_primary PRIMARY KEY (empleado_cod);


--
-- TOC entry 2124 (class 2606 OID 18544)
-- Name: oficina idx_18496_primary; Type: CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.oficina
    ADD CONSTRAINT idx_18496_primary PRIMARY KEY (oficina_cod);


--
-- TOC entry 2126 (class 2606 OID 18542)
-- Name: pago idx_18499_primary; Type: CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.pago
    ADD CONSTRAINT idx_18499_primary PRIMARY KEY (pedido_cod, pago_num);


--
-- TOC entry 2128 (class 2606 OID 18545)
-- Name: pais idx_18503_primary; Type: CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.pais
    ADD CONSTRAINT idx_18503_primary PRIMARY KEY (nombre);


--
-- TOC entry 2131 (class 2606 OID 18546)
-- Name: pedido idx_18506_primary; Type: CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.pedido
    ADD CONSTRAINT idx_18506_primary PRIMARY KEY (pedido_cod);


--
-- TOC entry 2133 (class 2606 OID 18548)
-- Name: producto idx_18512_primary; Type: CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.producto
    ADD CONSTRAINT idx_18512_primary PRIMARY KEY (producto_cod);


--
-- TOC entry 2111 (class 1259 OID 18523)
-- Name: idx_18487_cliente_empleadofk; Type: INDEX; Schema: bdjardineria; Owner: batoi
--

CREATE INDEX idx_18487_cliente_empleadofk ON bdjardineria.cliente USING btree (empleado_cod);


--
-- TOC entry 2112 (class 1259 OID 18524)
-- Name: idx_18487_cliente_fk; Type: INDEX; Schema: bdjardineria; Owner: batoi
--

CREATE INDEX idx_18487_cliente_fk ON bdjardineria.cliente USING btree (pais);


--
-- TOC entry 2115 (class 1259 OID 18531)
-- Name: idx_18490_detallepedido_productofk; Type: INDEX; Schema: bdjardineria; Owner: batoi
--

CREATE INDEX idx_18490_detallepedido_productofk ON bdjardineria.detallepedido USING btree (producto_cod);


--
-- TOC entry 2118 (class 1259 OID 18520)
-- Name: idx_18493_empleado_empleadofk; Type: INDEX; Schema: bdjardineria; Owner: batoi
--

CREATE INDEX idx_18493_empleado_empleadofk ON bdjardineria.empleado USING btree (jefe_cod);


--
-- TOC entry 2119 (class 1259 OID 18521)
-- Name: idx_18493_empleado_oficinafk; Type: INDEX; Schema: bdjardineria; Owner: batoi
--

CREATE INDEX idx_18493_empleado_oficinafk ON bdjardineria.empleado USING btree (oficina_cod);


--
-- TOC entry 2122 (class 1259 OID 18527)
-- Name: idx_18496_oficina_fk; Type: INDEX; Schema: bdjardineria; Owner: batoi
--

CREATE INDEX idx_18496_oficina_fk ON bdjardineria.oficina USING btree (pais);


--
-- TOC entry 2129 (class 1259 OID 18529)
-- Name: idx_18506_pedido_cliente; Type: INDEX; Schema: bdjardineria; Owner: batoi
--

CREATE INDEX idx_18506_pedido_cliente ON bdjardineria.pedido USING btree (cliente_cod);


--
-- TOC entry 2134 (class 1259 OID 18533)
-- Name: idx_18512_producto_categoria_nombrefk; Type: INDEX; Schema: bdjardineria; Owner: batoi
--

CREATE INDEX idx_18512_producto_categoria_nombrefk ON bdjardineria.producto USING btree (categoria_nombre);


--
-- TOC entry 2135 (class 2606 OID 18549)
-- Name: cliente cliente_empleadofk; Type: FK CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.cliente
    ADD CONSTRAINT cliente_empleadofk FOREIGN KEY (empleado_cod) REFERENCES bdjardineria.empleado(empleado_cod) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2136 (class 2606 OID 18554)
-- Name: cliente cliente_fk; Type: FK CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.cliente
    ADD CONSTRAINT cliente_fk FOREIGN KEY (pais) REFERENCES bdjardineria.pais(nombre) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2137 (class 2606 OID 18559)
-- Name: detallepedido detallepedido_pedidofk; Type: FK CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.detallepedido
    ADD CONSTRAINT detallepedido_pedidofk FOREIGN KEY (pedido_cod) REFERENCES bdjardineria.pedido(pedido_cod) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2138 (class 2606 OID 18564)
-- Name: detallepedido detallepedido_productofk; Type: FK CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.detallepedido
    ADD CONSTRAINT detallepedido_productofk FOREIGN KEY (producto_cod) REFERENCES bdjardineria.producto(producto_cod) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2139 (class 2606 OID 18569)
-- Name: empleado empleado_empleadofk; Type: FK CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.empleado
    ADD CONSTRAINT empleado_empleadofk FOREIGN KEY (jefe_cod) REFERENCES bdjardineria.empleado(empleado_cod) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2140 (class 2606 OID 18574)
-- Name: empleado empleado_oficinafk; Type: FK CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.empleado
    ADD CONSTRAINT empleado_oficinafk FOREIGN KEY (oficina_cod) REFERENCES bdjardineria.oficina(oficina_cod) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2141 (class 2606 OID 18579)
-- Name: oficina oficina_fk; Type: FK CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.oficina
    ADD CONSTRAINT oficina_fk FOREIGN KEY (pais) REFERENCES bdjardineria.pais(nombre) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2142 (class 2606 OID 18584)
-- Name: pago pago_fk; Type: FK CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.pago
    ADD CONSTRAINT pago_fk FOREIGN KEY (pedido_cod) REFERENCES bdjardineria.pedido(pedido_cod) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2143 (class 2606 OID 18589)
-- Name: pedido pedido_cliente; Type: FK CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.pedido
    ADD CONSTRAINT pedido_cliente FOREIGN KEY (cliente_cod) REFERENCES bdjardineria.cliente(cliente_cod) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2144 (class 2606 OID 18594)
-- Name: producto producto_categoria_nombrefk; Type: FK CONSTRAINT; Schema: bdjardineria; Owner: batoi
--

ALTER TABLE ONLY bdjardineria.producto
    ADD CONSTRAINT producto_categoria_nombrefk FOREIGN KEY (categoria_nombre) REFERENCES bdjardineria.categoria(categoria_nombre) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2020-04-14 21:12:39 UTC

--
-- PostgreSQL database dump complete
--

