SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP DATABASE IF EXISTS investigadores;
CREATE DATABASE investigadores;
USE investigadores;

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `numserie` char(4) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `facultad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`numserie`, `nombre`, `facultad`) VALUES
('pc01', 'Equipo PC01', 8),
('pc02', 'Equipo PC02', 9),
('pc03', 'Equipo PC03', 8),
('pc04', 'Equipo PC04', 8),
('pc05', 'Equipo PC05', 5),
('pc06', 'Equipo PC06', 7),
('pc07', 'Equipo PC07', 1),
('pc08', 'Equipo PC08', 4),
('pc09', 'Equipo PC09', 1),
('pc10', 'Equipo PC10', 5),
('pc11', 'Equipo PC11', 3),
('pc12', 'Equipo PC12', 9),
('pc13', 'Equipo PC13', 8),
('pc14', 'Equipo PC14', 3),
('pc15', 'Equipo PC15', 8),
('pc16', 'Equipo PC16', 7),
('pc17', 'Equipo PC17', 2),
('pc18', 'Equipo PC18', 7),
('pc19', 'Equipo PC19', 7),
('pc20', 'Equipo PC20', 8),
('pc21', 'Equipo PC21', 8),
('pc22', 'Equipo PC22', 8),
('pc23', 'Equipo PC23', 10),
('pc24', 'Equipo PC24', 2),
('pc25', 'Equipo PC25', 6),
('pc26', 'Equipo PC26', 2),
('pc27', 'Equipo PC27', 2),
('pc28', 'Equipo PC28', 8),
('pc29', 'Equipo PC29', 2),
('pc30', 'Equipo PC30', 6),
('pc31', 'Equipo PC31', 2),
('pc32', 'Equipo PC32', 4),
('pc33', 'Equipo PC33', 9),
('pc34', 'Equipo PC34', 5),
('pc35', 'Equipo PC35', 9),
('pc36', 'Equipo PC36', 8),
('pc37', 'Equipo PC37', 9),
('pc38', 'Equipo PC38', 1),
('pc39', 'Equipo PC39', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facultad`
--

CREATE TABLE `facultad` (
  `cod` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `facultad`
--

INSERT INTO `facultad` (`cod`, `nombre`) VALUES
(1, 'facul01'),
(2, 'facul02'),
(3, 'facul03'),
(4, 'facul04'),
(5, 'facul05'),
(6, 'facul06'),
(7, 'facul07'),
(8, 'facul08'),
(9, 'facul09'),
(10, 'facul10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `investigador`
--

CREATE TABLE `investigador` (
  `dni` char(9) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `facultad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `investigador`
--

INSERT INTO `investigador` (`dni`, `nombre`, `facultad`) VALUES
('01453762R', 'Oscar', 8),
('04881413P', 'Daniel', 3),
('07235654S', 'Joaquín', 7),
('10642182J', 'Aaron', 6),
('16466281Y', 'Maria', 7),
('19772516Z', 'Luis Miguel', 3),
('23268831F', 'Lucia', 2),
('24184573G', 'David', 7),
('24822894Y', 'Ciro Ivan', 4),
('25469516Y', 'Jose Antonio', 6),
('27830531W', 'Ramón', 4),
('32440546M', 'Adrian', 2),
('32563540W', 'Marc', 7),
('37338175K', 'Carlos', 5),
('44585378Q', 'Jose Luis', 8),
('45690264M', 'Jordi', 3),
('48724150F', 'Borja', 2),
('48787413C', 'Benedicto', 8),
('49696037Y', 'Esmeralda', 4),
('51182246R', 'Miguel', 7),
('55644636T', 'pepe', 2),
('56224668H', 'david', 2),
('56620588Q', 'jordi', 6),
('63313457T', 'Sergio', 2),
('63456993Q', 'Luis Miguel', 10),
('71065926C', 'Sergio', 7),
('79723373Z', 'Johnny', 5),
('80824894L', 'Antoni', 4),
('84189307S', 'Mauro', 9),
('84446763D', 'Edgar', 9),
('91445931R', 'Mario', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `dni` char(9) NOT NULL,
  `numserie` char(4) NOT NULL,
  `comienzo` date NOT NULL,
  `fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`dni`, `numserie`, `comienzo`, `fin`) VALUES
('01453762R', 'pc02', '2019-05-17', '2019-05-17'),
('01453762R', 'pc02', '2019-05-19', '2019-05-19'),
('01453762R', 'pc03', '2019-05-19', '2019-05-21'),
('04881413P', 'pc02', '2012-09-14', '2012-09-15'),
('04881413P', 'pc04', '2012-09-21', '2012-09-22'),
('04881413P', 'pc10', '2013-07-01', '2013-07-04'),
('04881413P', 'pc25', '2016-05-03', '2016-05-04'),
('04881413P', 'pc26', '2014-01-30', '2014-02-01'),
('04881413P', 'pc35', '2012-03-22', '2012-03-23'),
('07235654S', 'pc10', '2014-05-15', '2014-05-16'),
('07235654S', 'pc22', '2014-07-06', '2014-07-08'),
('07235654S', 'pc34', '2014-07-03', '2014-07-05'),
('10642182J', 'pc05', '2019-05-09', '2019-05-09'),
('10642182J', 'pc24', '2012-09-10', '2012-09-10'),
('16466281Y', 'pc09', '2019-05-17', '2019-05-19'),
('16466281Y', 'pc18', '2012-09-15', '2012-09-15'),
('16466281Y', 'pc19', '2019-05-20', '2019-05-22'),
('16466281Y', 'pc23', '2012-04-26', '2012-04-28'),
('16466281Y', 'pc31', '2014-05-21', '2014-05-22'),
('16466281Y', 'pc31', '2019-05-17', '2019-05-18'),
('19772516Z', 'pc15', '2016-11-18', '2016-11-20'),
('19772516Z', 'pc25', '2016-10-17', '2016-10-19'),
('19772516Z', 'pc34', '2013-01-18', '2013-01-19'),
('19772516Z', 'pc35', '2012-12-01', '2012-12-03'),
('19772516Z', 'pc38', '2012-07-07', '2012-07-08'),
('23268831F', 'pc09', '2014-02-16', '2014-02-18'),
('23268831F', 'pc16', '2012-05-30', '2012-05-30'),
('23268831F', 'pc18', '2013-02-23', '2013-02-25'),
('23268831F', 'pc25', '2015-04-02', '2015-04-02'),
('24184573G', 'pc28', '2013-12-17', '2013-12-18'),
('24184573G', 'pc29', '2014-04-30', '2014-05-02'),
('27830531W', 'pc04', '2016-02-01', '2016-02-01'),
('27830531W', 'pc16', '2019-05-11', '2019-05-14'),
('27830531W', 'pc18', '2019-05-09', '2019-05-11'),
('32440546M', 'pc03', '2015-01-06', '2015-01-08'),
('32440546M', 'pc08', '2016-05-30', '2016-06-01'),
('32440546M', 'pc24', '2016-09-28', '2016-09-29'),
('32563540W', 'pc24', '2012-03-06', '2012-03-08'),
('32563540W', 'pc24', '2016-01-18', '2016-01-20'),
('32563540W', 'pc32', '2012-11-01', '2012-11-02'),
('37338175K', 'pc03', '2014-08-09', '2014-08-10'),
('37338175K', 'pc04', '2012-06-20', '2012-06-22'),
('37338175K', 'pc07', '2019-05-06', '2019-05-06'),
('37338175K', 'pc16', '2019-05-14', '2019-05-17'),
('37338175K', 'pc18', '2019-05-14', '2019-05-16'),
('37338175K', 'pc22', '2013-11-21', '2013-11-23'),
('37338175K', 'pc26', '2014-12-14', '2014-12-16'),
('37338175K', 'pc26', '2019-05-01', '2019-05-02'),
('37338175K', 'pc32', '2015-05-28', '2015-05-30'),
('44585378Q', 'pc27', '2013-06-03', '2013-06-06'),
('45690264M', 'pc27', '2014-01-05', '2014-01-06'),
('48724150F', 'pc08', '2012-08-27', '2012-08-28'),
('48787413C', 'pc03', '2015-10-19', '2015-10-20'),
('48787413C', 'pc22', '2016-09-21', '2016-09-22'),
('48787413C', 'pc23', '2019-05-14', '2019-05-17'),
('49696037Y', 'pc10', '2014-12-25', '2014-12-27'),
('49696037Y', 'pc17', '2019-05-27', '2019-05-30'),
('49696037Y', 'pc21', '2013-08-25', '2013-08-27'),
('51182246R', 'pc14', '2015-11-28', '2015-12-01'),
('51182246R', 'pc27', '2016-03-05', '2016-03-07'),
('51182246R', 'pc30', '2019-05-12', '2019-05-15'),
('51182246R', 'pc37', '2012-01-30', '2012-02-01'),
('55644636T', 'pc10', '2012-08-29', '2012-08-31'),
('55644636T', 'pc20', '2015-08-05', '2015-08-08'),
('55644636T', 'pc23', '2019-05-19', '2019-05-20'),
('56224668H', 'pc14', '2012-12-23', '2012-12-25'),
('56224668H', 'pc31', '2019-05-30', '2019-06-02'),
('56620588Q', 'pc21', '2012-02-24', '2012-02-24'),
('63313457T', 'pc13', '2012-06-07', '2012-06-10'),
('63313457T', 'pc17', '2019-05-29', '2019-05-30'),
('63313457T', 'pc29', '2013-07-15', '2013-07-15'),
('63456993Q', 'pc13', '2019-05-10', '2019-05-12'),
('63456993Q', 'pc17', '2013-08-11', '2013-08-11'),
('63456993Q', 'pc30', '2019-05-02', '2019-05-04'),
('79723373Z', 'pc37', '2014-01-01', '2014-01-03'),
('79723373Z', 'pc37', '2019-05-27', '2019-05-29'),
('80824894L', 'pc24', '2015-03-09', '2015-03-11'),
('80824894L', 'pc37', '2012-10-01', '2012-10-03'),
('84189307S', 'pc26', '2014-03-14', '2014-03-17'),
('84189307S', 'pc31', '2014-07-05', '2014-07-08'),
('84189307S', 'pc37', '2015-09-07', '2015-09-10'),
('84446763D', 'pc10', '2016-11-22', '2016-11-24'),
('84446763D', 'pc17', '2019-05-19', '2019-05-20'),
('84446763D', 'pc18', '2019-05-16', '2019-05-17'),
('84446763D', 'pc33', '2019-05-17', '2019-05-19'),
('91445931R', 'pc08', '2019-05-03', '2019-05-06'),
('91445931R', 'pc15', '2013-11-17', '2013-11-17');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`numserie`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`),
  ADD KEY `fk_EQUIPOS_FACULTAD1` (`facultad`);

--
-- Indices de la tabla `facultad`
--
ALTER TABLE `facultad`
  ADD PRIMARY KEY (`cod`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

--
-- Indices de la tabla `investigador`
--
ALTER TABLE `investigador`
  ADD PRIMARY KEY (`dni`),
  ADD KEY `fk_INVESTIGADORES_FACULTAD` (`facultad`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`dni`,`numserie`,`comienzo`),
  ADD KEY `fk_INVESTIGADORES_has_EQUIPOS_EQUIPOS1` (`numserie`),
  ADD KEY `fk_INVESTIGADORES_has_EQUIPOS_INVESTIGADORES1` (`dni`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `facultad`
--
ALTER TABLE `facultad`
  MODIFY `cod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `fk_EQUIPOS_FACULTAD1` FOREIGN KEY (`facultad`) REFERENCES `facultad` (`cod`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `investigador`
--
ALTER TABLE `investigador`
  ADD CONSTRAINT `fk_INVESTIGADORES_FACULTAD` FOREIGN KEY (`facultad`) REFERENCES `facultad` (`cod`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `fk_INVESTIGADORES_has_EQUIPOS_EQUIPOS1` FOREIGN KEY (`numserie`) REFERENCES `equipo` (`numserie`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_INVESTIGADORES_has_EQUIPOS_INVESTIGADORES1` FOREIGN KEY (`dni`) REFERENCES `investigador` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION;


