-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: cientificos
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE IF EXISTS bdcientificos;
CREATE DATABASE bdcientificos;
USE bdcientificos;

--
-- Table structure for table `asignar`
--

DROP TABLE IF EXISTS `asignar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignar` (
  `proyecto_id` int(11) NOT NULL,
  `cientifico_id` int(11) NOT NULL,
  `horas` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`proyecto_id`,`cientifico_id`),
  KEY `fk_proyecto_has_cientifico_cientifico1_idx` (`cientifico_id`),
  KEY `fk_proyecto_has_cientifico_proyecto_idx` (`proyecto_id`),
  CONSTRAINT `fk_proyecto_has_cientifico_cientifico1` FOREIGN KEY (`cientifico_id`) REFERENCES `cientifico` (`cientifico_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proyecto_has_cientifico_proyecto` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`proyecto_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignar`
--

LOCK TABLES `asignar` WRITE;
/*!40000 ALTER TABLE `asignar` DISABLE KEYS */;
INSERT INTO `asignar` VALUES (1,1,100),(2,1,1000),(2,2,800),(2,3,700),(3,2,600),(3,3,200),(4,2,100),(4,3,100),(1,3,10),(5,3,200),(6,3,100);
/*!40000 ALTER TABLE `asignar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cientifico`
--

DROP TABLE IF EXISTS `cientifico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cientifico` (
  `cientifico_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cientifico_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `cientifico2020`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cientifico2020` (
  `cientifico_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cientifico_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cientifico`
--

LOCK TABLES `cientifico` WRITE;
/*!40000 ALTER TABLE `cientifico` DISABLE KEYS */;
INSERT INTO `cientifico` VALUES (1,'sergio'),(2,'carlos'),(3,'daniel'),(4,'amparo'),(5,'xavier');
/*!40000 ALTER TABLE `cientifico` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `cientifico2020` WRITE;
/*!40000 ALTER TABLE `cientifico2020` DISABLE KEYS */;
INSERT INTO `cientifico2020` VALUES (1,'sergio'), (3,'daniel'), (6,'luis'), (7,'raul');
/*!40000 ALTER TABLE `cientifico2020` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto` (
  `proyecto_id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `horas` smallint(6) DEFAULT NULL,
  `tipo` enum('software','hardware','redes','otros') DEFAULT NULL,
  PRIMARY KEY (`proyecto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto`
--

LOCK TABLES `proyecto` WRITE;
/*!40000 ALTER TABLE `proyecto` DISABLE KEYS */;
INSERT INTO `proyecto` VALUES (1,'diseño base datos',100,'software'),(2,'creación video juego',2500,'software'),(3,'aplicación de gestión',800,'software'),(4,'instalación red empresa',250,'redes'),(5,'actualizar hardware equipos',100,'hardware'),(6,'inventariar',50,'otros'), (7,'app móvil',80,'software');
/*!40000 ALTER TABLE `proyecto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-29  9:35:12
