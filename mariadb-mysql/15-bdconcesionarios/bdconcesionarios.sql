-- MariaDB dump 10.19  Distrib 10.5.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: bdconcesionario
-- ------------------------------------------------------
-- Server version	10.5.12-MariaDB-1:10.5.12+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE IF EXISTS bdconcesionario;
CREATE DATABASE IF NOT EXISTS bdconcesionario;
USE bdconcesionario;

--
-- Table structure for table `ciudades`
--

DROP TABLE IF EXISTS `ciudades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudades` (
  `nombre` varchar(10) NOT NULL,
  `habitantes` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudades`
--

LOCK TABLES `ciudades` WRITE;
/*!40000 ALTER TABLE `ciudades` DISABLE KEYS */;
INSERT INTO `ciudades` VALUES ('Alcoy',58977),('Alicante',331577),('Barcelona',1620000),('Elda',52404),('Ibi',23403),('Ibiza',49727),('Madrid',3223000),('Monóvar',12175),('Murcia',447182),('Novelda',25725),('Petrer',34479),('Valencia',791413),('Villena',33983),('Zaragoza',666880);
/*!40000 ALTER TABLE `ciudades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(12) DEFAULT NULL,
  `apellido` varchar(15) DEFAULT NULL,
  `ciudad` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ciudad` (`ciudad`),
  CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`ciudad`) REFERENCES `ciudades` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (31,'Jorge','Candel','Petrer'),(987619,'Santiago','Amorós','Petrer'),(11111112,'Jose Miguel','Martón','Novelda'),(12299999,'Ricardo','Mancheño','Monóvar'),(12345678,'Kevin','Esteve','Ibi'),(14123233,'Jemima','García','Monóvar'),(22222211,'Andrés','Pérez','Madrid'),(23287490,'Enrique','Sánchez','Madrid'),(24583143,'Alvaro','Naranjo','Monóvar'),(44512333,'David','Ruiz','Villena'),(44545433,'Francisco','Vicedo','Alicante'),(44567676,'Isela','Guerrero','Madrid'),(44599887,'Pascual','Bazán','Elda'),(65344533,'Silvia','Pérez','Alcoy'),(67453423,'Alvaro','Alted','Petrer'),(76357844,'Marcos','Vicente','Novelda'),(76411111,'Carlos','Lencina','Elda'),(87643455,'Sergio','Sepulcre','Alcoy'),(90876542,'Ignacio','Sanchis','Ibi'),(98765432,'Nacho','Fernández','Petrer');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coches`
--

DROP TABLE IF EXISTS `coches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coches` (
  `cod` varchar(8) NOT NULL,
  `nombre` varchar(15) DEFAULT NULL,
  `marca` int(11) NOT NULL,
  `precio` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod`),
  KEY `marca` (`marca`),
  CONSTRAINT `coches_ibfk_1` FOREIGN KEY (`marca`) REFERENCES `marcas` (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coches`
--

LOCK TABLES `coches` WRITE;
/*!40000 ALTER TABLE `coches` DISABLE KEYS */;
INSERT INTO `coches` VALUES ('A3','AUDI A3',2,11500),('A5','AUDI A5',2,14000),('A6Q','AUDI A6 QUATTRO',2,17000),('BMWe34','BMW Serie 5',1,14760),('BMWe46','BMW Serie 3 ',1,13450),('BMWe86','BMW Serie 1',1,13200),('BMWm45','BMW Serie 7',1,15800),('BMWm87','BMW Serie 4',1,17850),('C4C','C4',3,7400),('C5C','BMW Serie 1',3,9800),('IBIZA','Ibiza',4,8500),('KCEED','CEED',5,11000),('KRIO','RIO',5,8000),('KSPORT','SPORTAGE',5,15000),('NISPU','PULSAR',6,16000),('NISQA','QASQAI',6,14000);
/*!40000 ALTER TABLE `coches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `concesionarios`
--

DROP TABLE IF EXISTS `concesionarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `concesionarios` (
  `cif` int(11) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `ciudad` varchar(10) NOT NULL,
  PRIMARY KEY (`cif`),
  KEY `ciudad` (`ciudad`),
  CONSTRAINT `concesionarios_ibfk_1` FOREIGN KEY (`ciudad`) REFERENCES `ciudades` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `concesionarios`
--

LOCK TABLES `concesionarios` WRITE;
/*!40000 ALTER TABLE `concesionarios` DISABLE KEYS */;
INSERT INTO `concesionarios` VALUES (1222222,'Automóviles Alpe','Petrer'),(11111111,'Móvil Begar','Petrer'),(11232123,'BMW Barna','Barcelona'),(32323232,'4 rodes','Ibiza'),(44554433,'Fercar','Alicante'),(54345432,'Tot Turbo','Barcelona'),(56429642,'Grant Turismo','Alicante'),(76543213,'Todo motor','Madrid'),(77323232,'Motor Sport','Murcia'),(85543123,'Multi Car','Alicante'),(85643123,'BMW Villa de campo','Madrid'),(98654678,'Tope Gama','Madrid');
/*!40000 ALTER TABLE `concesionarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distribuciones`
--

DROP TABLE IF EXISTS `distribuciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribuciones` (
  `concesionario` int(11) NOT NULL,
  `coche` varchar(8) NOT NULL,
  `cantidad` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`concesionario`,`coche`),
  KEY `coche` (`coche`),
  CONSTRAINT `distribuciones_ibfk_1` FOREIGN KEY (`concesionario`) REFERENCES `concesionarios` (`cif`),
  CONSTRAINT `distribuciones_ibfk_2` FOREIGN KEY (`coche`) REFERENCES `coches` (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distribuciones`
--

LOCK TABLES `distribuciones` WRITE;
/*!40000 ALTER TABLE `distribuciones` DISABLE KEYS */;
INSERT INTO `distribuciones` VALUES (1222222,'KCEED',14),(1222222,'KRIO',25),(1222222,'KSPORT',12),(11111111,'BMWe34',5),(11111111,'BMWe46',11),(11111111,'BMWe86',10),(32323232,'IBIZA',17),(44554433,'A3',4),(54345432,'A3',9),(54345432,'A5',3),(56429642,'IBIZA',8),(77323232,'C5C',7),(77323232,'NISQA',6),(85543123,'KCEED',9),(85543123,'NISQA',5),(85643123,'IBIZA',6),(85643123,'NISPU',8);
/*!40000 ALTER TABLE `distribuciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marcas`
--

DROP TABLE IF EXISTS `marcas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marcas` (
  `cod` int(11) NOT NULL,
  `nombre` varchar(10) DEFAULT NULL,
  `ciudad` varchar(10) NOT NULL,
  PRIMARY KEY (`cod`),
  KEY `ciudad` (`ciudad`),
  CONSTRAINT `marcas_ibfk_1` FOREIGN KEY (`ciudad`) REFERENCES `ciudades` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marcas`
--

LOCK TABLES `marcas` WRITE;
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
INSERT INTO `marcas` VALUES (1,'BMW','Barcelona'),(2,'AUDI','Madrid'),(3,'Citroen','Alicante'),(4,'Seat','Madrid'),(5,'Kia','Petrer'),(6,'Nissan','Valencia'),(7,'Renault','Barcelona'),(8,'Opel','Zaragoza'),(9,'Ford','Valencia'),(10,'Mercedes','Alicante');
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas` (
  `concesionario` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `coche` varchar(8) NOT NULL,
  `color` varchar(9) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  PRIMARY KEY (`concesionario`,`cliente`,`coche`),
  KEY `cliente` (`cliente`),
  KEY `coche` (`coche`),
  CONSTRAINT `ventas_ibfk_1` FOREIGN KEY (`concesionario`) REFERENCES `concesionarios` (`cif`),
  CONSTRAINT `ventas_ibfk_2` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`),
  CONSTRAINT `ventas_ibfk_3` FOREIGN KEY (`coche`) REFERENCES `coches` (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
INSERT INTO `ventas` VALUES (1222222,31,'KSPORT','negro','2019-08-02',17340),(1222222,12299999,'KRIO','blanco','2019-08-03',11300),(1222222,23287490,'KCEED','blanco','2019-09-15',18200),(1222222,44512333,'KRIO','rojo','2019-08-01',15000),(1222222,44545433,'KRIO','gris','2019-10-27',9500),(1222222,44545433,'KSPORT','azul','2019-09-15',20100),(1222222,67453423,'KRIO','amarillo','2019-08-02',14000),(1222222,76357844,'KRIO','blanco','2019-08-02',11500),(1222222,76411111,'KRIO','blanco','2019-09-22',12300),(1222222,87643455,'KRIO','rojo','2019-08-01',13000),(11111111,987619,'BMWe86','negro','2019-09-20',37200),(11111111,14123233,'BMWe34','blanco','2019-06-22',31000),(11111111,44545433,'BMWe46','azul','2019-08-01',32000),(11111111,65344533,'BMWe86','blanco','2019-06-21',30000),(11111111,76411111,'BMWe86','negro','2019-08-01',29000),(11111111,90876542,'BMWe86','gris','2019-08-16',26000),(32323232,987619,'IBIZA','blanco','2019-09-16',10000),(44554433,24583143,'A3','rojo','2019-07-12',25000),(54345432,12345678,'A3','azul','2019-12-20',26430),(54345432,24583143,'A3','gris','2019-10-28',25300),(54345432,65344533,'A5','negro','2019-10-29',28950),(56429642,11111112,'IBIZA','azul','2019-08-02',27300),(77323232,12345678,'NISQA','blanco','2019-08-03',18000),(77323232,44567676,'C5C','negro','2019-06-21',7500),(85543123,987619,'IBIZA','rojo','2019-08-16',14500),(85543123,22222211,'KCEED','azul','2019-08-01',16450),(85543123,65344533,'NISQA','blanco','2019-09-30',23120),(85643123,44545433,'IBIZA','azul','2019-06-21',17000),(85643123,98765432,'NISPU','gris','2019-08-03',19500);
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-02 20:06:02
