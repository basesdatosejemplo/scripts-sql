--
-- Ventas por vendedor
--

CREATE VIEW ventas_vendedor AS 
SELECT * FROM vendedores
         WHERE NOT EXISTS (
                   SELECT 1 FROM grupos 
                   WHERE NOT EXISTS (
                             SELECT  1 FROM articulos 
                             WHERE grupo=grupos.Id 
                             AND EXISTS (
                                 SELECT 1 FROM lineas_factura, facturas
                                 WHERE lineas_factura.factura = facturas.id and articulo=articulos.id 
                                 AND  vendedor=vendedores.id )))
