DELIMITER $$
--
-- Procedimientos
--
CREATE PROCEDURE `compras_clientes`(IN dias INT, OUT num int)
BEGIN
SELECT count(*) INTO num FROM clientes WHERE Id IN (SELECT cliente FROM facturas WHERE fecha>=DATE_SUB(CURDATE(),INTERVAL dias DAY));
END$$

CREATE PROCEDURE `total_factura`(IN fact INT(10), OUT total float)
BEGIN
select sum(importe) into total from lineas_factura where factura = fact;
END$$

DELIMITER ;

